.. mldev-kubernetes documentation master file

MLDev Kubernetes
========================

.. toctree::
   :hidden:

   self

This is the official documentation for the MLDev Kubernetes software.
MLDev Kubernetes allows execute MLDev experiments in parallel using Kubernetes distributed computation environment.

.. toctree::
   :maxdepth: 1
   :caption: User documentation

   mldev-k8s.user-guide
   mldev-k8s.tutorial

.. toctree::
   :maxdepth: 1
   :caption: DevOps documentation

   mldev-k8s.devops-guide

.. toctree::
   :maxdepth: 1
   :caption: Developer documentation

   mldev-k8s.dev

.. toctree::
   :maxdepth: 1
   :caption: Reference

   mldev-k8s.reference

See more at project `Wiki <https://gitlab.com/mlrep/mldev-kubernetes/-/wikis/home>`_

Partners and supporters
-----------------------

**FASIE - Foundation for Assistance to Small Innovative Enterprises**

.. image:: https://www.fbras.ru/wp-content/uploads/2015/06/fasie_en__1_.png
   :alt: Foundation for Assistance to Small Innovative Enterprises
   :height: 80px
   :target: https://fasie.ru/

**Gitlab open source**

.. image:: https://gitlab.com/mlrep/mldev/-/wikis/images/gitlab-logo-gray-stacked-rgb.png
   :alt: GitLab Open Source program
   :height: 110px
   :target: https://about.gitlab.com/solutions/open-source/

Support and contacts
--------------------

Give feedback, suggest feature, report bug:

* `Telegram <https://t.me/mldev_betatest>`_ user group
* `#mlrep <https://opendatascience.slack.com>`_ channel at OpenDataScience Slack
* `Gitlab <https://gitlab.com/mlrep/mldev-kubernetes/-/issues>`_ issue tracker

Contributing
------------

Please check the `CONTRIBUTING.md <https://gitlab.com/mlrep/mldev/-/blob/develop/CONTRIBUTING.md>`_
guide if you'd like to participate in the project, ask a question or give a suggestion.

License
-------

The software is licensed under `Apache 2.0 license <https://gitlab.com/mlrep/mldev-kubernetes/-/blob/develop/LICENSE>`_

Index and tables
----------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`