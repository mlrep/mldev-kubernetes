JSON Pydantic models signatures implementation
========

.. automodule:: mldev_k8s.servings.models.signatures.json_pydantic
   :members:
   :show-inheritance: