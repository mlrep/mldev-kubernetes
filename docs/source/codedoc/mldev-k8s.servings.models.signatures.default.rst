Models signatures default implementation
========

.. automodule:: mldev_k8s.servings.models.signatures.default
   :members:
   :show-inheritance: