Kubernetes service launcher
========

.. automodule:: mldev_k8s.servings.launch.kubernetes
   :members:
   :show-inheritance: