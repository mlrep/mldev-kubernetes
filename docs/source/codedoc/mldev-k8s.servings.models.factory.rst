Model factory
========

.. automodule:: mldev_k8s.servings.models.factory
   :members:
   :show-inheritance: