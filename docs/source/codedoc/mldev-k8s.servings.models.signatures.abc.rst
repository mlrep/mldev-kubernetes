Models signatures abstract classes
========

.. automodule:: mldev_k8s.servings.models.signatures.abc
   :members:
   :show-inheritance: