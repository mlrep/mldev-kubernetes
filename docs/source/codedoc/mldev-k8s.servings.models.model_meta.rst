Model metadata
========

.. automodule:: mldev_k8s.servings.models.model_meta
   :members:
   :show-inheritance: