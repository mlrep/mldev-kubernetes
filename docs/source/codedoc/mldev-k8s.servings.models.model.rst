Inner model representation
========

.. automodule:: mldev_k8s.servings.models.model
   :members:
   :show-inheritance: