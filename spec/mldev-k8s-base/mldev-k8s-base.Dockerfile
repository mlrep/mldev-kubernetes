FROM ubuntu:18.04

USER root

RUN apt-get update && apt-get install curl sudo -y


RUN apt update && \
    apt install --no-install-recommends -y build-essential software-properties-common && \
    add-apt-repository -y ppa:deadsnakes/ppa && \
    apt install --no-install-recommends -y python3.7 python3.7-dev python3.7-distutils && \
    apt install --no-install-recommends -y python3.7-venv && \
    apt clean && rm -rf /var/lib/apt/lists/*

RUN update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 1
RUN update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 2

RUN curl -s https://bootstrap.pypa.io/get-pip.py -o get-pip.py && \
    python3 get-pip.py --force-reinstall && \
    rm get-pip.py

RUN pip3 --no-cache-dir install --upgrade virtualenv

# todo: proper user setup
RUN useradd -rm -s /bin/bash -g root -G sudo -u 1000 user
# RUN  echo 'user:user' | chpasswd

RUN mkdir /install_mldev
WORKDIR /install_mldev
RUN curl https://gitlab.com/mlrep/mldev/-/raw/develop/install_mldev.sh -o install_mldev.sh
RUN chmod +x ./install_mldev.sh
RUN echo N | ./install_mldev.sh base
#RUN ./install_mldev.sh base

RUN apt-get purge -y --auto-remove gcc-7 python3.7-dev python3.7-distutils build-essential

WORKDIR /