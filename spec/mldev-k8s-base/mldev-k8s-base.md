### k8s base image
docker build . -t mldev-k8s-base -f mldev-k8s-base.Dockerfile
docker image tag mldev-k8s-base <registry host name and port>/mldev-k8s-base
docker push <registry host name and port>/mldev-k8s-base