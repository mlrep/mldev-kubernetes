docker build . -t tensorboard-s3 -f tensorboard-s3.Dockerfile
docker run -d --name tensorboard-s3 -p 6006:6006 --env-file ./.env --privileged tensorboard-s3 