#!/bin/bash

# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

TENSORBOARD_LOG_DIR="${TENSORBOARD_LOG_DIR}"
ACCESS_KEY_ID="${S3_ACCESS_KEY_ID}"
SECRET_ACCESS_KEY="${S3_SECRET_ACCESS_KEY}"
BUCKET_NAME="${S3_BUCKET_NAME}"
HOST="${S3_HOST}"
MOUNT_DIR="${S3_MOUNT_DIR}"

mkdir -p "${S3_MOUNT_DIR}"

echo "${ACCESS_KEY_ID}":"${SECRET_ACCESS_KEY}" > /run_tensorboard_s3/.passwd-s3fs
chmod 600 /run_tensorboard_s3/.passwd-s3fs

s3fs "${BUCKET_NAME}" "${MOUNT_DIR}" -o passwd_file="/run_tensorboard_s3/.passwd-s3fs" -o url="${HOST}" -o use_path_request_style

tensorboard --logdir "${TENSORBOARD_LOG_DIR}" --bind_all