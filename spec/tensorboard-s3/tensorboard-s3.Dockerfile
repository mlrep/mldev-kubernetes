FROM tensorboard

USER root

WORKDIR /run_tensorboard_s3

RUN apt-get update && apt-get install s3fs -y

COPY run.sh .
RUN chmod 777 run.sh

WORKDIR /

ENTRYPOINT /run_tensorboard_s3/run.sh

