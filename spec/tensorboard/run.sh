# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

#!/bin/bash

TENSORBOARD_LOG_DIR="${TENSORBOARD_LOG_DIR}"

tensorboard --logdir "${TENSORBOARD_LOG_DIR}" --bind_all