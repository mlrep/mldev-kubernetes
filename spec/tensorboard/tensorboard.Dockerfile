# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

FROM ubuntu:18.04
RUN apt-get update
RUN apt-get install python3 -y
RUN apt-get install python3-pip -y
RUN apt-get install vim -y
RUN pip3 install --upgrade pip
RUN pip3 install --upgrade virtualenv
RUN pip3 install tensorflow==2.6.0
RUN pip3 install keras==2.6.*
#RUN python3 -m pip install tensorflow==2.6.0
RUN pip3 install tensorboard==2.6.0

WORKDIR /run_tensorboard

COPY run.sh .
RUN chmod 777 run.sh

WORKDIR /

ENTRYPOINT /run_tensorboard/run.sh