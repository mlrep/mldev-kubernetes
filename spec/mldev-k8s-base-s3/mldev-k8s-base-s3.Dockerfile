FROM mldev-k8s-base

WORKDIR /install_mldev

RUN apt-get update && apt-get install s3fs -y

COPY mldev-k8s-base-s3-mount.sh .
RUN chmod 777 mldev-k8s-base-s3-mount.sh

WORKDIR /
