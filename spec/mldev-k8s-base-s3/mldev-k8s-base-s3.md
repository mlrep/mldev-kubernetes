### k8s s3 base image
docker build . -t mldev-k8s-base-s3 -f mldev-k8s-base-s3.Dockerfile
docker image tag mldev-k8s-base-s3 <registry host name and port>/mldev-k8s-base-s3
docker push <registry host name and port>/mldev-k8s-base-s3
``