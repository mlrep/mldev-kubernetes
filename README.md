#### Описание MLDev Kubernetes на русском языке доступно по [ссылке](https://gitlab.com/mlrep/mldev-kubernetes/-/blob/master/README-ru.md) и в русскоязычных статьях [wiki](https://gitlab.com/mlrep/mldev-kubernetes/-/wikis/Главная)

# MLDev Kubernetes

MLDev Kubernetes gives researcher an ability to run experiments in Kubernetes. 
Kubernetes allows use of cluster computation resources, not available at researcher workstation.
So it makes compute-intensive experiments possible.

Parallel execution of experiment stages are also supported, 
it allows scale computing both at the cluster and locally, at researcher workstation. 

To store input data and experiment results it is possible to use different storage systems.
They are supported via file system interfaces and abstractions.

Simplicity and ease of use for researcher are distinctive features of MLDev Kubernetes.
To run experiment with MLDev Kubernetes researcher need not to dive into engineering or internal machinery of Kubernetes.
It also does not require any significant changes in existing experiment pipelines.

# Run experiment in Kubernetes

MLDev Kuberntes requires of preliminary environment setup. Guidance is given in [Setup](https://gitlab.com/mlrep/mldev-kubernetes/-/wikis/User-Guide) section of user guide.

[Instructions](https://gitlab.com/mlrep/mldev-kubernetes/-/wikis/User-Guide) include steps to run simple experiment in Kubernetes.

# Documentation

User and development documentation is available in project [wiki](https://gitlab.com/mlrep/mldev-kubernetes/-/wikis/home).

## User guide

[User guide](https://gitlab.com/mlrep/mldev-kubernetes/-/wikis/User-Guide) helps with:
- environment setting
- first run of experiment in Kubernetes 
- learning of MLDev Kubernetes functions

## Tutorial

Tutorial helps with doing of practical tasks and to learn MLDev Kubernetes capabilities better.

MLDev Kubernetes use cases are provided in [tutorial](https://gitlab.com/mlrep/mldev-kubernetes/-/wikis/Tutorial) in for of step-by-step instructions.

## Developer documentation

To setup developer environment, explore MLDev Kubernetes architecture and start development, please follow [developer documentation](https://gitlab.com/mlrep/mldev-kubernetes/-/wikis/Developer-documentation). 

## DevOps guide

Setup guides for Kubernetes execution environment and data storage required for MLDev Kubernetes are given in [DevOps guide](https://gitlab.com/mlrep/mldev-kubernetes/-/wikis/DevOps-Guide).

# Partners and supporters

### FASIE - Foundation for Assistance to Small Innovative Enterprises

<p>
<a href="https://fasie.ru/"><img src="https://www.fbras.ru/wp-content/uploads/2015/06/fasie_en__1_.png" alt="Foundation for Assistance to Small Innovative Enterprises" height="80px"/></a>
</p>

### Gitlab open source

<p>
<a href="https://about.gitlab.com/solutions/open-source/"><img src="https://gitlab.com/mlrep/mldev/-/wikis/images/gitlab-logo-gray-stacked-rgb.png" alt="GitLab Open Source program" height="80px"></a>
</p> 


# Support and contacts

Give feedback, suggest feature, report bug:
- [Telegram](https://t.me/mldev_betatest) user group
- [#mlrep](https://opendatascience.slack.com) channel at OpenDataScience Slack
- [Gitlab](https://gitlab.com/mlrep/mldev-kubernetes/-/issues) issue tracker

# Contributing

Please check the [CONTRIBUTING.md](https://gitlab.com/mlrep/mldev/-/blob/develop/CONTRIBUTING.md) guide if you'd like to participate in the project, ask a question or give a suggestion.

# License

The software is licensed under [Apache 2.0 license](LICENSE).
