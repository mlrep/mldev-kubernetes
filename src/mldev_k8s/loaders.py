# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Loaders module
==================

Module contains loaders for YAML tags

* :py:class:`ParallelNodeIterator`
    Wraps YAML sets and sequences inside ParallelPipeline tag to gain control over processing
* :py:function:`parallel_pipeline_loader`
    Loader for ParallelPipeline tag
* :py:function:`parallel_stages_loader`
    Loader for ParallelStages tag

"""

from yaml import SequenceNode, MappingNode
from yaml.constructor import ConstructorError

from mldev_k8s.commons import IterableType
from mldev_k8s.logs import CoordinatorLogFactory


class LoaderLogger:
    logger = None

    @staticmethod
    def get_logger():
        if not LoaderLogger.logger:
            LoaderLogger.logger = CoordinatorLogFactory().get_logger(__name__)
        return LoaderLogger.logger


class ParallelNodeIterator:
    """
    Wraps YAML sets and sequences inside ParallelPipeline tag to gain control over processing.
    In a tree-like DSL for dependencies specification ParallelNodeIterator corresponds to one non-leaf node.
    See also `graph_spec.GraphSpecStageGeneratorFactory`.

    :param loader: specific loader
    :param items: iterable containing node elements
    """

    def __init__(self, loader, items):
        self.__items = iter(items)
        self.__loader = loader

    def __iter__(self):
        return self

    def __next__(self):
        next_item = next(self.__items)
        return self.__loader.construct_object(next_item, deep=True)


def parallel_pipeline_loader(cls):
    """
    Loader for ParallelPipeline tag
    """

    def wrapped(loader, node):
        LoaderLogger.get_logger().debug('Parallel pipeline loader executed')
        if (not isinstance(node, SequenceNode)) and (not isinstance(node, MappingNode)):
            raise ConstructorError(None, None,
                                   "expected a sequence or mapping node, but found %s" % node.id,
                                   node.start_mark)
        if isinstance(node, SequenceNode):
            iterable = ParallelNodeIterator(loader, node.value)
            return cls(iterable, IterableType.SERIAL)
        if isinstance(node, MappingNode):
            iterable = ParallelNodeIterator(loader, [val[0] for val in node.value])
            return cls(iterable, IterableType.PARALLEL)

    return wrapped


def parallel_stages_loader(cls):
    """
    Loader for ParallelStages tag
    """
    def wrapped(loader, node):
        LoaderLogger.get_logger().debug('Parallel stages loader executed')
        if not isinstance(node, SequenceNode):
            raise ConstructorError(None, None,
                                   "expected a sequence node, but found %s" % node.id,
                                   node.start_mark)
        iterable = ParallelNodeIterator(loader, node.value)
        return cls(iterable, IterableType.PARALLEL)

    return wrapped

