# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

from typing import Iterable

from mldev.experiment import *
from mldev.utils import *
from mldev import logger


# todo: implement reading spec from stages
from stage import ParallelStage

#todo: not implemented yet

@experiment_tag(name='ParallelStage')
class ParallelStageSpec:

    def __init__(self, stage, upstream):
        self.stage = stage
        self.upstream = upstream

    def __call__(self, name, *args, experiment={}, **kwargs):
        self.stage(name, args, experiment, kwargs)

    def prepare(self, stage_name):
        self.stage.prepare(stage_name)

    def run(self, stage_name):
        self.stage.run(stage_name)

    @property
    def name(self):
        return self.stage.name

    def __str__(self):
        # todo
        return 'stage parallel extension'

    def __repr__(self):
        # todo
        return 'stage parallel extension'


# todo: implement properly
class StageSpecStageGeneratorFactory:

    def __init__(self, iter):
        self.iter = iter

    def __construct_wrapper_from_stage_spec(self, stage_spec):
        return ParallelStage(stage_spec.stage)

    def __construct_wrapper_from_stage(self, stage):
        return ParallelStage(stage)

    def create(self):
        for stage in self.iter:
            if isinstance(stage, ParallelStageSpec):
                stage_wrapper = self.__construct_wrapper_from_stage_spec(stage)
                yield stage_wrapper
            # todo: предполагает, что стейдж просто коллабл, можно добавить проверку типа
            if hasattr(stage, '__call__'):
                stage_wrapper = self.__construct_wrapper_from_stage(stage)
                yield stage_wrapper
