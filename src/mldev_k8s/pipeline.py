# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Pipeline module
==================

Contains experiment tag for parallel pipeline execution

* :py:class:`ParallelPipeline`
    Experiment tag for parallel pipeline execution, supports DSL for stage dependencies specification.
    See also `graph_spec.GraphSpecStageGeneratorFactory`.

"""

import itertools
from typing import Iterable

from mldev.experiment import *

from mldev_k8s.container import CoordinatorRunner
from mldev_k8s.coordination import PipelineTraverseFinished
from mldev_k8s.graph_spec import GraphSpecStageGeneratorFactory
from mldev_k8s.loaders import parallel_pipeline_loader, IterableType
from mldev_k8s.logs import CoordinatorLogFactory


@experiment_tag(loader=parallel_pipeline_loader)
class ParallelPipeline:
    """
    Experiment tag for parallel pipeline execution.

    :param arg: child nodes on top level in a tree-like dependencies specification structure
    :param iterable_type: iterable type for child nodes, can be SERIAL or PARALLEL
    """

    logger = None

    def __init__(self, arg: Iterable, iterable_type: IterableType):
        self.args = list(itertools.tee(arg, 3))[1:]
        self.arg = None
        self.iter_num = 0
        self.initial_arg = arg
        self.unpacked_arg = None
        self.name = 'root'
        self.dependencies = []
        self.iterable_type = iterable_type
        self.parent = None
        self.coordinator_snd_channel = None

    def __get_logger(self):
        if not ParallelPipeline.logger:
            ParallelPipeline.logger = CoordinatorLogFactory().get_logger(self.__class__.__name__)
        return ParallelPipeline.logger

    def __iter__(self):
        if self.iter_num >= 2:
            raise RuntimeError('Cant iterate over parallel pipeline more that 2 times, bug')
        self.__get_logger().debug('Parallel pipeline __iter__ is called')
        # assumes that multiple iterating over stages inside parallel pipeline is possible,
        # that is why it is necessary to reset inner state; without it __next__ call further will have no sense
        self.unpacked_arg = None
        self.arg = self.args[self.iter_num]
        self.iter_num += 1
        return self

    def __next__(self):
        try:
            self.__get_logger().debug('Parallel pipeline __next__ is called')
            if not self.unpacked_arg:
                self.coordinator_snd_channel = CoordinatorRunner().run_or_get()[0]
                self.unpacked_arg = GraphSpecStageGeneratorFactory(self.arg, self, self.iterable_type).create()
            return next(self.unpacked_arg)
        except StopIteration:
            self.coordinator_snd_channel.send_nowait(PipelineTraverseFinished())
            raise StopIteration
