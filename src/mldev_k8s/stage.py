# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Stage module
==================

Contains proxy for experiment stage used for execution in different environments

* :py:class:`ParallelStage`
    Wraps experiment stage to take control over actual stage execution .

"""

from mldev_k8s.container import CoordinatorRunner
from mldev_k8s.coordination import CallStageMessage, StageMethodCall
from mldev_k8s.logs import CoordinatorLogFactory


class ParallelStageLogger:
    logger = None

    @staticmethod
    def get_logger(name):
        if not ParallelStageLogger.logger:
            ParallelStageLogger.logger = CoordinatorLogFactory().get_logger(name)
        return ParallelStageLogger.logger


class ParallelStage:
    """
    Proxy for experiment stage. Instead actual stage execution it passes messages
    about methods that were called to coordinator.

    Its interface is same as BasicStage interface.

    :param stage: underlying experiment stage
    :param upstream_stages_names: upstream stages names
    """

    logger = None

    def __init__(self, stage, upstream_stages_names):
        self.stage = stage
        self.stage_name = stage.name
        self.name = stage.name
        self.upstream_stages_names = upstream_stages_names
        self.coordinator_snd_channel = CoordinatorRunner().run_or_get()[0]

    def __get_logger(self):
        if not ParallelStage.logger:
            ParallelStage.logger = CoordinatorLogFactory().get_logger(self.__class__.__name__)
        return ParallelStage.logger

    def __call__(self, name, *args, experiment={}, **kwargs):
        self.__get_logger().debug(f'Parallel stage {self.stage_name} __call__ is called')
        self.coordinator_snd_channel.send_nowait(CallStageMessage(self.stage_name, StageMethodCall.RUN))

    def prepare(self, stage_name):
        self.__get_logger().debug(f'Parallel stage {self.stage_name} prepare is called')
        self.coordinator_snd_channel.send_nowait(CallStageMessage(self.stage_name, StageMethodCall.PREPARE))


class DummyParallelStage:

    def __init__(self, stage):
        self.stage = stage
        self.name = stage.name

    def __call__(self, name, *args, experiment={}, **kwargs):
        pass

    def prepare(self, stage_name):
        pass
