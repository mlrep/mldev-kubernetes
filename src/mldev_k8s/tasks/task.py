# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Tasks module
==================

Module contains task handling logic. Task handler is created for each executed experiment stage.

* :py:class:`TaskRef`
    Data object containing all information related to task.
* :py:class:`Coordinator`
    Performs setup and start of coordinated tasks.
* :py:class:`WorkflowState`
    Handles current experiment execution state, mainly for reporting goals.
    Implemented as State pattern.

"""

import abc
import enum
from abc import ABC
from datetime import datetime, timedelta

from mldev_k8s.channels import WouldBlock, ReceiveChannel, SendChannel


class CoordinationTaskMessage(ABC):
    pass


class TaskHandlerStatusChangedMessage(CoordinationTaskMessage):

    def __init__(self, stage_name, status):
        self.stage_name = stage_name
        self.status = status


class TaskHandlerHeartBeatMessage(CoordinationTaskMessage):

    def __init__(self, stage_name):
        self.stage_name = stage_name


class ParallelMessage(ABC):
    pass


# todo: не используется, убрать вместе с методом process_control_message
class ControlMessage(ParallelMessage):
    pass


class ExecutorMessage(ParallelMessage):
    pass


class ExecutorResultType(enum.Enum):
    SUCCESS = 'success'
    ERROR = 'error'


class ExecutorFinished(ExecutorMessage):

    def __init__(self, result_type):
        self.result_type = result_type


class ExecutorHeartbeat(ExecutorMessage):
    pass


class UpstreamMessage(ParallelMessage):
    pass


class UpstreamFinished(UpstreamMessage):

    def __init__(self, upstream_stage_name, upstream_status_name):
        self.upstream_stage_name = upstream_stage_name
        self.upstream_status_name = upstream_status_name


class UpstreamHeartBeat(UpstreamMessage):

    def __init__(self, upstream_stage_name):
        self.upstream_stage_name = upstream_stage_name


class TaskHandlerState(ABC):

    def __init__(self, task_handler):
        self.task_handler = task_handler

    @abc.abstractmethod
    def process_control_msg(self, control_msg: ControlMessage):
        pass

    @abc.abstractmethod
    def process_upstream_msg(self, upstream_msg: UpstreamMessage):
        pass

    @abc.abstractmethod
    def process_executor_msg(self, executor_msg):
        pass

    @abc.abstractmethod
    def process_state(self):
        pass

    @property
    @abc.abstractmethod
    def name(self):
        pass


class WaitingState(TaskHandlerState):

    def __init__(self, task_handler):
        super().__init__(task_handler)

    @classmethod
    def state_name(cls):
        return 'WAITING'

    @property
    def name(self):
        return WaitingState.state_name()

    def process_control_msg(self, control_msg: ControlMessage):
        raise RuntimeError('Unsupported message for this state')

    def process_upstream_msg(self, upstream_msg):
        if type(upstream_msg) is UpstreamFinished:
            finished_stage = upstream_msg.upstream_stage_name
            finished_state = upstream_msg.upstream_status_name
            self.task_handler.log_factory.get_logger(self.task_handler.stage_name).info(
                f'TaskHandler {self.task_handler.stage_name} '
                f'in Waiting State: received UpstreamFinished msg from '
                f'{finished_stage} with state {finished_state}')
            if finished_stage in self.task_handler.upstream_unfinished_stages_names:
                self.task_handler.upstream_unfinished_stages_names.remove(finished_stage)
            else:
                raise RuntimeError(f'{finished_stage} not expected to be finished')

            new_state = None
            if finished_state != CompletedState.state_name():
                new_state = WillNotRunState(self.task_handler)
            else:
                if len(self.task_handler.upstream_unfinished_stages_names) == 0:
                    new_state = ProcessingState(self.task_handler)
            if new_state:
                self.task_handler.set_new_state_and_notify_coordinator(new_state)
            return
        if type(upstream_msg) is UpstreamHeartBeat:
            stage_name = upstream_msg.upstream_stage_name
            self.task_handler.log_factory.get_logger(self.task_handler.stage_name).info(
                f'TaskHandler {self.task_handler.stage_name} '
                f'in Waiting State: received UpstreamHeartBeat msg from '
                f'{stage_name}')
            if stage_name not in self.task_handler.upstream_unfinished_stages_names:
                self.task_handler.log_factory.get_logger(self.task_handler.stage_name).warning(
                    f'TaskHandler {self.task_handler.stage_name} '
                    f'in Waiting State: received UpstreamHeartBeat msg from '
                    f'{stage_name}, but stage is not marked active, skipped; probably bug')
                return
            self.task_handler.upstream_stages_heartbeats[stage_name] = datetime.utcnow()
            return

        raise RuntimeError('Unsupported message')

    def process_executor_msg(self, executor_msg):
        raise RuntimeError('Unsupported message for this state')

    def process_state(self):
        self.task_handler.send_heartbeat_to_coordinator()
        self.task_handler.send_heartbeats_to_downstream_task_handlers()

        if not self.task_handler.check_active_upstream_task_handlers_alive():
            self.task_handler.log_factory.get_logger(self.task_handler.stage_name).warning(
                f'TaskHandler {self.task_handler.stage_name} in Waiting State: '
                f'Timeout of one or more active upstream stages; will not run')
            new_state = WillNotRunState(self.task_handler)
            for channel in self.task_handler.downstream_snd_channels:
                msg = UpstreamFinished(self.task_handler.stage_name, new_state.name)
                channel.send_nowait(msg)
            self.task_handler.set_new_state_and_notify_coordinator(new_state)


class ProcessingState(TaskHandlerState):

    def __init__(self, task_handler):
        super().__init__(task_handler)
        task_handler.task_executor_runner.run_task_executor(task_handler.stage_name,
                                                            task_handler.from_executor_snd_channel,
                                                            task_handler.workflow_id)
        now = datetime.utcnow()
        self.task_handler.executor_heartbeat = now

    @classmethod
    def state_name(cls):
        return 'PROCESSING'

    @property
    def name(self):
        return ProcessingState.state_name()

    def process_control_msg(self, control_msg: ControlMessage):
        raise RuntimeError('Unsupported message for this state')

    def process_upstream_msg(self, upstream_msg: UpstreamMessage):
        raise RuntimeError('Unsupported message for this state')

    def process_executor_msg(self, executor_msg: ExecutorMessage):
        if type(executor_msg) is ExecutorFinished:
            self.task_handler.log_factory.get_logger(self.task_handler.stage_name).info(
                f'TaskHandler {self.task_handler.stage_name} in Processing State: '
                f'Received ExecutorFinished msg with {executor_msg.result_type}')
            new_state = None
            if executor_msg.result_type == ExecutorResultType.SUCCESS:
                new_state = CompletedState(self.task_handler)
            if executor_msg.result_type == ExecutorResultType.ERROR:
                new_state = FailedState(self.task_handler)

            for channel in self.task_handler.downstream_snd_channels:
                msg = UpstreamFinished(self.task_handler.stage_name, new_state.name)
                channel.send_nowait(msg)
            self.task_handler.set_new_state_and_notify_coordinator(new_state)
            return
        if type(executor_msg) is ExecutorHeartbeat:
            self.task_handler.log_factory.get_logger(self.task_handler.stage_name).debug(
                f'TaskHandler {self.task_handler.stage_name} in Processing State: '
                f'Received ExecutorHeartBeat msg')
            self.task_handler.executor_heartbeat = datetime.utcnow()
            return
        raise RuntimeError('Unsupported message')

    def process_state(self):
        self.task_handler.send_heartbeat_to_coordinator()
        self.task_handler.send_heartbeats_to_downstream_task_handlers()

        if not self.task_handler.check_executor_is_alive():
            self.task_handler.log_factory.get_logger(self.task_handler.stage_name).warning(
                f'TaskHandler {self.task_handler.stage_name} in Processing State: '
                f'Timeout of executor; finishing')
            new_state = FailedState(self.task_handler)
            for channel in self.task_handler.downstream_snd_channels:
                msg = UpstreamFinished(self.task_handler.stage_name, new_state.name)
                channel.send_nowait(msg)
            self.task_handler.set_new_state_and_notify_coordinator(new_state)


class CompletedState(TaskHandlerState):

    def __init__(self, task_handler):
        super().__init__(task_handler)

    @classmethod
    def state_name(cls):
        return 'COMPLETED'

    @property
    def name(self):
        return CompletedState.state_name()

    def process_control_msg(self, control_msg: ControlMessage):
        raise RuntimeError('Unsupported message for this state')

    def process_upstream_msg(self, upstream_msg: UpstreamMessage):
        raise RuntimeError('Unsupported message for this state')

    def process_executor_msg(self, executor_msg):
        raise RuntimeError('Unsupported message for this state')

    def process_state(self):
        pass


class WillNotRunState(TaskHandlerState):

    def __init__(self, task_handler):
        super().__init__(task_handler)

    def process_control_msg(self, control_msg: ControlMessage):
        raise RuntimeError('Unsupported message for this state')

    def process_upstream_msg(self, upstream_msg: UpstreamMessage):
        raise RuntimeError('Unsupported message for this state')

    def process_executor_msg(self, executor_msg):
        raise RuntimeError('Unsupported message for this state')

    @classmethod
    def state_name(cls):
        return 'WILL_NOT_START'

    @property
    def name(self):
        return WillNotRunState.state_name()

    def process_state(self):
        pass


class FailedState(TaskHandlerState):

    def __init__(self, task_handler):
        super().__init__(task_handler)

    def process_control_msg(self, control_msg: ControlMessage):
        raise RuntimeError('Unsupported message for this state')

    def process_upstream_msg(self, upstream_msg: UpstreamMessage):
        raise RuntimeError('Unsupported message for this state')

    def process_executor_msg(self, executor_msg):
        raise RuntimeError('Unsupported message for this state')

    def process_state(self):
        pass

    @classmethod
    def state_name(cls):
        return 'FAILED'

    @property
    def name(self):
        return FailedState.state_name()


COORDINATOR_HEARTBEAT_INTERVAL_SEC = 5
TASK_HANDLERS_HEARTBEAT_INTERVAL_SEC = 5
TASK_HANDLERS_TIMEOUT_SEC = 60
TASK_EXECUTOR_HEARTBEAT_INTERVAL_SEC = 5
TASK_EXECUTOR_TIMEOUT_SEC = 60


class TaskHandler:

    def __init__(self, task_handler_log_factory, stage_name, workflow_id, coordinator_rcv_channel: ReceiveChannel,
                 coordinator_snd_channel: SendChannel, from_executor_snd_channel: SendChannel,
                 executor_rcv_channel: ReceiveChannel, task_executor_runner, upstream_stages_names=None,
                 downstream_stages_names=None, upstream_rcv_channels=None, downstream_snd_channels=None):
        self.stage_name = stage_name
        self.workflow_id = workflow_id
        # todo: возможно, не нужно, нет управляющих сообщений от координатора (предполагалось для этого)
        self.coordinator_rcv_channel = coordinator_rcv_channel
        self.coordinator_snd_channel = coordinator_snd_channel
        self.executor_rcv_channel = executor_rcv_channel
        self.from_executor_snd_channel = from_executor_snd_channel
        self.task_executor_runner = task_executor_runner
        self.upstream_rcv_channels = upstream_rcv_channels
        self.downstream_snd_channels = downstream_snd_channels
        self.upstream_stages_names = upstream_stages_names
        self.downstream_stages_names = downstream_stages_names
        self.state = None
        self.upstream_unfinished_stages_names = None
        self.log_factory = task_handler_log_factory
        self.last_coordinator_heartbeat_sent = None
        self.last_downstream_task_handlers_heartbeats_sent = None
        now = datetime.utcnow()
        self.upstream_stages_heartbeats = {stage: now for stage in self.upstream_stages_names}
        self.executor_heartbeat = None

        if self.upstream_stages_names is None or len(self.upstream_stages_names) == 0:
            self.state: TaskHandlerState = ProcessingState(self)
            self.upstream_unfinished_stages_names = set()
            msg = TaskHandlerStatusChangedMessage(self.stage_name, self.state.name)
            self.coordinator_snd_channel.send_nowait(msg)
        else:
            self.state: TaskHandlerState = WaitingState(self)
            self.upstream_unfinished_stages_names = set(self.upstream_stages_names)

    def receive_from_channel_and_process(self, channel: ReceiveChannel, handler):
        try:
            msg = channel.receive_nowait()
        except WouldBlock:
            return False
        handler(msg)
        return True

    def is_in_terminal_state(self):
        return type(self.state) in {FailedState, WillNotRunState, CompletedState}

    def send_heartbeat_to_coordinator(self):
        now = datetime.utcnow()
        time_elapsed = now - self.last_coordinator_heartbeat_sent if self.last_coordinator_heartbeat_sent else \
            timedelta(seconds=COORDINATOR_HEARTBEAT_INTERVAL_SEC)
        if time_elapsed.seconds < COORDINATOR_HEARTBEAT_INTERVAL_SEC:
            return
        msg = TaskHandlerHeartBeatMessage(self.stage_name)
        self.coordinator_snd_channel.send_nowait(msg)
        self.last_coordinator_heartbeat_sent = now

    def send_heartbeats_to_downstream_task_handlers(self):
        now = datetime.utcnow()
        time_elapsed = now - self.last_downstream_task_handlers_heartbeats_sent \
            if self.last_downstream_task_handlers_heartbeats_sent else \
            timedelta(seconds=TASK_HANDLERS_HEARTBEAT_INTERVAL_SEC)
        if time_elapsed.seconds < TASK_HANDLERS_HEARTBEAT_INTERVAL_SEC:
            return
        for channel in self.downstream_snd_channels:
            msg = UpstreamHeartBeat(self.stage_name)
            channel.send_nowait(msg)
        self.last_downstream_task_handlers_heartbeats_sent = now

    def check_active_upstream_task_handlers_alive(self):
        now = datetime.utcnow()
        for stage in self.upstream_unfinished_stages_names:
            time_elapsed = now - self.upstream_stages_heartbeats[stage]
            if time_elapsed.seconds > TASK_HANDLERS_TIMEOUT_SEC:
                return False
        return True

    def check_executor_is_alive(self):
        now = datetime.utcnow()
        time_elapsed = now - self.executor_heartbeat
        if time_elapsed.seconds > TASK_EXECUTOR_TIMEOUT_SEC:
            return False
        return True

    def set_new_state_and_notify_coordinator(self, new_state):
        msg = TaskHandlerStatusChangedMessage(self.stage_name, new_state.name)
        self.coordinator_snd_channel.send_nowait(msg)
        self.state = new_state
        self.log_factory.get_logger(self.stage_name).info(
            f'TaskHandler {self.stage_name} now in {self.state.name} state')
