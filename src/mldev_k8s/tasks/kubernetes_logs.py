import math
import time
from datetime import datetime
from typing import Tuple, Optional, Generator, List

from dateutil.parser import isoparse
from kubernetes.client import CoreV1Api
from urllib3 import HTTPResponse
from urllib3.exceptions import ReadTimeoutError

from mldev_k8s import config

"""
Kubernetes tasks logs module
==================

Module reads logs from Kubernetes pod.

* :py:class:`PodLogsConsumer`
    Iterator, reads logs items each as raw byte row. 
* :py:class:`ReadLogsTimeoutLoggingWrapper`
    Wraps :py:class:`PodLogsConsumer` to handle ReadTimeoutError exception.
* :py:class:`LogItemsCache`
     Is used to determine if new log items already exists (has been already read) to support 
     logs reading by fragments.
* :py:class:`PodLogsReader`
    Reads pod log by fragments, maintains log reading state.
"""


class PodLogsConsumer:
    """
    Iterator, reads logs items each as raw byte row.

    Inspired by Airflow's pod_manager.py:
    https://github.com/apache/airflow/blob/ac46902154c060246dec942f921f7670015e6031/airflow/providers/cncf/kubernetes/utils/pod_manager.py

    :param response: HTTPResponse for pod logs streaming, result of read_namespaced_pod_log with Follow=True
    """

    def __init__(
            self,
            response: HTTPResponse
    ):
        self.response = response

    def __iter__(self) -> Generator[bytes, None, None]:
        r"""The generator yields log items divided by the '\n' symbol."""
        incomplete_log_item: List[bytes] = []
        for data_chunk in self.response.stream(amt=None, decode_content=True):
            if b"\n" in data_chunk:
                log_items = data_chunk.split(b"\n")
                yield from self._extract_log_items(incomplete_log_item, log_items)
                incomplete_log_item = self._save_incomplete_log_item(log_items[-1])
            else:
                incomplete_log_item.append(data_chunk)
        if incomplete_log_item:
            yield b"".join(incomplete_log_item)

    @staticmethod
    def _extract_log_items(incomplete_log_item: List[bytes], log_items: List[bytes]):
        yield b"".join(incomplete_log_item) + log_items[0] + b"\n"
        for x in log_items[1:-1]:
            yield x + b"\n"

    @staticmethod
    def _save_incomplete_log_item(sub_chunk: bytes):
        return [sub_chunk] if [sub_chunk] else []


class ReadLogsTimeoutLoggingWrapper:
    """
    Wraps :py:class:`PodLogsConsumer` to handle ReadTimeoutError exception.
    """

    def __init__(self, generator: PodLogsConsumer, log_factory, name):
        self.__generator = generator
        self.__log_factory = log_factory
        self.__name = name

    def __iter__(self):
        try:
            yield from self.__generator
        except ReadTimeoutError:
            self.__log_factory.get_logger(self.__name).debug('Read timeout while reading pod logs...')
            return


class LogItemsCache:
    """
    Contains last read log fragment. Supports log reading by fragments.
    Is used to determine if new log items already exists (has been already read).
    """

    def __init__(self, log_factory, name):
        self.__cache_log: Optional[List[Tuple[datetime, str]]] = None
        # если __start_new_fragment = True, то вызов check_and_process_new_log_item
        # осуществляется с первым элементом текущего фрагмента;
        # используется для определения, был ли в текущем фрагменте прочитан
        # хотя бы один новый элемент (включая случай пустого фрагмента)
        self.__start_new_fragment = False
        # счетчик - сколько подряд фрагментов логов не было прочитано ни одного нового элемента
        # (включая случай пустого фрагмента)
        self.__no_new_log_items_fragments_counter = 0
        self.__log_factory = log_factory
        self.__name = name

    def prepare_for_new_fragment(self):
        """
        Must be called before processing new fragment.
        """
        self.__start_new_fragment = True

    def new_fragment_finished(self):
        """
        Must be called after processing of last item of current fragment.
        """
        # если после обработки элементов лога последнего фрагмента
        # self.__start_new_fragment == True, то, значит в лог ничего не добавилось
        if self.__start_new_fragment:
            self.__no_new_log_items_fragments_counter += 1
        else:
            self.__no_new_log_items_fragments_counter = 0

    def any_log_items_exist(self) -> bool:
        """
        Determines if any log items were read.
        """
        return self.__cache_log is not None

    def get_no_new_log_items_fragments_counter(self):
        """
        Returns counter of how many successive log fragments were empty or had not new log items.
        """
        return self.__no_new_log_items_fragments_counter

    # todo: пояснить смысл констант в kubernetes.py
    def __check_if_log_item_is_newer_than_cache(self, log_item: Tuple[datetime, str]):
        # проверяет, что новый элемент лога (из нового фрагмента) отсутствует и является более новым,
        # чем элементы прочитанного ранее фрагмента лога;
        last_existing_log_item = self.__cache_log[-1]
        if last_existing_log_item[0] > log_item[0]:
            return False
        if last_existing_log_item[0] < log_item[0]:
            return True
        current_existing_item = last_existing_log_item
        index = len(self.__cache_log) - 1
        while index >= 0 and current_existing_item[0] == log_item[0]:
            if current_existing_item[1] == log_item[1]:
                return False
            index -= 1
            current_existing_item = self.__cache_log[index]
        return True

    def check_and_process_new_log_item(self, new_log_item: Tuple[datetime, str]) -> bool:
        """
        Checks if new log item was not already processed previously.

        Also stores new log item in inner log cache to perform checks for future fragments log items.
        """
        # если текущий элемент лога является первым для текущего фрагмента логов
        if self.__start_new_fragment:
            # если это вообще первый элемент лога в сеансе (кэш пуст)
            if not self.__cache_log:
                # инициализируем кэш одним этим элементом,
                # переключаем self.__start_new_fragment = False -
                # следующие элементы фрагмента лога не будут считаться первыми в текущем фрагменте
                self.__cache_log = [new_log_item]
                self.__start_new_fragment = False
                return True
            # если кэш уже есть, значит в нем находятся элементы лога из предудыщего фрагмента
            # провряем, что обрабатываемый элемент лога отсутствует в предыдущем фрагменте и более новый
            # нужно, поскольку может быть пересечение фрагментов
            log_item_is_newer_than_cache = self.__check_if_log_item_is_newer_than_cache(new_log_item)
            if not log_item_is_newer_than_cache:
                # если элемент уже есть или более старый, ничего не делаем,
                # возвращаем False - элемент уже есть в кэше, то есть он уже был обработан
                return False
            else:
                # если элемента нет и он более новый, то считаем его первым новым элементом
                # и переинициализируем кэш им, забывая предыдущий фрагмент лога;
                # возвращаем True - элемент не был ранее обработан
                self.__cache_log = [new_log_item]
                self.__start_new_fragment = False
                return True
        # если текущий элемент лога не является первым для текущего фрагмента логов
        else:
            # добавляем элементв в кэш лога, чтобы он мог участвовать в последующих проверках;
            # возвращаем True - элемент не был ранее обработан
            self.__cache_log.append(new_log_item)
            return True


class PodLogsReader:
    """
    Reads pod log by fragments, maintains log reading state.
    """

    def __init__(self, core_api_client: CoreV1Api, tasks_log_factory, log_factory, name, pod_name,
                 pod_phase, wait_for_logs_after_pod_finished_sec, pod_log_request_timeout_sec):
        self.__core_api_client = core_api_client
        self.__tasks_log_factory = tasks_log_factory
        self.__log_factory = log_factory

        self.__name = name
        self.__pod_name = pod_name
        self.__pod_phase = pod_phase

        self.__wait_for_logs_after_pod_finished_sec = wait_for_logs_after_pod_finished_sec
        self.__pod_log_request_timeout_sec = pod_log_request_timeout_sec

        self.__last_log_timestamp = None
        self.__pod_finish_timestamp = None

        self.__log_items_cache = LogItemsCache(log_factory, name)

        self.pod_logs_contain_error = False

    def __parse_str_log_line(self, str_log_line) -> Tuple[datetime, str]:
        index = str_log_line.find(" ")
        if index == -1:
            raise Exception("timestamp doesnt exist in log line, bug")
        try:
            date_time_str = str_log_line[:index]
            log_message = str_log_line[index + 1:].rstrip()
            date_time = isoparse(date_time_str)
        except ValueError as e:
            raise Exception("Cant parse str log line timestamp") from e
        return date_time, log_message

    def __get_read_pod_log_stream_response(self):
        if not self.__last_log_timestamp:
            since_seconds = None
        else:
            now = time.time()
            since_seconds = math.ceil((now - self.__last_log_timestamp))

        # Follow=True и _preload_content=False позволяет читать логи потоком -
        # позволяет не загружать сразу большой объем данных;
        # _request_timeout и since_seconds позволяет читать логи фрагментами -
        # позволяет "не зависнуть" в ожидании логов, учитывает возможные сбои при длительном соединении,
        # возвращает управление в вызывающий процесс, не используя async
        logs_http_response = self.__core_api_client.read_namespaced_pod_log(name=self.__pod_name,
                                                                            namespace=config.TASK_K8S_NAMESPACE,
                                                                            _request_timeout=
                                                                            self.__pod_log_request_timeout_sec,
                                                                            _preload_content=False,
                                                                            since_seconds=since_seconds,
                                                                            timestamps=True,
                                                                            follow=True)

        pod_logs_consumer = PodLogsConsumer(logs_http_response)

        return pod_logs_consumer

    def __check_errors_in_log_message(self, log_message):
        if log_message.startswith('ERROR:'):
            self.pod_logs_contain_error = True
            self.__log_factory.get_logger(self.__name).error('Error in task log')

    def __log_log_message(self, log_message):
        self.__tasks_log_factory.get_logger(self.__name).info(log_message)

    def read_and_process_log_fragment(self):
        pod_logs_consumer = self.__get_read_pod_log_stream_response()
        wrapper = ReadLogsTimeoutLoggingWrapper(pod_logs_consumer, self.__log_factory, self.__name)

        self.__log_items_cache.prepare_for_new_fragment()

        for binary_log_item in wrapper:
            if not binary_log_item:
                continue
            str_log_line = binary_log_item.decode("utf-8", errors="backslashreplace")
            date_time, log_message = self.__parse_str_log_line(str_log_line)
            check_result = self.__log_items_cache.check_and_process_new_log_item((date_time, log_message))
            if check_result:
                self.__log_log_message(log_message)
                self.__last_log_timestamp = date_time.timestamp()
                self.__check_errors_in_log_message(log_message)

        self.__log_items_cache.new_fragment_finished()

    def set_current_pod_phase(self, pod_phase):
        self.__pod_phase = pod_phase
        if pod_phase != 'Running' and not self.__pod_finish_timestamp:
            self.__pod_finish_timestamp = time.time()

    def need_read_logs(self):
        # важно дождаться и прочитать все логи, которые могут поступить с задержкой даже
        # после завершения работы пода

        # всегда читаем логи, если под работает
        if self.__pod_phase == 'Running':
            return True
        # если логи уже читались, а сейчас под завершен, и последний фрагмент оказался пустой,
        # то дальше ждать не нужно
        if self.__log_items_cache.any_log_items_exist() and self.__log_items_cache\
                .get_no_new_log_items_fragments_counter() >= 1:
            return False

        # ждем появления логов в завершенном поде не более заданного времени
        if self.__pod_finish_timestamp + self.__wait_for_logs_after_pod_finished_sec > \
                self.__last_log_timestamp:
            return True
        return False
