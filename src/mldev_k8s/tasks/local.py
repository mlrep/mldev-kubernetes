import os
import sys
from subprocess import CalledProcessError

#from mldev.utils import *

import mldev_k8s.config as config
from mldev_k8s.tasks.task import ExecutorFinished, ExecutorResultType

# todo: not implemented yet

class LocalTaskExecutor:

    def __init__(self, name, handler_snd_channel, workflow_id):
        self.name = name
        self.handler_snd_channel = handler_snd_channel
        self.workflow_id = workflow_id

    def run(self):
        sys.stdout = open(str(os.getpid()) + ".out", "a")
        sys.stderr = open(str(os.getpid()) + "_error.out", "a")
        command = f'mldev run {self.name} -f {config.EXPERIMENT_FILE}'
        try:
            completed_process = exec_command(command)
            # todo: use logging instead
            print(f'Task {self.name}: sending success msg')
            self.handler_snd_channel.send_nowait(ExecutorFinished(ExecutorResultType.SUCCESS))
        except CalledProcessError as e:
            print(f'Task {self.name}: sending failed msg')
            self.handler_snd_channel.send_nowait(ExecutorFinished(ExecutorResultType.ERROR))