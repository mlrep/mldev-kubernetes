# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Kubernetes tasks module
==================

Module executes tasks on Kubernetes cluster.

* :py:class:`KubernetesTaskExecutor`
    Executes tasks on Kubernetes cluster

"""

import configparser
import time
from datetime import datetime, timedelta

import kubernetes
from kubernetes import client
from kubernetes.client import Configuration, ApiException, OpenApiException

import mldev_k8s.config as config
from mldev_k8s.tasks.kubernetes_logs import PodLogsReader
from mldev_k8s.tasks.task import ExecutorFinished, ExecutorResultType, TASK_EXECUTOR_HEARTBEAT_INTERVAL_SEC, \
    ExecutorHeartbeat


class TaskFailedException(Exception):
    pass


class JobManifestFactory:

    def __init__(self, name, workflow_id):
        self.name = name
        self.workflow_id = workflow_id

    def __get_container_command_args(self):
        # return ['-c', 'while true; do sleep 10 && echo 111; done']
        # return ['-c', 'for (( a = 1; a <= 20; a++)); do sleep 10 && echo "$a"; done']
        return ['-c', './venv/bin/mldev run -f ./run-stage-env.yml']

    def __get_job_manifest(self, job_name):

        if config.TASK_DOCKER_IMAGE_REGISTRY:
            image = config.TASK_DOCKER_IMAGE_REGISTRY + '/' + config.TASK_DOCKER_IMAGE_NAME
            image_pull_policy = config.TASK_K8S_IMAGE_PULL_POLICY
        else:
            image = config.TASK_DOCKER_IMAGE_NAME
            image_pull_policy = 'Never'

        job_manifest = {
            'apiVersion': 'batch/v1',
            'kind': 'Job',
            'metadata': {
                'name': job_name,
                'labels': {
                    'mldev': 'mldev',
                    'mldev-workflow': f'mldev-wf-{self.workflow_id}'
                }
            },
            'spec': {
                'template': {
                    'spec': {
                        'containers': [
                            {
                                'name': 'mldev-task-container',
                                'image': image,
                                'imagePullPolicy': image_pull_policy,
                                'command': ['/bin/bash'],
                                'args': self.__get_container_command_args(),
                                # todo: make parameter
                                # 'resources': {
                                #     'limits': {
                                #         'memory': '256M',
                                #         'cpu': 0.2
                                #     }
                                # },
                                'env': [{
                                    'name': 'PARALLEL_STAGE_NAME',
                                    'value': self.name
                                }, {
                                    'name': 'PARALLEL_EXPERIMENT_FILE',
                                    'value': config.EXPERIMENT_FILE
                                }, {
                                    'name': 'PYTHONPATH',
                                    'value': config.TASK_K8S_PYTHONPATH
                                }]
                            }
                        ],
                        # todo: продумать с рестартами
                        'restartPolicy': 'Never'
                    }
                }
            }
        }
        return job_manifest

    def __add_pvc_volume_mount(self, job_manifest):
        container_manifest = job_manifest['spec']['template']['spec']['containers'][0]
        container_manifest['volumeMounts'] = [
            {
                'name': 'task-pv',
                'mountPath': '/experiment/results'
            }
        ]
        job_manifest['spec']['template']['spec']['volumes'] = [
            {
                'name': 'task-pv',
                'persistentVolumeClaim': {
                    'claimName': config.TASK_STORAGE_VOLUME_PVC_NAME
                }
            }
        ]
        return job_manifest

    def __add_image_pull_secret(self, job_manifest):
        if not config.TASK_K8S_IMAGE_PULL_SECRET:
            return job_manifest
        job_manifest['spec']['template']['spec']['imagePullSecrets'] = [
            {
                'name': config.TASK_K8S_IMAGE_PULL_SECRET
            }]
        return job_manifest


    # todo fix
    def __update_command_args_to_mount_s3(self, pod_manifest):
        command_args = self.__get_container_command_args()
        new_command = [command_args[0], '/install_mldev/mldev-k8s-base-s3-mount.sh && ' + command_args[1]]
        pod_manifest['spec']['containers'][0]['args'] = new_command
        return pod_manifest

    # todo fix
    def __update_env_vars_to_mount_s3(self, pod_manifest):
        envs = pod_manifest['spec']['containers'][0]['env']
        bucket_name = {'name': 'S3_BUCKET_NAME', 'value': config.TASK_STORAGE_S3_BUCKET_NAME}
        host = {'name': 'S3_HOST', 'value': config.TASK_STORAGE_S3_HOST}
        mount_dir = {'name': 'S3_MOUNT_DIR', 'value': '/experiment/results'}
        envs += [bucket_name, host, mount_dir]

        if config.TASK_STORAGE_S3_CREDENTIALS == config.S3Credentials.FILE:
            cfg = configparser.ConfigParser()
            cfg.read(config.TASK_STORAGE_S3_CREDENTIALS_FILE)
            aws_access_key_id = cfg['default']['aws_access_key_id']
            aws_secret_access_key = cfg['default']['aws_secret_access_key']
            access_key_id = {'name': 'S3_ACCESS_KEY_ID', 'value': aws_access_key_id}
            secret_access_key = {'name': 'S3_SECRET_ACCESS_KEY', 'value': aws_secret_access_key}
            envs += [access_key_id, secret_access_key]
        else:
            raise Exception('only s3 file credentials are supported now')

        return pod_manifest

    def __set_privileged_security_context(self, job_manifest):
        container_manifest = job_manifest['spec']['template']['spec']['containers'][0]
        container_manifest['securityContext'] = {'privileged': True}
        return job_manifest

    def create_job_manifest(self, job_name):
        job_manifest = self.__get_job_manifest(job_name)
        job_manifest = self.__add_image_pull_secret(job_manifest)
        if config.TASK_K8S_STORAGE_TYPE == config.StorageType.VOLUME:
            job_manifest = self.__add_pvc_volume_mount(job_manifest)
        if config.TASK_K8S_STORAGE_TYPE == config.StorageType.S3:
            job_manifest = self.__update_command_args_to_mount_s3(job_manifest)
            job_manifest = self.__update_env_vars_to_mount_s3(job_manifest)
            job_manifest = self.__set_privileged_security_context(job_manifest)
        return job_manifest


WAIT_JOB_IS_CREATED_TIMEOUT_SEC = 30
WAIT_JOB_IS_CREATED_SLEEP_TIME_SEC = 5
WAIT_POD_IS_RUNNING_TIMEOUT_SEC = 600
WAIT_POD_IS_RUNNING_SLEEP_TIME_SEC = 5
WAIT_GET_POD_NAME_TIMEOUT_SEC = 60
WAIT_GET_POD_NAME_SLEEP_TIME_SEC = 5

# сколько ждать появления логов после завершения пода
WAIT_FOR_LOGS_AFTER_POD_FINISHED_SEC = 60
# время соединения и чтения одного фрагмента лога пода
POD_LOG_REQUEST_TIMEOUT_SEC = 2
WAIT_POD_UNTIL_FINISH_SLEEP_TIME_SEC = 5


class KubernetesTaskExecutor:
    """
    Executes tasks on Kubernetes cluster.

    :param name: task (stage) name
    :param handler_snd_channel: channel to send messages to task handler
    :param workflow_id: related workflow id
    :param tasks_log_factory: logging factory for task logs
    :param task_executor_log_factory: logging factory for task executor logs
    """

    def __init__(self, name, handler_snd_channel, workflow_id,
                 tasks_log_factory, task_executor_log_factory):
        self.name = name
        self.handler_snd_channel = handler_snd_channel
        self.workflow_id = workflow_id
        self.tasks_log_factory = tasks_log_factory
        self.log_factory = task_executor_log_factory
        self.last_handler_heartbeat_sent = None
        self.batch_api_client = None
        self.core_api_client = None
        self.job_manifest_factory = JobManifestFactory(name, workflow_id)

    def __send_heartbeat_to_handler(self):
        now = datetime.utcnow()
        time_elapsed = now - self.last_handler_heartbeat_sent if self.last_handler_heartbeat_sent else \
            timedelta(seconds=TASK_EXECUTOR_HEARTBEAT_INTERVAL_SEC)
        if time_elapsed.seconds < TASK_EXECUTOR_HEARTBEAT_INTERVAL_SEC:
            return
        msg = ExecutorHeartbeat()
        self.handler_snd_channel.send_nowait(msg)
        self.last_handler_heartbeat_sent = now

    def __create_job(self, job_manifest):
        self.log_factory.get_logger(self.name).debug('Creating job...')
        try:
            self.batch_api_client.create_namespaced_job(body=job_manifest, namespace=config.TASK_K8S_NAMESPACE)
            self.__send_heartbeat_to_handler()
        except OpenApiException as e:
            self.log_factory.get_logger(self.name).error(e, exc_info=True)
            raise TaskFailedException

    def __wait_job_is_created(self, job_name):
        self.log_factory.get_logger(self.name).debug('Waiting for job creation...')
        start = datetime.utcnow()
        time_elapsed_sec = 0
        while time_elapsed_sec < WAIT_JOB_IS_CREATED_TIMEOUT_SEC:
            try:
                job = self.batch_api_client.read_namespaced_job(job_name, namespace=config.TASK_K8S_NAMESPACE)
                self.__send_heartbeat_to_handler()
                return job
            except ApiException as e:
                if e.status != 404:
                    self.log_factory.get_logger(self.name).error(e, exc_info=True)
                    raise TaskFailedException
                self.log_factory.get_logger(self.name).debug('Waiting for job creation, received code 404')
            except OpenApiException as e:
                self.log_factory.get_logger(self.name).error(e, exc_info=True)
                raise TaskFailedException
            time.sleep(WAIT_JOB_IS_CREATED_SLEEP_TIME_SEC)
            now = datetime.utcnow()
            time_elapsed_sec = (now - start).seconds

    def __wait_pod_is_running(self, pod_name):
        self.log_factory.get_logger(self.name).debug('Waiting for pod running...')
        start = datetime.utcnow()
        time_elapsed_sec = 0
        while time_elapsed_sec < WAIT_POD_IS_RUNNING_TIMEOUT_SEC:
            try:
                pod = self.core_api_client.read_namespaced_pod_status(pod_name, namespace=config.TASK_K8S_NAMESPACE)
                self.__send_heartbeat_to_handler()
                pod_phase = pod.status.phase
                # self.log_factory.get_logger(self.name).debug(f'Task pod {pod_name} phase is {pod_phase}')
                # see for short status:
                # https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/V1PodStatus.md
                # for more details:
                # https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/V1ContainerStatus.md
                # also can be useful:
                # https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/V1JobStatus.md
                if pod_phase == 'Failed':
                    self.log_factory.get_logger(self.name).error(f'Task pod {pod_name} phase is {pod_phase}')
                    raise TaskFailedException
                if pod_phase == 'Running' or pod_phase == 'Succeeded':
                    self.log_factory.get_logger(self.name).debug(f'Task pod {pod_name} phase is {pod_phase}')
                    return
                if pod_phase == 'Unknown':
                    self.log_factory.get_logger(self.name).debug(f'Task pod {pod_name} phase is {pod_phase}')
                    # todo: редкий случай, но может потребоваться обработка
                    pass
                if pod_phase == 'Pending':
                    self.log_factory.get_logger(self.name).debug(f'Task pod {pod_name} phase is {pod_phase}')
                    pass
            except OpenApiException as e:
                self.log_factory.get_logger(self.name).error(e, exc_info=True)
                raise TaskFailedException
            time.sleep(WAIT_POD_IS_RUNNING_SLEEP_TIME_SEC)
            now = datetime.utcnow()
            time_elapsed_sec = (now - start).seconds
        raise TaskFailedException('Wait for pod running timeout')

    def __get_pod_name(self, job):
        self.log_factory.get_logger(self.name).debug('Getting pod name...')
        start = datetime.utcnow()
        time_elapsed_sec = 0
        while time_elapsed_sec < WAIT_GET_POD_NAME_TIMEOUT_SEC:
            try:
                controllerUid = job.metadata.uid
                pod_label_selector = "controller-uid=" + controllerUid
                pods_list = self.core_api_client.list_namespaced_pod(namespace=config.TASK_K8S_NAMESPACE,
                                                                     label_selector=pod_label_selector)
                self.__send_heartbeat_to_handler()
                pods_available = len(pods_list.items)
                if pods_available > 1:
                    raise TaskFailedException(f'Expected 1 pod, given: {pods_available}')
                if pods_available == 1:
                    return pods_list.items[0].metadata.name
            except OpenApiException as e:
                self.log_factory.get_logger(self.name).error(e, exc_info=True)
                raise TaskFailedException
            time.sleep(WAIT_GET_POD_NAME_SLEEP_TIME_SEC)
            now = datetime.utcnow()
            time_elapsed_sec = (now - start).seconds
        raise TaskFailedException('Wait get pod name timeout')

    def __raise_if_pod_not_succeed(self, pod_phase):
        if pod_phase == 'Failed' or pod_phase == 'Unknown':
            raise TaskFailedException
        # todo: 'Unknown' редкий случай, но может потребоваться обработка

    def __raise_if_pod_logs_contain_error(self, pod_logs_contain_error):
        if pod_logs_contain_error:
            raise TaskFailedException

    def __wait_until_pod_finish(self, pod_name):
        self.log_factory.get_logger(self.name).debug('Waiting for pod finishing...')
        pod_phase = 'Running'
        pod_logs_reader = PodLogsReader(self.core_api_client, self.tasks_log_factory, self.log_factory,
                                        self.name, pod_name, pod_phase, WAIT_FOR_LOGS_AFTER_POD_FINISHED_SEC,
                                        POD_LOG_REQUEST_TIMEOUT_SEC)
        # опционально можно еще добавить проверку not pod_logs_contain_error
        while pod_phase == 'Running' or pod_logs_reader.need_read_logs():
            begin_time = time.time()
            try:
                pod = self.core_api_client.read_namespaced_pod_status(pod_name, namespace=config.TASK_K8S_NAMESPACE)
                self.__send_heartbeat_to_handler()
                pod_phase = pod.status.phase  # 'Running', 'Unknown', 'Succeeded', 'Failed', 'Pending'
                # не предпринимаем сейчас действий в зависимости от состояния
                # логи нужно прочитать в любом случае, поэтому, например, исключения выбрасываем в конце
                self.log_factory.get_logger(self.name).debug(f'Task pod {pod_name} phase is {pod_phase}')
                pod_logs_reader.set_current_pod_phase(pod_phase)
                pod_logs_reader.read_and_process_log_fragment()
            except OpenApiException as e:
                self.log_factory.get_logger(self.name).error(e, exc_info=True)
                raise TaskFailedException
            except Exception as e:
                self.log_factory.get_logger(self.name).error(e, exc_info=True)
                raise TaskFailedException
            end_time = time.time()
            delta = end_time - begin_time
            sleep_time = WAIT_POD_UNTIL_FINISH_SLEEP_TIME_SEC - delta \
                if delta < WAIT_POD_UNTIL_FINISH_SLEEP_TIME_SEC else 0
            time.sleep(sleep_time)
        # нужно дочитать логи до конца, поэтому ошибки выбрасываем уже после выхода из цикла
        self.__raise_if_pod_not_succeed(pod_phase)
        self.__raise_if_pod_logs_contain_error(pod_logs_reader.pod_logs_contain_error)

    def __setup_clients(self):
        self.log_factory.get_logger(self.name).debug('Setting up clients...')
        try:
            kubernetes.config.load_kube_config(config.TASK_K8S_KUBECONFIG)
            try:
                cfg = Configuration().get_default_copy()
            except AttributeError:
                cfg = Configuration()
            cfg.assert_hostname = False  # for dev purposes
            Configuration.set_default(cfg)

            self.batch_api_client = client.BatchV1Api()
            self.core_api_client = client.CoreV1Api()

            self.__send_heartbeat_to_handler()
        except OpenApiException as e:
            self.log_factory.get_logger(self.name).error(e, exc_info=True)
            raise TaskFailedException

    def run(self):
        job_name = f'mldev-wf-{self.workflow_id}-job-{self.name}'
        job_manifest = self.job_manifest_factory.create_job_manifest(job_name)

        try:
            self.__setup_clients()
            self.__create_job(job_manifest)
            job = self.__wait_job_is_created(job_name)
            pod_name = self.__get_pod_name(job)
            self.__wait_pod_is_running(pod_name)
            self.__wait_until_pod_finish(pod_name)
        except TaskFailedException as e:
            self.log_factory.get_logger(self.name).error('Task failed')
            self.log_factory.get_logger(self.name).error(e, exc_info=True)
            self.handler_snd_channel.send_nowait(ExecutorFinished(ExecutorResultType.ERROR))
            return

        self.__send_heartbeat_to_handler()
        self.log_factory.get_logger(self.name).info('Task succeeded')
        self.log_factory.get_logger(self.name).info(f'Task executor {self.name}: sending success msg')
        self.handler_snd_channel.send_nowait(ExecutorFinished(ExecutorResultType.SUCCESS))
