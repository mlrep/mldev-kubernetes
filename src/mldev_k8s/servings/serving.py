# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Experiment tags for serving.
==================

Module contains all necessary experiment tags for serving.

* :py:class:`Serving`
    Starts model as a service using components from container.
"""

from typing import List

from mldev.experiment_tag import experiment_tag

from mldev_k8s.servings.container import ComponentFactory


@experiment_tag()
class Serving:

    def __init__(self, name: str, models_metadata_files: List[str]):
        self.name = name
        self.__models_metadata_files = models_metadata_files

    def __call__(self, *args, **kwargs):
        component_factory = ComponentFactory
        fire = component_factory.get_or_create_fire()
        fire.fire_up(self.name, self.__models_metadata_files)
