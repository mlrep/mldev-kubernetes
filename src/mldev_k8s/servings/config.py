# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Configuration module
==================

Module containing all  configuration parameters
"""

import os


class RunEnvironment:
    KUBERNETES = 'kubernetes'
    LOCAL = 'local'


class RunMode:
    NORMAL = 'normal'
    TEST = 'test'


HYPERCORN_CONFIG_FILE_NAME = 'server_config.toml'

# SERVING_SERVICE_DOCKER_* config: variables to build service docker image
SERVICE_DOCKER_FILE = os.getenv('SERVING_SERVICE_DOCKER_FILE', 'mldev-k8s-serving.Dockerfile')
SERVICE_DOCKER_CONTEXT = os.getenv('SERVING_SERVICE_DOCKER_CONTEXT', '.')
SERVICE_DOCKER_IMAGE_NAME = os.getenv('SERVING_SERVICE_DOCKER_IMAGE_NAME', 'mldev-k8s-serving:experiment')
SERVICE_DOCKER_IMAGE_REGISTRY = os.getenv('SERVING_SERVICE_DOCKER_IMAGE_REGISTRY')
SERVICE_DOCKER_BASE_IMAGE = os.getenv('SERVING_SERVICE_DOCKER_BASE_IMAGE', 'mldev-k8s-base')

# SERVING_SERVINCE_K8S_* config: variables to manage pods, services (Kubernetes environment)
SERVICE_K8S_NAMESPACE = os.getenv('SERVING_SERVICE_K8S_NAMESPACE', 'mldev')
SERVICE_K8S_IMAGE_PULL_POLICY = os.getenv('SERVING_SERVICE_K8S_IMAGE_PULL_POLICY', 'Always')
SERVICE_K8S_IMAGE_PULL_SECRET = os.getenv('SERVING_SERVICE_K8S_IMAGE_PULL_SECRET')
SERVICE_K8S_KUBECONFIG = os.getenv('SERVING_SERVICE_K8S_KUBECONFIG', 'kubeconfig.yaml')
SERVICE_K8S_SERVICE_PORT = os.getenv('SERVING_SERVICE_K8S_PORT', 8881)

DEFAULT_RUN_ENV = os.getenv('SERVING_DEFAULT_RUN_ENV', RunEnvironment.KUBERNETES)
RUN_MODE = os.getenv('SERVING_RUN_MODE', RunMode.NORMAL)
TEST_CONTAINER = os.getenv('TEST_CONTAINER', None)
