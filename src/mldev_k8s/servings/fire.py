# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Entry point for whole serving functionality
==================

Module contains single class for all serving functionality encapsulation.

* :py:class:`Fire`
    Starts model as a service according to given configuration.
"""

import secrets

from mldev_k8s.servings.environments.abc import DockerImageFactory, ServiceLauncherFactory


class Fire:

    def __init__(self, docker_image_factory: DockerImageFactory, service_launcher_factory: ServiceLauncherFactory):
        self.__docker_image_factory = docker_image_factory
        self.__service_launcher_factory = service_launcher_factory
        self.__deployment_id = secrets.token_hex(3)

    def fire_up(self, name, models_metadata_files):
        self.__docker_image_factory.create()

        service_launcher = self.__service_launcher_factory.create(name, self.__deployment_id,
                                                                  models_metadata_files)

        service_launcher.launch()
