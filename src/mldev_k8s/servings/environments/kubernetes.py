# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Kubernetes environment module
==================

Abstract classes supporting different environments.

* :py:class:`KubernetesDockerImageFactory`
    Factory to create docker image for Kubernetes.
* :py:class:`KubernetesServiceLauncherFactory`
    Factory to create service launcher for Kubernetes.

"""

import docker
from docker.errors import BuildError, APIError

from mldev_k8s.servings import config
from mldev_k8s.servings.environments.abc import DockerImageFactory, ServiceLauncherFactory
from mldev_k8s.servings.launch.kubernetes import KubernetesServiceLauncher

from mldev import logger


class KubernetesDockerImageFactory(DockerImageFactory):
    """
    Builds Docker experiment image to run on Kubernetes.
    """

    """
    Builds docker image with tag given by env var `config.SERVICE_DOCKER_IMAGE_NAME`
    using base image specified in env var `config.SERVICE_DOCKER_BASE_IMAGE`.

    If env var `config.SERVICE_DOCKER_IMAGE_REGISTRY` is specified, it is added to tag.

    If env var `config.SERVICE_DOCKER_IMAGE_REGISTRY` is specified, built image will be pushed to registry.    

    :return: built image.
    """

    def create(self, **kwargs):
        # https://docker-py.readthedocs.io/en/stable/client.html
        client = docker.from_env()
        try:
            if config.SERVICE_DOCKER_IMAGE_REGISTRY:
                tag = config.SERVICE_DOCKER_IMAGE_REGISTRY + '/' + config.SERVICE_DOCKER_IMAGE_NAME
                base_image = config.SERVICE_DOCKER_IMAGE_REGISTRY + '/' + config.SERVICE_DOCKER_BASE_IMAGE
            else:
                tag = config.SERVICE_DOCKER_IMAGE_NAME
                base_image = config.SERVICE_DOCKER_BASE_IMAGE

            build_args = {'MLDEV_K8S_BASE_IMAGE': base_image}

            logger.info(f'Start building image with tag {tag}')
            image, build_logs_generator = client.images.build(path=config.SERVICE_DOCKER_CONTEXT,
                                                              dockerfile=config.SERVICE_DOCKER_FILE,
                                                              tag=tag,
                                                              # todo: сделать параметром config
                                                              # nocache=True,
                                                              buildargs=build_args)
            logger.info('Build succeed')

            if config.SERVICE_DOCKER_IMAGE_REGISTRY:
                logger.info(f'Start pushing image')
                client.images.push(tag)

        except BuildError as e:
            raise e
        except APIError as e:
            raise e

        return image


class KubernetesServiceLauncherFactory(ServiceLauncherFactory):

    def create(self, name, deployment_id, models_metadata_files):
        return KubernetesServiceLauncher(name, deployment_id, models_metadata_files)
