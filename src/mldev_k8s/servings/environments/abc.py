# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Environments abc module
==================

Abstract classes supporting different environments.

* :py:class:`DockerImageFactory`
    Factory to create docker image.
* :py:class:`TaskExecutorFactory`
    Factory to create service launcher.

"""

from abc import ABC, abstractmethod

from mldev_k8s.servings.launch.launcher import ServiceLauncher


class DockerImageFactory(ABC):

    @abstractmethod
    def create(self, **kwargs):
        pass


class ServiceLauncherFactory(ABC):

    @abstractmethod
    def create(self, name, deployment_id, models_metadata_files) -> ServiceLauncher:
        pass
