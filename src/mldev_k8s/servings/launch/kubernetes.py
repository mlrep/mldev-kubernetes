# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Kubernetes service module
==================

Module executes tasks on Kubernetes cluster.

* :py:class:`KubernetesServiceLauncher`
    Launches model as a service on Kubernetes cluster

"""

import time
from datetime import datetime

import kubernetes
from hypercorn import Config
from kubernetes import client
from kubernetes.client.configuration import Configuration
from kubernetes.client.exceptions import OpenApiException, ApiException

from mldev_k8s.servings import config
from mldev_k8s.servings.launch.launcher import ServiceLauncher

from mldev import logger


class ServingFailedException(Exception):
    pass


class ServiceManifestFactory:

    def __init__(self, name, deployment_id):
        self.name = name
        self.deployment_id = deployment_id

    def __get_service_manifest(self, service_name, pod_name):
        serving_label = f'mldev-srvng-{self.deployment_id}'

        service_manifest = {
            'apiVersion': 'v1',
            'kind': 'Service',
            'metadata': {
                'name': service_name,
                'labels': {
                    'mldev': 'mldev',
                    'mldev-srvng': serving_label
                }
            },
            'spec': {
                'selector': {
                    'app.kubernetes.io/name': pod_name
                },
                'ports': [
                    {
                        'name': 'model-svc-port',
                        'protocol': 'TCP',
                        'port': config.SERVICE_K8S_SERVICE_PORT,
                        'targetPort': 'model-pod-port'
                    }
                ]
            }
        }
        return service_manifest

    def create_service_manifest(self, service_name, pod_name):
        service_manifest = self.__get_service_manifest(service_name, pod_name)
        return service_manifest


class PodManifestFactory:

    def __init__(self, name, deployment_id, models_metadata_files):
        self.name = name
        self.models_metadata_files = models_metadata_files
        self.deployment_id = deployment_id

    # noinspection PyMethodMayBeStatic
    def __get_container_command_args(self):
        # оставлено для удобства тестирования
        # return ['-c', 'while true; do sleep 10 && echo 111; done']
        return ['-c', f'source ./venv/bin/activate && '
                      f'hypercorn mldev_k8s.servings.service.main:app '
                      f'--config {config.HYPERCORN_CONFIG_FILE_NAME}']

    # noinspection PyMethodMayBeStatic
    def __get_service_port(self):
        hypercorn_cfg = Config()
        hypercorn_cfg = hypercorn_cfg.from_toml(config.HYPERCORN_CONFIG_FILE_NAME)
        port = int(hypercorn_cfg.bind[0].split(':')[1])
        return port

    def __get_pod_manifest(self, pod_name):

        if config.SERVICE_DOCKER_IMAGE_REGISTRY:
            image = config.SERVICE_DOCKER_IMAGE_REGISTRY + '/' + config.SERVICE_DOCKER_IMAGE_NAME
            image_pull_policy = config.SERVICE_K8S_IMAGE_PULL_POLICY
        else:
            image = config.SERVICE_DOCKER_IMAGE_NAME
            image_pull_policy = 'Never'

        serving_label = f'mldev-srvng-{self.deployment_id}'

        pod_manifest = {
            'apiVersion': 'v1',
            'kind': 'Pod',
            'metadata': {
                'name': pod_name,
                'labels': {
                    'mldev': 'mldev',
                    'mldev-srvng': serving_label,
                    'app.kubernetes.io/name': pod_name
                }
            },
            'spec': {
                'containers': [
                    {
                        'name': 'mldev-service-container',
                        'image': image,
                        'imagePullPolicy': image_pull_policy,
                        'command': ['/bin/bash'],
                        'args': self.__get_container_command_args(),
                        'ports': [
                            {
                                'containerPort': self.__get_service_port(),
                                'name': 'model-pod-port'
                            }
                        ],
                        'env': [

                        ]
                    }
                ],
                # todo: продумать с рестартами
                'restartPolicy': 'Never'
            }
        }
        return pod_manifest

    def __add_image_pull_secret(self, pod_manifest):
        if not config.SERVICE_K8S_IMAGE_PULL_SECRET:
            return pod_manifest
        pod_manifest['spec']['imagePullSecrets'] = [
            {
                'name': config.SERVICE_K8S_IMAGE_PULL_SECRET
            }]
        return pod_manifest

    def __update_env_vars_with_models_metadata_files(self, pod_manifest):
        envs = pod_manifest['spec']['containers'][0]['env']
        models_metadata_env_val = ','.join(self.models_metadata_files)
        models_metadata_env = {'name': 'SERVING_MODELS_METADATA',
                               'value': models_metadata_env_val}
        envs += [models_metadata_env]

    def create_pod_manifest(self, pod_name):
        pod_manifest = self.__get_pod_manifest(pod_name)
        pod_manifest = self.__add_image_pull_secret(pod_manifest)
        self.__update_env_vars_with_models_metadata_files(pod_manifest)
        return pod_manifest


WAIT_POD_IS_CREATED_TIMEOUT_SEC = 30
WAIT_POD_IS_CREATED_SLEEP_TIME_SEC = 5
WAIT_POD_IS_STARTED_TIMEOUT_SEC = 600
WAIT_POD_IS_STARTED_SLEEP_TIME_SEC = 5

WAIT_SERVICE_IS_CREATED_TIMEOUT_SEC = 30
WAIT_SERVICE_IS_CREATED_SLEEP_TIME_SEC = 5


class KubernetesServiceLauncher(ServiceLauncher):

    def __init__(self, name, deployment_id, models_metadata_files):
        self.__deployment_id = deployment_id
        self.__models_metadata_files = models_metadata_files
        self.__name = name
        self.__pod_manifest_factory = PodManifestFactory(name, deployment_id, models_metadata_files)
        self.__service_manifest_factory = ServiceManifestFactory(name, deployment_id)

    def __setup_clients(self):
        logger.debug('Setting up clients...')
        try:
            kubernetes.config.load_kube_config(config.SERVICE_K8S_KUBECONFIG)
            try:
                cfg = Configuration().get_default_copy()
            except AttributeError:
                cfg = Configuration()
            cfg.assert_hostname = False  # for dev purposes
            Configuration.set_default(cfg)

            self.core_api_client = client.CoreV1Api()

        except OpenApiException as e:
            logger.error(e, exc_info=True)
            raise ServingFailedException

    def __create_pod(self, pod_manifest):
        logger.debug('Creating pod...')
        try:
            self.core_api_client.create_namespaced_pod(body=pod_manifest,
                                                       namespace=config.SERVICE_K8S_NAMESPACE)
        except OpenApiException as e:
            logger.error(e, exc_info=True)
            raise ServingFailedException

    def __wait_pod_is_created(self, pod_name):
        logger.debug('Waiting for pod creation...')
        start = datetime.utcnow()
        time_elapsed_sec = 0
        while time_elapsed_sec < WAIT_POD_IS_CREATED_TIMEOUT_SEC:
            try:
                pod = self.core_api_client.read_namespaced_pod(pod_name, namespace=config.SERVICE_K8S_NAMESPACE)
                return pod
            except ApiException as e:
                if e.status != 404:
                    logger.error(e, exc_info=True)
                    raise ServingFailedException
                logger('Waiting for pod creation, received code 404')
            except OpenApiException as e:
                logger.error(e, exc_info=True)
                raise ServingFailedException
            time.sleep(WAIT_POD_IS_CREATED_SLEEP_TIME_SEC)
            now = datetime.utcnow()
            time_elapsed_sec = (now - start).seconds
        raise ServingFailedException('Wait for pod creation timeout')

    def __wait_pod_is_started(self, pod_name):
        logger.debug('Waiting for pod started...')
        start = datetime.utcnow()
        time_elapsed_sec = 0
        while time_elapsed_sec < WAIT_POD_IS_STARTED_TIMEOUT_SEC:
            try:
                pod = self.core_api_client.read_namespaced_pod_status(pod_name,
                                                                      namespace=config.SERVICE_K8S_NAMESPACE)
                pod_phase = pod.status.phase
                # see for short status:
                # https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/V1PodStatus.md
                # for more details:
                # https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/V1ContainerStatus.md
                # also can be useful:
                # https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/V1JobStatus.md
                if pod_phase == 'Failed' or pod_phase == 'Succeeded':
                    logger.error(f'Service pod {pod_name} phase is {pod_phase}')
                    raise ServingFailedException
                if pod_phase == 'Running':
                    logger.debug(f'Service pod {pod_name} phase is {pod_phase}')
                    return
                if pod_phase == 'Unknown':
                    logger.debug(f'Service pod {pod_name} phase is {pod_phase}')
                    # todo: редкий случай, но может потребоваться обработка
                    pass
                if pod_phase == 'Pending':
                    logger.debug(f'Service pod {pod_name} phase is {pod_phase}')
                    pass
            except OpenApiException as e:
                logger.error(e, exc_info=True)
                raise ServingFailedException
            time.sleep(WAIT_POD_IS_CREATED_SLEEP_TIME_SEC)
            now = datetime.utcnow()
            time_elapsed_sec = (now - start).seconds
        raise ServingFailedException('Wait for pod started timeout')

    def __create_pod_name(self):
        # todo: следует проверять, что имя self.__name состоит из цифр, числа и дефиса
        if self.__name:
            pod_name = f'mldev-srvng-{self.__deployment_id}-pod-{self.__name}'
        else:
            pod_name = f'mldev-srvng-{self.__deployment_id}-pod'
        return pod_name

    def __create_service_name(self):
        # todo: следует проверять, что имя self.__name состоит из цифр, числа и дефиса
        if self.__name:
            service_name = f'mldev-srvng-{self.__deployment_id}-svc-{self.__name}'
        else:
            service_name = f'mldev-srvng-{self.__deployment_id}-svc'
        return service_name

    def __create_service(self, service_manifest):
        logger.debug('Creating service...')
        try:
            self.core_api_client.create_namespaced_service(body=service_manifest,
                                                           namespace=config.SERVICE_K8S_NAMESPACE)
        except OpenApiException as e:
            logger.error(e, exc_info=True)
            raise ServingFailedException

    def __wait_service_is_created(self, service_name):
        logger.debug('Waiting for service creation...')
        start = datetime.utcnow()
        time_elapsed_sec = 0
        while time_elapsed_sec < WAIT_SERVICE_IS_CREATED_TIMEOUT_SEC:
            try:
                svc = self.core_api_client.read_namespaced_service(service_name,
                                                                   namespace=config.SERVICE_K8S_NAMESPACE)
                return svc
            except ApiException as e:
                if e.status != 404:
                    logger.error(e, exc_info=True)
                    raise ServingFailedException
                logger('Waiting for service creation, received code 404')
            except OpenApiException as e:
                logger.error(e, exc_info=True)
                raise ServingFailedException
            time.sleep(WAIT_SERVICE_IS_CREATED_SLEEP_TIME_SEC)
            now = datetime.utcnow()
            time_elapsed_sec = (now - start).seconds
        raise ServingFailedException('Wait for service creation timeout')

    def __wait_service_is_ready(self, service_name):
        # опционально, можно реализовать проверки, используя статусы
        # https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/V1ServiceStatus.md
        # self.core_api_client.read_namespaced_service_status(service_name,
        # namespace=config.SERVICE_K8S_NAMESPACE)
        pass

    def launch(self):
        pod_name = self.__create_pod_name()
        pod_manifest = self.__pod_manifest_factory.create_pod_manifest(pod_name)

        service_name = self.__create_service_name()
        service_manifest = self.__service_manifest_factory.create_service_manifest(service_name, pod_name)

        try:
            self.__setup_clients()
            self.__create_pod(pod_manifest)
            self.__wait_pod_is_created(pod_name)
            self.__wait_pod_is_started(pod_name)
            # todo: полезно проверить, что работает спустя некоторое время
            #  (может сломаться быстро уже после статуса Running)

            self.__create_service(service_manifest)
            self.__wait_service_is_created(service_name)
            self.__wait_service_is_ready(service_name)
        except ServingFailedException as e:
            logger.error('Serving failed')
            logger.error(e, exc_info=True)
            return

        logger.info('Serving succeeded')
