# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Service launcher abc module
==================

Abstract classes supporting service launching.

* :py:class:`ServiceLauncher`
    Interface for service launcher.
"""

from abc import ABC, abstractmethod


class ServiceLauncher(ABC):

    @abstractmethod
    def launch(self):
        pass
