# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Service implementation
==================

Module contains functions for service app configuration.
Starlette web framework is used.

"""

from starlette.applications import Starlette
from starlette.config import Config
from starlette.datastructures import CommaSeparatedStrings
from starlette.responses import PlainTextResponse
from starlette.routing import Route

from mldev_k8s.servings.models.factory import create_and_fill_models_registry, ModelRegistry
from mldev_k8s.servings.service.utils import create_endpoint

from mldev import logger

config = Config(".env")

SERVING_MODELS_METADATA = config('SERVING_MODELS_METADATA', cast=CommaSeparatedStrings)

serving_models_registry = create_and_fill_models_registry(SERVING_MODELS_METADATA)


def create_handler(runner, model_method_name):

    if hasattr(runner, model_method_name):
        handler = getattr(runner, model_method_name)
    else:
        # todo: functools partial
        def runner_wrapper(input_data):
            return runner(model_method_name, input_data)

        handler = runner_wrapper

    async def endpoint(request):
        input_data = await request.json()
        return PlainTextResponse(str(handler(input_data)))

    return endpoint


def construct_routes(serving_models_registry: ModelRegistry):
    routes = []
    for registry_item in serving_models_registry:
        public_method_name = registry_item.public_method_name
        method_name = registry_item.method_name
        model_version = registry_item.model_version
        model_name = registry_item.model_name

        if public_method_name:
            uri_method_name = public_method_name
        else:
            uri_method_name = method_name

        if model_version:
            url = f'/mldev/models/{model_name}/versions/{model_version}/{uri_method_name}'
        else:
            url = f'/mldev/models/{model_name}/{uri_method_name}'

        handler = registry_item.handler
        input_validator = registry_item.signature.validate_and_transform_input_data
        output_validator = registry_item.signature.validate_and_transform_output_data

        logger.info(f'Registered endpoint with url {url}')

        new_route = Route(url, create_endpoint(handler, input_validator, output_validator), methods=['POST'])
        routes.append(new_route)

    return routes


routes = construct_routes(serving_models_registry)

app = Starlette(routes=routes)
