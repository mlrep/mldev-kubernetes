# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Utilities for service implementation
==================

Module contains functions supporting service app configuration.

"""

from json import JSONDecodeError

from mldev import logger
from starlette.responses import JSONResponse

from mldev_k8s.servings.models.signatures.abc import ServingValidationError


async def _read_request_json(request):
    try:
        raw_input_data = await request.json()
    except JSONDecodeError as e:
        logger.error(e, exc_info=True)
        return None, JSONResponse(content={'detail': 'Cannot decode request body as JSON'}, status_code=400)
    return raw_input_data, None


def _validate_and_transform_input(input_validator_ptr, raw_input_data):
    try:
        input_data = input_validator_ptr(raw_input_data)
        return input_data, None
    except ServingValidationError as e:
        logger.info(e, exc_info=True)
        content = {'detail': 'Input data validation and/or transformation failed',
                   'validation_details': 'Not provided'}
        if e.__cause__:
            content['validation_details'] = repr(e.__cause__)
        return None, JSONResponse(content=content, status_code=400)


def _process(handler_ptr, input_data):
    try:
        model_output = handler_ptr(input_data)
        return model_output, None
    except Exception as e:
        logger.error(e, exc_info=True)
        content = {'detail': 'Unhandled error during inference', 'error_details': repr(e)}
        return None, JSONResponse(content=content, status_code=500)


def _validate_and_transform_output(output_validator_ptr, model_output):
    try:
        output_data = output_validator_ptr(model_output)
        return output_data, None
    except ServingValidationError as e:
        logger.info(e, exc_info=True)
        content = {'detail': 'Output data validation and/or transformation failed',
                   'validation_details': 'Not provided'}
        if e.__cause__:
            content['validation_details'] = repr(e.__cause__)
        return None, JSONResponse(content=content, status_code=400)


def create_endpoint(handler_ptr, input_validator_ptr, output_validator_ptr):
    async def endpoint(request):
        raw_input_data, response = await _read_request_json(request)
        if response:
            return response

        # todo: async validation
        input_data, response = _validate_and_transform_input(input_validator_ptr, raw_input_data)
        if response:
            return response

        # todo: async processing
        model_output, response = _process(handler_ptr, input_data)
        if response:
            return response

        # todo: async validation
        output_data, response = _validate_and_transform_output(output_validator_ptr, model_output)
        if response:
            return response

        return JSONResponse(output_data)

    return endpoint
