# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Model metadata module.
==================

Module contains classes for model metadata reading and validation.

"""

from typing import Optional, List

from pydantic import ConfigDict
from pydantic.main import BaseModel


class ModelImplementationMetadata(BaseModel):
    model_entry: str  # модуль или класс (полное имя) модели
    model_loading_params: Optional[dict] = None  # параметры, передаются загрузчику
    model_runtime_params: Optional[dict] = None  # параметры, передаются раннеру
    requirements: dict
    model_src_paths: Optional[list] = None
    model_data_paths: Optional[list] = None

    model_config = ConfigDict(
        protected_namespaces=('model_protected_',)
    )


class ModelMethodSignatureMetadata(BaseModel):
    input_definition: str
    output_definition: Optional[str] = None
    type: str # тип спецификации сигнатуры
    type_params: Optional[dict] = None  # параметры, передаются соответствующему обработчику сигнатуры

    model_config = ConfigDict(
        protected_namespaces=('model_protected_',)
    )


class ModelMethodMetadata(BaseModel):
    name: str
    public_name: Optional[str] = None
    signature: Optional[ModelMethodSignatureMetadata] = None

    model_config = ConfigDict(
        protected_namespaces=('model_protected_',)
    )


class ModelMetadata(BaseModel):
    name: str
    version: Optional[str]
    runner: Optional[str]
    loader: Optional[str]
    model_implementation: ModelImplementationMetadata
    model_methods: Optional[List[ModelMethodMetadata]]

    model_config = ConfigDict(
        protected_namespaces=('model_protected_',)
    )
