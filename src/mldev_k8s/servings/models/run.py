# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Model runner protocol and default implementation
==================

Module contains protocol each model runner have to implement.

* :py:class:`Runner`
    Default runner. Simply calls model method.
"""

from typing_extensions import Protocol

from mldev_k8s.servings.models.model import Model


class Runner(Protocol):

    def __init__(self, model: Model):
        pass

    def __call__(self, method_name: str, input_data: object):
        pass


class DefaultModelRunner:

    def __init__(self, model: Model):
        self.__model = model

    def __call__(self, method_name: str, input_data: object):
        method = getattr(self.__model.model_object.model, method_name)
        return method(input_data)

