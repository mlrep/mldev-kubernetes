# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Functions supporting signature construction and validation.
==================

Module contains functions for creation signatures for models.
Also it validates general requirements to models and runners interfaces.

"""

from typing import List, Dict

from mldev_k8s.servings.models.model import ModelMethod
from mldev_k8s.servings.models.model_meta import ModelMethodMetadata, ModelMethodSignatureMetadata
from mldev_k8s.servings.models.signatures.abc import ModelMethodSignature
from mldev_k8s.servings.models.signatures.default import DefaultSignature
from mldev_k8s.servings.models.signatures.json_pydantic import create_json_pydantic_signature


def _create_model_signature(model_signature_metadata: ModelMethodSignatureMetadata) -> ModelMethodSignature:
    if not model_signature_metadata or not model_signature_metadata.type:
        model_signature = DefaultSignature()
    elif model_signature_metadata.type == 'pydantic_json':
        model_signature = create_json_pydantic_signature(model_signature_metadata)
    else:
        raise AttributeError(f"Not supported signature type: {model_signature_metadata.type}")
    return model_signature


def _create_default_model_method():
    model_method = ModelMethod()
    model_method.name = 'predict'
    model_method.public_name = 'predict'
    model_method.signature = DefaultSignature()
    return model_method


def _create_model_methods(model_methods_metadata: List[ModelMethodMetadata]):
    if not model_methods_metadata:
        default_model_method = _create_default_model_method()
        return {default_model_method.name: default_model_method}

    result = {}
    for model_method_metadata in model_methods_metadata:
        model_method = ModelMethod()
        model_method.name = model_method_metadata.name
        model_method.public_name = model_method_metadata.public_name
        model_method.signature = _create_model_signature(model_method_metadata.signature)
        result[model_method_metadata.name] = model_method
    return result


def _validate_model_signature(loaded_model: object, model_methods: Dict[str, ModelMethod]):
    # todo реализовать
    # проверять, что у всех методов модели согласно сигнатуре один входной аргумент
    # если исходно сигнатура не указана, то должен быть predict (уже добавлен в model_methods)
    # входы и выходы методов на этом уровне не проверяются (мало смысла,
    # нужно было бы разбирать typing hints, которые и не всегда указываются)
    pass


def create_and_validate_signature(loaded_model: object, model_methods_metadata: List[ModelMethodMetadata]):
    model_methods = _create_model_methods(model_methods_metadata)
    _validate_model_signature(loaded_model, model_methods)
    return model_methods


def validate_runner_signature(runner: object, model_methods: Dict[str, ModelMethod]):
    # todo реализовать
    # В раннере не должно быть метода, которого нет в сигнатуре,
    # но в раннере может не быть метода, который есть в сигнатуре (будет вызываться __call__
    # с соответствующем именем метода в виде параметра).
    #
    # Если исходно сигнатура не указана, то у раннера может быть только predict
    # (не противоречит правилам валидации сигнатуры модели, predict уже добавлен в model_methods).
    #
    # У всех методов раннера должен быть один аргумент-вход.
    # Соответствие реализации модели и сигнатуры проверяется в другом месте,
    # здесь проверяется соответствие сигнатуры и раннера.
    pass
