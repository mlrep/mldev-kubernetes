# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Default model signature implementation
==================

Module contains classes for default model signature implementation.
It doesnt make any validation or transformation on input and output data.

* :py:class:`DefaultSignature`
    Default signature implementation.
"""


from mldev_k8s.servings.models.signatures.abc import ModelMethodSignature


class DefaultSignature(ModelMethodSignature):

    def validate_and_transform_input_data(self, input_data: dict):
        return input_data

    def validate_and_transform_output_data(self, output_data: dict):
        return output_data
