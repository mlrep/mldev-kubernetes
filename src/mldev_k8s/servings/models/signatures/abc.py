# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Model signatures abc module
==================

Abstract classes and protocols supporting models signatures.

* :py:class:`ModelMethodSignature`
    Protocol for model method signature.
"""

from typing_extensions import Protocol


class ModelMethodSignature(Protocol):

    def validate_and_transform_input_data(self, input_data: dict):
        pass

    def validate_and_transform_output_data(self, output_data: dict):
        pass


class ServingValidationError(AttributeError):
    pass
