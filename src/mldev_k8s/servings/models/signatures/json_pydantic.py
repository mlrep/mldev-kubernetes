# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
JSON Pydantic model signature implementation
==================

Module contains classes for JSON Pydantic signature implementation.
It constructs Pydantic models for input and output data according to provided JSON-based specification.
Constructed Pydantic models are used to validate input and output.
Also input dict-like object can be converted to the instance of constructed input Pydantic model.
Further it will be passed to model as input.

* :py:class:`JsonPydanticSignature`
    SON Pydantic signature implementation.
"""

import json
from typing import Type, Optional, List

from pydantic import create_model, BaseModel, ValidationError

from mldev_k8s.servings.models.model_meta import ModelMethodSignatureMetadata
from mldev_k8s.servings.models.signatures.abc import ModelMethodSignature, ServingValidationError


class JsonPydanticSignature(ModelMethodSignature):

    def __init__(self, input_pydantic_model: Type[BaseModel],
                 output_pydantic_model: Optional[Type[BaseModel]],
                 parse_input_dict: bool):
        self.__input_pydantic_model = input_pydantic_model
        self.__output_pydantic_model = output_pydantic_model
        self.__parse_input_dict = parse_input_dict

    # noinspection PyUnresolvedReferences
    def validate_and_transform_input_data(self, input_data: dict):
        try:
            parsed_input_data = self.__input_pydantic_model.model_validate(input_data)
            if self.__parse_input_dict:
                return parsed_input_data
            return input_data
        except ValidationError as e:
            raise ServingValidationError from e

    # noinspection PyUnresolvedReferences
    def validate_and_transform_output_data(self, output_data: dict):
        try:
            if self.__output_pydantic_model:
                self.__output_pydantic_model.model_validate(output_data)
            return output_data
        except ValidationError as e:
            raise ServingValidationError from e


def create_json_pydantic_signature(model_signature_metadata: ModelMethodSignatureMetadata):
    parse_input_dict = False
    if model_signature_metadata.type_params:
        if 'parse_input_dict' in model_signature_metadata.type_params:
            if model_signature_metadata.type_params['parse_input_dict']:
                parse_input_dict = True

    input_dict_proto = json.loads(model_signature_metadata.input_definition)
    input_signature = create_json_pydantic_model(input_dict_proto)

    if model_signature_metadata.output_definition:
        output_dict_proto = json.loads(model_signature_metadata.output_definition)
        output_signature = create_json_pydantic_model(output_dict_proto)
    else:
        output_signature = None

    signature = JsonPydanticSignature(input_signature, output_signature, parse_input_dict)

    return signature


def create_json_pydantic_model(dict_proto: dict):

    if not isinstance(dict_proto, dict):
        raise AttributeError("dict type for dict_proto is expected")

    def _create_model(name, var):
        if (type(var) is not dict) and (type(var) is not list):
            return type(var), ...
        if type(var) is list:
            return List[_create_model(name + "_from_list", var[0])[0]], ...
        return create_model(name, **{k: _create_model(k, v) for k, v in var.items()}), None

    pydantic_model = _create_model("Signature", dict_proto)[0]

    return pydantic_model
