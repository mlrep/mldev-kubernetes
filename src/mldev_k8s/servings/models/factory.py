# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Model factory module
==================

Module contains classes and functions for supporting of all steps of models handlers creation
using metadata files. Handlers are used for inference and called by service.

All handlers are stored in model registry, which is available for service.

Steps performed to create handler:

#. Read metadata file for model.
#. Create model loader according to metadata.
#. Load model entry via created loader.
#. Create model representation, including model methods signature creation
and model methods validation for conformity with protocol.
#. Create runner and validate its methods for conformity too.
#. Register each model interface method as handler in registry.

* :py:class:`ModelRegistry`
    Model registry.
* :py:func:`create_and_fill_models_registry`
    Supports described process of model registry creation.
"""

import functools
import itertools
import json
from importlib import import_module
from typing import List, cast, Dict, Iterable

from mldev_k8s.servings.models.load import Loader
from mldev_k8s.servings.models.model import Model, ModelObject
from mldev_k8s.servings.models.model_meta import ModelMetadata
from mldev_k8s.servings.models.run import Runner
from mldev_k8s.servings.models.signatures.signature import create_and_validate_signature, validate_runner_signature, \
    ModelMethodSignature

DEFAULT_LOADER_NAME = 'mldev_k8s.servings.models.load'
DEFAULT_RUNNER_NAME = 'mldev_k8s.servings.models.run.DefaultRunner'


def _create_loader(loader_name: str) -> Loader:
    if not loader_name:
        loader_name = DEFAULT_LOADER_NAME

    loader = import_module(loader_name)

    # todo: проверять сигнатуру загрузчика, что удовлетворяет протоколу
    # if hasattr(loader_module, 'load') and callable(getattr(loader_module, 'load')):
    #    pass

    loader = cast(Loader, loader)

    return loader


def _create_runner(runner_name: str, model: Model) -> Runner:
    if not runner_name:
        runner_name = DEFAULT_RUNNER_NAME

    module_name, class_name = runner_name.rsplit('.', 1)

    runner_module = import_module(module_name)
    runner_class = getattr(runner_module, class_name)
    runner = runner_class(model)

    validate_runner_signature(runner, model.model_methods)

    runner = cast(Runner, runner)

    return runner


def _create_model(loaded_model: object, model_meta: ModelMetadata):

    model_object = ModelObject()
    model_object.model = loaded_model
    model_object.model_runtime_params = model_meta.model_implementation.model_runtime_params

    model = Model()
    model.model_object = model_object
    model.name = model_meta.name
    model.model_methods = create_and_validate_signature(loaded_model, model_meta.model_methods)
    model.version = model_meta.version

    return model


def _read_model_metadata(model_metadata_file: str):
    with open(model_metadata_file) as f:
        model_metadata_obj = json.load(f)
    return ModelMetadata.model_validate(model_metadata_obj)


def create_and_fill_models_registry(models_metadata_files: List[str]):
    models_registry = ModelRegistry()

    for model_metadata_file in models_metadata_files:
        model_metadata = _read_model_metadata(model_metadata_file)

        loader = _create_loader(model_metadata.loader)

        loaded_model_instance = loader.load(model_metadata.model_implementation)

        model = _create_model(loaded_model_instance, model_metadata)

        runner = _create_runner(model_metadata.runner, model)

        models_registry.register(model, runner)

    return models_registry


class ModelMethodHandlerRegistryItem:

    def __init__(self, model_name: str, method_name: str,
                 public_method_name: str, handler, model_version: str, signature):
        self.model_name = model_name
        self.model_version = model_version
        self.method_name = method_name
        self.public_method_name = public_method_name
        self.handler = handler
        self.signature: ModelMethodSignature = signature


class ModelRegistryItem:

    def __init__(self, key: str, model: Model, runner: Runner,
                 methods_handlers: Dict[str, ModelMethodHandlerRegistryItem]):
        self.key = key
        self.model = model
        self.runner = runner
        self.methods_handlers = methods_handlers


class ModelRegistry:

    # todo: продвинутая поддержка версий и latest

    def __init__(self):
        self.__registry: Dict[str, ModelRegistryItem] = {}

    # noinspection PyMethodMayBeStatic
    def __get_model_item_key(self, model_name, model_version):
        if model_version:
            model_key = model_name + ":" + model_version
        else:
            model_key = model_name
        return model_key

    # noinspection PyMethodMayBeStatic
    def __create_handler(self, runner: Runner, model_method_name: str):
        if hasattr(runner, model_method_name):
            handler = getattr(runner, model_method_name)
        else:
            handler = functools.partial(runner.__call__, model_method_name)
        return handler

    def register(self, model: Model, runner: Runner):
        model_key = self.__get_model_item_key(model.name, model.version)
        # todo: предупреждение, если перезапись
        methods_handlers = {}
        for model_method in model.model_methods.values():
            handler = self.__create_handler(runner, model_method.name)
            model_method_handler_item = ModelMethodHandlerRegistryItem(model.name, model_method.name,
                                                                       model_method.public_name,
                                                                       handler, model.version,
                                                                       model_method.signature)
            methods_handlers[model_method.name] = model_method_handler_item
        registry_item = ModelRegistryItem(model_key, model, runner, methods_handlers)
        self.__registry[model_key] = registry_item

    def get_handler(self, model_name: str, model_method_name, model_version: str = None):
        model_key = self.__get_model_item_key(model_name, model_version)
        return self.__registry[model_key].methods_handlers[model_method_name]

    def __iter__(self) -> Iterable[ModelMethodHandlerRegistryItem]:
        return itertools.chain.from_iterable(
            (method_handler for method_handler in model_registry_item.methods_handlers.values())
            for model_registry_item in self.__registry.values()
        )
