# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Internal representation of loaded model.
==================

Module contains classes for internal representation of loaded model.

"""

from typing import Optional, Dict, Any


class ModelMethod:
    name: str
    public_name: Optional[str]
    signature: Any


class ModelObject:
    model: object
    model_runtime_params: Optional[dict]


class Model:
    name: str
    version: str
    model_object: ModelObject
    model_methods: Dict[str, ModelMethod]
