# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Model loader protocol and default implementation
==================

Module contains protocol each model loader have to implement.

* :py:func:`load`
    Default loader. Threats model entry as Python module.
"""

from importlib import import_module
from types import ModuleType

from typing_extensions import Protocol

from mldev_k8s.servings.models.model_meta import ModelImplementationMetadata


class Loader(Protocol):

    def load(self, model_implementation_meta: ModelImplementationMetadata) -> ModuleType:
        pass


def load(model_implementation_meta: ModelImplementationMetadata):

    model_module = import_module(model_implementation_meta.model_entry)

    if hasattr(model_module, 'load') and callable(getattr(model_module, 'load')):
        model_module.load(model_implementation_meta.model_loading_params)

    return model_module
