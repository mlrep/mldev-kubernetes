# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Default container module
==================

Module contains single factory class for main components creation.

* :py:class:`DefaultComponentFactory`
    Supports creation of necessary components according to given configuration.
"""

from mldev_k8s.servings import config
from mldev_k8s.servings.environments.kubernetes import KubernetesDockerImageFactory, KubernetesServiceLauncherFactory
from mldev_k8s.servings.fire import Fire


class DefaultComponentFactory:

    _fire = None

    def _get_docker_image_factory(self):
        if config.DEFAULT_RUN_ENV == config.RunEnvironment.LOCAL:
            raise NotImplementedError
        if config.DEFAULT_RUN_ENV == config.RunEnvironment.KUBERNETES:
            return KubernetesDockerImageFactory()
        raise Exception(f'Unsupported run environment: {config.DEFAULT_RUN_ENV}')

    def _get_service_launcher_factory(self):
        if config.DEFAULT_RUN_ENV == config.RunEnvironment.LOCAL:
            raise NotImplementedError
        if config.DEFAULT_RUN_ENV == config.RunEnvironment.KUBERNETES:
            return KubernetesServiceLauncherFactory()
        raise Exception(f'Unsupported run environment: {config.DEFAULT_RUN_ENV}')

    def get_or_create_fire(self):

        if not DefaultComponentFactory._fire:

            docker_image_factory = self._get_docker_image_factory()
            service_launcher_factory = self._get_service_launcher_factory()
            fire = Fire(docker_image_factory, service_launcher_factory)
            DefaultComponentFactory._fire = fire

        return DefaultComponentFactory._fire


ComponentFactory = DefaultComponentFactory
