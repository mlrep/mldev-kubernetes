# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Container module
==================

Module chooses container to use.

"""

import importlib

from typing_extensions import Protocol

from mldev_k8s.servings import config
from mldev_k8s.servings.fire import Fire


class ComponentFactoryProtocol(Protocol):

    def get_or_create_fire(self) -> Fire:
        pass


if config.RUN_MODE == config.RunMode.NORMAL:
    container_name = 'mldev_k8s.servings.default_container'
elif config.RUN_MODE == config.RunMode.TEST:
    container_name = config.TEST_CONTAINER
else:
    raise AttributeError(f'Unknown run mode {config.RUN_MODE}')

container_module = importlib.import_module(container_name, package=None)

component_factory_class = getattr(container_module, 'ComponentFactory')

ComponentFactory: ComponentFactoryProtocol = component_factory_class()
