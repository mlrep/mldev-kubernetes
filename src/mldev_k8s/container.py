# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Container module
==================

Module chooses container to use.

"""
import importlib

import mldev_k8s.config as config

MAIN_CYCLE_SLEEP_TIME_SEC = 1

if config.RUN_MODE == config.RunMode.NORMAL or config.RUN_MODE == config.RunMode.DEBUG_GRAPH:
    container_name = 'mldev_k8s.mp.mp_container'
elif config.RUN_MODE == config.RunMode.TEST:
    container_name = config.TEST_CONTAINER
else:
    raise AttributeError(f'Unknown run mode {config.RUN_MODE}')

container_module = importlib.import_module(container_name, package=None)

CoordinatorRunner = getattr(container_module, 'CoordinatorRunner')
