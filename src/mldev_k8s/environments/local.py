# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

from mldev_k8s.environments.abc import DockerImageFactory, TaskExecutorFactory
from mldev_k8s.tasks.local import LocalTaskExecutor


# todo: not implemented yet

class LocalDockerImageFactory(DockerImageFactory):

    def create(self, **kwargs):
        pass


class LocalTaskExecutorFactory(TaskExecutorFactory):

    def create(self, name, handler_snd_channel, workflow_id):
        return LocalTaskExecutor(name, handler_snd_channel, workflow_id)
