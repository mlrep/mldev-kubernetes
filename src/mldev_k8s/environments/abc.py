# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Environments abc module
==================

Abstract classes supporting different environments.

* :py:class:`DockerImageFactory`
    Factory to create docker image.
* :py:class:`TaskExecutorFactory`
    Factory to create task executor.

"""

from abc import ABC, abstractmethod


class DockerImageFactory(ABC):

    @abstractmethod
    def create(self, **kwargs):
        pass


class TaskExecutorFactory(ABC):

    @abstractmethod
    def create(self, name, handler_snd_channel, workflow_id):
        pass
