# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Kubernetes environment module
==================

Abstract classes supporting different environments.

* :py:class:`KubernetesDockerImageFactory`
    Factory to create docker image for Kubernetes.
* :py:class:`KubernetesTaskExecutorFactory`
    Factory to create task executor for Kubernetes.

"""

import docker
from docker.errors import BuildError, APIError

import mldev_k8s.config as config
from mldev_k8s.environments.abc import DockerImageFactory, TaskExecutorFactory
from mldev_k8s.tasks.kubernetes import KubernetesTaskExecutor


class KubernetesDockerImageFactory(DockerImageFactory):
    """
    Builds Docker experiment image to run on Kubernetes.

    :param log_factory: factory to get logger.
    """
    def __init__(self, log_factory):
        self.log_factory = log_factory

    """
    Builds docker image with tag given by env var `config.TASK_DOCKER_IMAGE_NAME`
    using base image specified in env var `config.TASK_DOCKER_BASE_IMAGE`.
    
    If env var `config.TASK_DOCKER_IMAGE_REGISTRY` is specified, it is added to tag.
    
    If env var `config.TASK_DOCKER_IMAGE_REGISTRY` is specified, built image will be pushed to registry.    

    :return: built image.
    """
    def create(self, **kwargs):
        # https://docker-py.readthedocs.io/en/stable/client.html
        client = docker.from_env()
        try:
            if not config.TASK_DOCKER_IMAGE_REGISTRY:
                raise AttributeError('env var PARALLEL_TASK_DOCKER_IMAGE_REGISTRY must be specified')

            tag = config.TASK_DOCKER_IMAGE_REGISTRY + '/' + config.TASK_DOCKER_IMAGE_NAME
            base_image = config.TASK_DOCKER_IMAGE_REGISTRY + '/' + config.TASK_DOCKER_BASE_IMAGE

            build_args = {'MLDEV_K8S_BASE_IMAGE': base_image}

            self.log_factory.get_logger(self.__class__.__name__)\
                .info(f'Start building image with tag {tag}')
            image, build_logs_generator = client.images.build(path=config.TASK_DOCKER_CONTEXT,
                                                              dockerfile=config.TASK_DOCKER_FILE,
                                                              tag=tag,
                                                              # todo: сделать параметром config
                                                              # nocache=True,
                                                              buildargs=build_args)
            self.log_factory.get_logger(self.__class__.__name__).info('Build succeed')

            if config.TASK_DOCKER_IMAGE_REGISTRY:
                self.log_factory.get_logger(self.__class__.__name__).info(f'Start pushing image')
                client.images.push(tag)

        except BuildError as e:
            raise e
        except APIError as e:
            raise e

        return image


class KubernetesTaskExecutorFactory(TaskExecutorFactory):
    """
    Builds Docker experiment image to run on Kubernetes.

    :param tasks_log_factory: task logging factory (logs from Kubernetes).
    :param task_executor_log_factory: task executor logging factory.
    """
    def __init__(self, tasks_log_factory, task_executor_log_factory):
        self.tasks_log_factory = tasks_log_factory
        self.task_executor_log_factory = task_executor_log_factory

    def create(self, name, handler_snd_channel, workflow_id):
        """
        Creates Kubernetes task executor.

        :param name: task executor name (stage name obviously).
        :param handler_snd_channel: channel used by executor to send messages to handler.
        :param workflow_id: id of workflow.
        :return: `tasks.kubernetes.KubernetesTaskExecutor`.
        """
        return KubernetesTaskExecutor(name, handler_snd_channel, workflow_id,
                                      self.tasks_log_factory, self.task_executor_log_factory)
