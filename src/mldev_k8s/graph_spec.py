# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Graph Specification module
==================

Module contains classes for parsing specification of dependencies between stages
in form of explicit graph.

* :py:class:`GraphSpecStageGeneratorFactory`
    Recurrent algorithm for dependencies specification parsing
* :py:class:`ParallelStages`
    An experiment tag; should be used to mark sequence of stages as independent (allowing parallel execution)

"""

from typing import Iterable

from mldev.experiment import *

import mldev_k8s.config as config
from mldev_k8s.commons import IterableType
from mldev_k8s.container import CoordinatorRunner
from mldev_k8s.coordination import RegisterStageMessage
from mldev_k8s.loaders import parallel_stages_loader
from mldev_k8s.logs import CoordinatorLogFactory
from mldev_k8s.stage import ParallelStage, DummyParallelStage


class GraphSpecStageGeneratorFactory:
    """
    Recurrent algorithm for dependencies specification parsing. Implemented as an iterator.

    Dependencies specification is a tree-like YAML structure with child nodes for each node are represented
    as sets and sequences.
    Set means that children can be executed in parallel, sequence means that children must be
    executed in serial order.

    When used with :py:class:`pipeline.ParallelPipeline` YAML sets and sequences have been already
    parsed to iterable with given iterable_type.

    :param iterable: child nodes
    :param parent: parent node
    :param iterable_type: iterable type for child nodes, can be SERIAL or PARALLEL
    """

    logger = None

    def __init__(self, iterable, parent=None, iterable_type=IterableType.SERIAL):
        self.iterable = iterable
        self.parent = parent
        self.iterable_type = iterable_type
        self.dependencies = None
        self.__init_dependencies()
        self.coordinator_snd_channel = CoordinatorRunner().run_or_get()[0]

    def __get_logger(self):
        if not GraphSpecStageGeneratorFactory.logger:
            GraphSpecStageGeneratorFactory.logger = CoordinatorLogFactory().get_logger(self.__class__.__name__)
        return GraphSpecStageGeneratorFactory.logger

    def __init_dependencies(self):
        if self.iterable_type == IterableType.SERIAL:
            parent = self.parent
            while (parent is not None) and (parent.iterable_type == IterableType.PARALLEL):
                parent = parent.parent
            if parent is None:
                self.dependencies = []
            else:
                self.dependencies = parent.dependencies
        if self.iterable_type == IterableType.PARALLEL:
            self.dependencies = []

    def __update_dependencies_for_iterable(self, new_factory):
        if self.iterable_type == IterableType.SERIAL:
            self.dependencies = new_factory.dependencies
        if self.iterable_type == IterableType.PARALLEL:
            self.dependencies += new_factory.dependencies

    def __update_dependencies_for_stage(self, stage):
        if self.iterable_type == IterableType.SERIAL:
            self.dependencies = [stage]
        if self.iterable_type == IterableType.PARALLEL:
            self.dependencies.append(stage)

    def __determine_upstream_stages_for_stage(self):
        upstream_stages = None
        if self.iterable_type == IterableType.SERIAL:
            upstream_stages = self.dependencies
        if self.iterable_type == IterableType.PARALLEL:
            parent = self.parent
            while (parent is not None) and (parent.iterable_type == IterableType.PARALLEL):
                parent = parent.parent
            if parent is None:
                upstream_stages = []
            else:
                upstream_stages = parent.dependencies
        if upstream_stages is None:
            raise RuntimeError('Something is wrong, bug')
        return upstream_stages

    def __construct_parallel_stage_wrapper(self, stage):
        self.__get_logger().info(f'Found leaf node; constructing wrapper for stage: {stage.name}')
        upstream_stages = self.__determine_upstream_stages_for_stage()
        upstream_stages_names = [stage.name for stage in upstream_stages]
        register_stage_msg = RegisterStageMessage(stage.name, upstream_stages_names)
        self.coordinator_snd_channel.send_nowait(register_stage_msg)
        return ParallelStage(stage, upstream_stages_names)

    def __graph_debug(self, stage):
        self.__get_logger().info(f'Current leaf node (stage): {stage.name}')
        undefined = True
        if self.iterable_type == IterableType.SERIAL:
            dependencies = [stage.name for stage in self.dependencies]
            self.__get_logger().info(f'Current iterable node type is {self.iterable_type}; '
                                     f'current stage dependencies are: {dependencies}')
            undefined = False
        if self.iterable_type == IterableType.PARALLEL:
            parent = self.parent
            while (parent is not None) and (parent.iterable_type == IterableType.PARALLEL):
                parent = parent.parent
            if parent is None:
                self.__get_logger().info(f'Current iterable node type is {self.iterable_type}; '
                                         f'current stage dependencies are: {[]}')
                undefined = False
            else:
                dependencies = [stage.name for stage in parent.dependencies]
                self.__get_logger().info(f'Current iterable node type is {self.iterable_type}; '
                                         f'current stage dependencies are: {dependencies}')
                undefined = False
        if undefined:
            self.__get_logger().info('Something is wrong; undefined')
        return DummyParallelStage(stage)

    def __construct_stage_wrapper(self, stage):
        if (config.RUN_MODE == config.RunMode.NORMAL) or (config.RUN_MODE == config.RunMode.TEST):
            return self.__construct_parallel_stage_wrapper(stage)
        if config.RUN_MODE == config.RunMode.DEBUG_GRAPH:
            return self.__graph_debug(stage)
        raise RuntimeError(f'Not supported run mode {config.RUN_MODE}')

    def create(self):
        """
        Creates new `GraphSpecStageGeneratorFactory` for each non-leaf node and yields from it.
        Implemented as Python generator.

        Always tracks dependencies for given node to know upstream stages for every stage encountered.

        Yields `stage.ParallelStage` as proxy for leaf node treated as stage.

        :return: `stage.ParallelStage` as proxy for leaf node treated as stage
        """
        for current in self.iterable:
            if isinstance(current, list) or isinstance(current, tuple):
                factory = GraphSpecStageGeneratorFactory(current, self, IterableType.SERIAL)
                yield from factory.create()
                self.__update_dependencies_for_iterable(factory)
            if isinstance(current, dict):
                factory = GraphSpecStageGeneratorFactory(current, self, IterableType.PARALLEL)
                yield from factory.create()
                self.__update_dependencies_for_iterable(factory)
            if isinstance(current, ParallelStages):
                factory = GraphSpecStageGeneratorFactory(current.arg, self, IterableType.PARALLEL)
                yield from factory.create()
                self.__update_dependencies_for_iterable(factory)
            # assumes stage is just callable
            if hasattr(current, '__call__'):
                stage_wrapper = self.__construct_stage_wrapper(current)
                yield stage_wrapper
                self.__update_dependencies_for_stage(current)


@experiment_tag(loader=parallel_stages_loader)
class ParallelStages:
    """
    Alias for YAML set, but does not require child nodes to be hashable.

    This means child nodes can be stored sequences and `ParallelStages` itself.

    :param arg: child nodes as iterable
    :param iterable_type: iterable type for child nodes, can be SERIAL or PARALLEL
    """

    def __init__(self, arg: Iterable, iterable_type: IterableType):
        self.arg = arg
        self.iterable_type = iterable_type
