# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Coordination module
==================

Module to coordinate parallel execution of experiment stages.

* :py:class:`TaskRef`
    Data object containing all information related to task.
* :py:class:`Coordinator`
    Performs setup and start of coordinated tasks.
* :py:class:`WorkflowState`
    Handles current experiment execution state, mainly for reporting goals.
    Implemented as State pattern.

"""

import abc
import enum
import secrets
from abc import ABC
from datetime import datetime

from mldev_k8s.channels import ChannelsFactory, WouldBlock
from mldev_k8s.tasks.task import TaskHandlerStatusChangedMessage, WaitingState, CompletedState, FailedState, WillNotRunState, \
    TaskHandlerHeartBeatMessage


class TaskRef:
    """
    Structure containing all runtime information about running task.

    :param stage_name: name of stage corresponding to task.
    """

    def __init__(self, stage_name):
        self.stage_name = stage_name
        "Name of stage corresponding to task"
        self.is_finished = False
        "True, if task is finished"
        self.task_status_name = WaitingState.state_name()
        "Current state"
        self.upstream_stages_names = []
        "Names of upstream stages"
        self.downstream_stages_names = []
        "Names of downstream stages"
        self.stage_methods_called = set()
        "Stores all stages methods called by pipeline"
        self.last_heartbeat = None
        "Time of last heartbeat from task"

class StageMethodCall(enum.Enum):
    PREPARE = 'prepare'
    RUN = 'run'


LOG_STAGES_STATE_INTERVAL_SEC = 30
ANY_TASK_HANDLER_TIMEOUT_SEC = 60
COORDINATION_MSG_TIMEOUT_SEC = 60


class Coordinator:
    """
    Setups and starts coordinated tasks. Coordination scheme is choreography.
    It means `Coordinator` only performs initial setup of communication means between tasks.

    Since tasks are coordinated together they are called 'workflow'.
    `Coordinator` also checks if all tasks are finished and finishes workflow.

    :param docker_image_factory: factory of docker images to build experiment image.
    :param task_handler_runner: runner to run handler for each task.
    :param channels_factory: factory to create communication channels.
    :param coordinator_log_factory: logging factory.
    """

    def __init__(self, docker_image_factory, task_handler_runner, channels_factory: ChannelsFactory,
                 coordinator_log_factory):
        self.channels_factory = channels_factory
        self.docker_image_factory = docker_image_factory
        self.task_handler_runner = task_handler_runner
        self.log_factory = coordinator_log_factory

        self.start_checks_performed = 0
        "Counter is used to determine if actual execution of stages can be started"
        self.pipeline_traversed = False
        "Flag is used to determine if actual execution of stages can be started"
        self.state = ConfigurationState(self)
        "Current workflow state"
        self.workflow_id = secrets.token_hex(3)
        "Workflow id"
        now = datetime.utcnow()
        self.log_stages_state_last_time = now
        "Last time state of stages was logged"
        self.any_task_last_heartbeat = now
        "Last heartbeat received from any task"
        self.last_time_coordination_msg_received = now
        "Last time coordination message was received"

        self.added_stages_tasks_names = []
        "List of all added stages names"
        self.tasks_refs = {}
        "Dictionary containing `TaskRef` for all added stages"
        self.from_handlers_snd_channel, self.handlers_rcv_channel = \
            self.channels_factory.create_send_receive_pair()
        "Pair of receive messages from handlers (one directional communication)"
        self.upstream_downstream_channels_pairs = {}
        "Dictionary with channels; key is a tuple of upstream-downstream stages names; " \
        "value is a tuple of send and receive channels (one directional communication)"

    def get_logger(self):
        return self.log_factory.get_logger(self.__class__.__name__)

    def mark_pipeline_traversed(self):
        self.pipeline_traversed = True

    def __add_stage_to_upstream_stage(self, stage_name, upstream_stage_name):
        upstream_task_ref = self.tasks_refs[upstream_stage_name]
        upstream_task_downstream_stages_names = upstream_task_ref.downstream_stages_names
        if upstream_task_downstream_stages_names is None:
            upstream_task_downstream_stages_names = [stage_name]
        elif stage_name not in upstream_task_downstream_stages_names:
            upstream_task_downstream_stages_names.append(stage_name)
        upstream_task_ref.downstream_stages_names = upstream_task_downstream_stages_names

    def add_stage(self, stage_name, upstream_stages_names=None):
        if stage_name in self.tasks_refs:
            self.get_logger().info(f'Stage {stage_name} has been already registered, skipped.')
            return
        self.added_stages_tasks_names.append(stage_name)

        task_ref = TaskRef(stage_name)
        if upstream_stages_names:
            task_ref.upstream_stages_names = upstream_stages_names
            for upstream_stage_name in upstream_stages_names:
                self.__add_stage_to_upstream_stage(stage_name, upstream_stage_name)
                send_receive_channels_pair = self.channels_factory.create_send_receive_pair()
                self.upstream_downstream_channels_pairs[(upstream_stage_name, stage_name)] = send_receive_channels_pair
        self.tasks_refs[stage_name] = task_ref

    def add_stage_call(self, stage_name, stage_method: StageMethodCall):
        if stage_name not in self.tasks_refs:
            raise RuntimeError('Not registered stage')
        if stage_method not in self.tasks_refs[stage_name].stage_methods_called:
            self.tasks_refs[stage_name].stage_methods_called.add(stage_method)
        else:
            raise RuntimeError('Method call already added')

    def check_can_start(self):
        self.start_checks_performed += 1
        all_stages_run_is_called = all([StageMethodCall.RUN in task_ref.stage_methods_called
                                        for task_ref in self.tasks_refs.values()])
        if all_stages_run_is_called or self.start_checks_performed > 1:
            return True
        return False

    def check_wont_start(self):
        any_stages_run_is_called = any([StageMethodCall.RUN in task_ref.stage_methods_called
                                        for task_ref in self.tasks_refs.values()])
        any_stages_prepare_is_called = any([StageMethodCall.PREPARE in task_ref.stage_methods_called
                                            for task_ref in self.tasks_refs.values()])
        return self.pipeline_traversed and (not any_stages_run_is_called) and (not any_stages_prepare_is_called)

    def log_tasks_state(self):
        """
        Logs current state of all tasks.
        """
        now = datetime.utcnow()
        time_elapsed = now - self.log_stages_state_last_time
        if time_elapsed.seconds < LOG_STAGES_STATE_INTERVAL_SEC:
            return
        tasks_states_str = ""
        task_refs = sorted(list(self.tasks_refs.items()), key=lambda item: item[0])
        for stage_name, task_ref in task_refs:
            last_heartbeat_str = task_ref.last_heartbeat.strftime("%H:%M:%S") if task_ref.last_heartbeat else 'None'
            tasks_states_str += f'\nStage: {stage_name}; State: {task_ref.task_status_name}; ' \
                                f'Heartbeat: {last_heartbeat_str}; Depends on: {task_ref.upstream_stages_names}'
        self.get_logger().info(tasks_states_str)
        self.log_stages_state_last_time = now

    def __build_docker_image(self):
        return self.docker_image_factory.create()

    def start(self):
        """
        Starts actual stages execution.
        """
        self.__build_docker_image()

        for stage_name in self.added_stages_tasks_names:
            task_ref = self.tasks_refs[stage_name]

            # it's necessary to store links to channels (i.e. from_executor_to_handler_snd_channel)
            # during all time coordinator is running; if some links are lost, they corresponding
            # channels can be closed depending on implementations of channels, i.e. multiprocessing.Queue
            # (looks like is is a side effect of garbage collector)
            # see. https://docs.python.org/3/library/multiprocessing.html#multiprocessing.Queue.close
            #
            # handler_snd_channel - sending messages to handler, not used now
            handler_snd_channel, from_handler_rcv_channel = self.channels_factory.create_send_receive_pair()
            task_ref.from_handler_rcv_channel = from_handler_rcv_channel

            from_executor_to_handler_snd_channel, from_executor_to_handler_rcv_channel = \
                self.channels_factory.create_send_receive_pair()
            task_ref.from_executor_to_handler_snd_channel = from_executor_to_handler_snd_channel
            task_ref.from_executor_to_handler_rcv_channel = from_executor_to_handler_rcv_channel

            upstream_rcv_channels = []
            for upstream_stage_name in task_ref.upstream_stages_names:
                upstream_channel = self.upstream_downstream_channels_pairs[(upstream_stage_name, stage_name)][1]
                upstream_rcv_channels.append(upstream_channel)

            downstream_snd_channels = []
            for downstream_stage_name in task_ref.downstream_stages_names:
                downstream_channel = self.upstream_downstream_channels_pairs[(stage_name, downstream_stage_name)][0]
                downstream_snd_channels.append(downstream_channel)

            self.task_handler_runner.run_task_handler(stage_name, from_handler_rcv_channel,
                                                      self.from_handlers_snd_channel.clone(),
                                                      from_executor_to_handler_snd_channel,
                                                      from_executor_to_handler_rcv_channel,
                                                      task_ref.upstream_stages_names, task_ref.downstream_stages_names,
                                                      upstream_rcv_channels, downstream_snd_channels, self.workflow_id)
            now = datetime.utcnow()
            task_ref.last_heartbeat = now
            self.any_task_last_heartbeat = datetime.utcnow()

    def check_tasks_are_finished_locally(self):
        all_finished = all([task_ref.is_finished for task_ref in self.tasks_refs.values()])
        if self.pipeline_traversed and all_finished:
            self.state = FinishedState(self)

    def check_any_tasks_are_alive(self):
        now = datetime.utcnow()
        time_elapsed = now - self.any_task_last_heartbeat
        if time_elapsed.seconds <= ANY_TASK_HANDLER_TIMEOUT_SEC:
            return
        self.get_logger().error(f'Task handlers timeout, finishing. '
                                f'Will wait for running task executors if they exist. '
                                f'To force kill use Ctrl+C.')
        self.state = FinishedState(self)

    def check_configuration_timeout(self):
        # assumes Configuration state
        now = datetime.utcnow()
        time_elapsed = now - self.last_time_coordination_msg_received
        if time_elapsed.seconds <= COORDINATION_MSG_TIMEOUT_SEC:
            return
        self.get_logger().error(f'Workflow start conditions are not satisfied, '
                                f'but no coordination messages received for {COORDINATION_MSG_TIMEOUT_SEC} sec; '
                                f'Check pipeline logic and assumptions.')
        self.state = FinishedState(self)

    def receive_from_channel_and_process(self, channel, handler):
        try:
            msg = channel.receive_nowait()
        except WouldBlock:
            return False
        handler(msg)
        return True


class CoordinationMessage(ABC):
    """
    Base class for messages sent from main process to coordinator.
    """
    pass


class CallStageMessage(CoordinationMessage):
    """
    Tells coordinator about stage method invocation by pipeline.

    :param stage_name: name of called stage.
    :param method: name of method invoked.
    """

    def __init__(self, stage_name, method):
        self.stage_name = stage_name
        self.method = method


class RegisterStageMessage(CoordinationMessage):
    """
    Tells coordinator to register new stage found in pipeline.

    :param stage_name: name of called stage.
    :param upstream_stages_names: names of stages oh which current stage depends.
    """

    def __init__(self, stage_name, upstream_stages_names=None):
        self.stage_name = stage_name
        self.upstream_stages_names = upstream_stages_names


class PipelineTraverseFinished(CoordinationMessage):
    """
    Tells to coordinator that all stages in pipeline have been iterated (pipeline is traversed).
    There can be more that one cycles over pipeline stages.
    """
    pass


class WorkflowState(ABC):
    """
    Handles current experiment execution state manly for reporting goals.
    Is also used to start and finish execution.

    Implemented as State pattern.

    :param coordinator: related workflow coordinator.
    """

    def __init__(self, coordinator: Coordinator):
        self._coordinator = coordinator

    @abc.abstractmethod
    def process_coordination_message(self, coordination_message):
        """
        Process message received from main process.

        :param coordination_message: message from main process.
        """
        pass

    @abc.abstractmethod
    def process_task_handler_message(self, task_message):
        """
        Process message received from one of task handlers.

        :param task_message: message from one of task handlers.
        """
        pass

    @abc.abstractmethod
    def process_state(self):
        """
        Performs repeating logic, independent of messages.
        """
        pass


class ConfigurationState(WorkflowState):
    """
    In Configuration state coordinator allows to add new stages, it creates structures to coordinate
    corresponding tasks.

    Coordinator opens channels to set up communication between coordinator,
    task handlers, task executors.

    Finally in this state coordinator decides whether to start or not actual pipeline execution.
    If conditions are satisfied it changes state to `RunningState` or `FinishedState`.

    :param coordinator: related workflow coordinator.
    """

    def __init__(self, coordinator: Coordinator):
        super().__init__(coordinator)

    def process_coordination_message(self, coordination_message):
        self._coordinator.last_time_coordination_msg_received = datetime.utcnow()
        if type(coordination_message) == RegisterStageMessage:
            self._coordinator.get_logger().info(
                f'Configuration State: Received register stage message; stage: {coordination_message.stage_name}.')
            self._coordinator.add_stage(coordination_message.stage_name, coordination_message.upstream_stages_names)
            return
        if type(coordination_message) == CallStageMessage:
            self._coordinator.get_logger().info(
                f'Configuration State: Received call stage message; stage: {coordination_message.stage_name}'
                f'; method: {coordination_message.method}.')
            self._coordinator.add_stage_call(coordination_message.stage_name, coordination_message.method)
            return
        if type(coordination_message) == PipelineTraverseFinished:
            self._coordinator.get_logger().info(f'Configuration State: Received pipeline traverse message.')
            self._coordinator.mark_pipeline_traversed()
            wont_start = self._coordinator.check_wont_start()
            if wont_start:
                self._coordinator.get_logger().info(f'Configuration State: Actual execution will not start.')
                self._coordinator.state = FinishedState(self._coordinator)
                return
            can_start = self._coordinator.check_can_start()
            if can_start:
                self._coordinator.get_logger().info(f'Configuration State: Actual execution can start.')
                self._coordinator.start()
                self._coordinator.state = RunningState(self._coordinator)
            return

        raise RuntimeError('Unsupported message')

    def process_task_handler_message(self, task_message):
        raise RuntimeError('Doesnt support task handler messages in Configuration State')

    def process_state(self):
        self._coordinator.check_tasks_are_finished_locally()
        self._coordinator.check_configuration_timeout()


class RunningState(WorkflowState):
    """
    In Running state coordinator receives from task handler messages about their tasks states.

    Coordinator reports current states of all tasks and decides when to go to `FinishedState`.

    :param coordinator: related workflow coordinator.
    """

    def __init__(self, coordinator: Coordinator):
        super().__init__(coordinator)

    def process_task_handler_message(self, task_message):
        if type(task_message) == TaskHandlerStatusChangedMessage:
            stage_name = task_message.stage_name
            self._coordinator.get_logger().info(f'Running State: '
                                                f'Received status changed message from {stage_name};'
                                                f' New status: {task_message.status}.')
            if stage_name not in self._coordinator.tasks_refs:
                raise RuntimeError(f'Stage {stage_name} not registered')
            self._coordinator.tasks_refs[stage_name].task_status_name = task_message.status
            now = datetime.utcnow()
            self._coordinator.tasks_refs[stage_name].last_heartbeat = now
            self._coordinator.any_task_last_heartbeat = now
            if task_message.status in {CompletedState.state_name(), FailedState.state_name(),
                                       WillNotRunState.state_name()}:
                self._coordinator.tasks_refs[stage_name].is_finished = True
            return
        if type(task_message) == TaskHandlerHeartBeatMessage:
            stage_name = task_message.stage_name
            self._coordinator.get_logger().debug(f'Running State: '
                                                 f'Received heartbeat message from stage {stage_name}.')
            if stage_name not in self._coordinator.tasks_refs:
                raise RuntimeError(f'Stage {stage_name} not registered.')
            now = datetime.utcnow()
            self._coordinator.any_task_last_heartbeat = now
            self._coordinator.tasks_refs[stage_name].last_heartbeat = now
            return

        raise RuntimeError('Unsupported message')

    def process_coordination_message(self, coordination_message):
        self._coordinator.get_logger().warning(f'Coordination message received in Running state, '
                                               f'ignored.')

    def process_state(self):
        self._coordinator.check_tasks_are_finished_locally()
        self._coordinator.check_any_tasks_are_alive()
        self._coordinator.log_tasks_state()


class FinishedState(WorkflowState):
    """
    In Finished state coordinator does nothing. Finished state just indicates pipeline is finished.

    Finished pipeline can include not successful tasks (stages), it also may not start at all.

    :param coordinator: related workflow coordinator.
    """

    def __init__(self, coordinator: Coordinator):
        super().__init__(coordinator)

    def process_coordination_message(self, coordination_message):
        self._coordinator.get_logger().warning(f'Coordination message received in Finished state, '
                                               f'ignored.')

    def process_task_handler_message(self, task_message):
        raise RuntimeError('Unsupported message in this state')

    def process_state(self):
        self._coordinator.log_tasks_state()
