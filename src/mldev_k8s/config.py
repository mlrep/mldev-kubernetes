# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Configuration module
==================

Module containing all  configuration parameters
"""

import os


class DependencySpecificationType:
    GRAPH = 'graph'
    BY_STAGE = 'by_stage'


class RunEnvironment:
    KUBERNETES = 'kubernetes'
    LOCAL = 'local'


class RunMode:
    NORMAL = 'normal'
    DEBUG_GRAPH = 'debug_graph'
    TEST = 'test'


class StorageType:
    VOLUME = 'volume'
    S3 = 's3'
    LOCAL = 'local'


class S3Credentials:
    FILE = 'file'
    SECRETS = 'secrets'


class LoggingMode:
    GATHER = 'gather'
    DETAILED = 'detailed'


# PARALLEL_TASK_DOCKER_* config: variables to build tasks docker image
TASK_DOCKER_FILE = os.getenv('PARALLEL_TASK_DOCKER_FILE', 'mldev-k8s.Dockerfile')
TASK_DOCKER_CONTEXT = os.getenv('PARALLEL_TASK_DOCKER_CONTEXT', '.')
TASK_DOCKER_IMAGE_NAME = os.getenv('PARALLEL_TASK_DOCKER_IMAGE_NAME', 'mldev-k8s:experiment')
TASK_DOCKER_IMAGE_REGISTRY = os.getenv('PARALLEL_TASK_DOCKER_IMAGE_REGISTRY')
TASK_DOCKER_BASE_IMAGE = os.getenv('PARALLEL_TASK_DOCKER_BASE_IMAGE', 'mldev-k8s-base')

# PARALLEL_TASK_K8S_* config: variables to manage tasks pods/jobs (Kubernetes environment)
TASK_K8S_NAMESPACE = os.getenv('PARALLEL_TASK_K8S_NAMESPACE', 'mldev')
TASK_K8S_IMAGE_PULL_POLICY = os.getenv('PARALLEL_TASK_K8S_IMAGE_PULL_POLICY', 'Always')
TASK_K8S_IMAGE_PULL_SECRET = os.getenv('PARALLEL_TASK_K8S_IMAGE_PULL_SECRET')
TASK_K8S_KUBECONFIG = os.getenv('PARALLEL_TASK_K8S_KUBECONFIG', 'kubeconfig.yaml')
TASK_K8S_PYTHONPATH = os.getenv('PYTHONPATH', './.mldev:./src')
TASK_K8S_STORAGE_TYPE = os.getenv('PARALLEL_TASK_K8S_STORAGE_TYPE', StorageType.VOLUME)

# PARALLEL_TASK_STORAGE_* config: variables to manage tasks storage for all environments
TASK_STORAGE_VOLUME_PVC_NAME = os.getenv('PARALLEL_TASK_STORAGE_VOLUME_PVC_NAME', 'task-pvc')
TASK_STORAGE_S3_CREDENTIALS = os.getenv('PARALLEL_TASK_STORAGE_S3_CREDENTIALS', S3Credentials.FILE)
TASK_STORAGE_S3_CREDENTIALS_FILE = os.getenv('PARALLEL_TASK_STORAGE_S3_CREDENTIALS_FILE', 's3-credentials')
TASK_STORAGE_S3_BUCKET_NAME = os.getenv('PARALLEL_TASK_STORAGE_S3_BUCKET_NAME', 'mldev-experiment')
# todo: убрать значение по умолчанию
TASK_STORAGE_S3_HOST = os.getenv('PARALLEL_TASK_STORAGE_S3_HOST', 'http://host.minikube.internal:9000')

DEFAULT_RUN_ENV = os.getenv('PARALLEL_DEFAULT_RUN_ENV', RunEnvironment.KUBERNETES)
RUN_MODE = os.getenv('PARALLEL_RUN_MODE', RunMode.NORMAL)
TEST_CONTAINER = os.getenv('TEST_CONTAINER', None)
DEPENDENCY_SPEC_TYPE = os.getenv('PARALLEL_DEPENDENCY_SPEC_TYPE', DependencySpecificationType.GRAPH)

GATHER_LOGS = os.getenv('PARALLEL_GATHER_LOGS', 'True')
PER_TASK_LOGS = os.getenv('PARALLEL_PER_TASK_LOGS', 'False')

# experiment file containing stage to run inside task; passed to tasks job/pod via container env var
EXPERIMENT_FILE = os.getenv('PARALLEL_EXPERIMENT_FILE', './experiment-parallel.yml')
# experiment stage (from experiment file) to run; passed to task job/pod via container env var
STAGE_NAME = os.getenv('PARALLEL_STAGE_NAME', None)
