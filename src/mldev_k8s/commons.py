# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Commons module
==================

Module containing common constants

* :py:class:`IterableType`
    Defines type of node in tree-like structure defining stages dependencies.
    See also `graph_spec.GraphSpecStageGeneratorFactory`.

"""


class IterableType:
    SERIAL = 'serial'
    PARALLEL = 'parallel'
