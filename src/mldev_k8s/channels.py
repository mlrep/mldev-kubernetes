# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Channels abc module
==================

Abstract classes supporting different communications technologies for coordination.

* :py:class:`SendChannel`
    Channel used to send messages.
* :py:class:`ReceiveChannel`
    Channel used to receive messages.
* :py:class:`ChannelsFactory`
    Creates pair of send and receive channels for two parties one direction communication.
* :py:class:`WouldBlock`
    Generalization of different exceptions occurred if channel is full or empty.

"""

import abc


class SendChannel(abc.ABC):
    """
    Channel used to send message from one party to another.
    """

    @abc.abstractmethod
    def send_nowait(self, message):
        """
        Puts message in channel and returns immediately.
        """
        pass

    @abc.abstractmethod
    def clone(self):
        """
        Clones itself, useful in case of many senders with one consumer.
        """
        pass


class ReceiveChannel(abc.ABC):
    """
    Channel used to receive message by one party from another.
    """

    @abc.abstractmethod
    def receive_nowait(self):
        """
        Gets message from channel if it exists, and returns immediately.
        """
        pass


class ChannelsFactory(abc.ABC):
    """
    Creates pair of send and receive channels for two parties one direction communication.
    For bi-directional communication create two pairs of channels.
    """

    @abc.abstractmethod
    def create_send_receive_pair(self):
        """
        Creates pare of channels in tuple (send_channel, receive_channel).
        """
        pass


class WouldBlock(Exception):
    """
    Generalization of different exceptions occurred if channel is full or empty.
    """
    pass
