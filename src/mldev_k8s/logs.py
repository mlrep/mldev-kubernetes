# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Logging module
==================

Module contains all necessary logging factories

"""


import abc
import logging
import os

import yaml


class LogConfig:

    def __init__(self, log_level='INFO', logs_dir='logs', k8s_log_tasks_separately=True,
                 k8s_tasks_log_file='k8s_tasks.log', k8s_coordination_logs=False, k8s_tasks_logs=False):
        self.log_level = log_level
        self.logs_dir = logs_dir
        self.k8s_log_tasks_separately = k8s_log_tasks_separately
        self.k8s_tasks_log_file = k8s_tasks_log_file
        self.k8s_coordination_logs = k8s_coordination_logs
        self.k8s_tasks_logs = k8s_tasks_logs


class LogFactory(abc.ABC):

    def __init__(self):
        self.__log_cfg = None

    def __read_log_config(self):
        lc = LogConfig()
        try:
            with open('.mldev/config.yaml', "r") as stream:
                config = yaml.safe_load(stream)
        except IOError:
            return lc
        if 'logger' not in config:
            return lc
        if 'level' in config['logger']:
            lc.log_level = config['logger']['level']
        if 'logs_dir' in config['logger']:
            lc.logs_dir = config['logger']['logs_dir']
        if 'k8s_log_tasks_separately' in config['logger']:
            lc.k8s_log_tasks_separately = bool(config['logger']['k8s_log_tasks_separately'])
        if 'k8s_coordination_logs' in config['logger']:
            lc.k8s_coordination_logs = bool(config['logger']['k8s_coordination_logs'])
        if 'k8s_tasks_log_file' in config['logger']:
            lc.k8s_tasks_log_file = config['logger']['k8s_tasks_log_file']
        if 'k8s_tasks_logs' in config['logger']:
            lc.k8s_tasks_logs = bool(config['logger']['k8s_tasks_logs'])
        return lc

    def _get_log_config(self):
        if self.__log_cfg is None:
            self.__log_cfg = self.__read_log_config()
        return self.__log_cfg

    def __get_formatter(self):
        return logging.Formatter("%(asctime)s:%(levelname)s:%(name)s:%(message)s",
                                 "%Y-%m-%d %H:%M:%S")

    def _construct_simple_file_logger(self, logger_name, logs_file_path):
        logger = logging.getLogger(logger_name)
        log_cfg = self._get_log_config()
        log_level = log_cfg.log_level
        logger.setLevel(log_level)

        simple_file_handler = logging.FileHandler(logs_file_path)
        simple_file_handler.setFormatter(self.__get_formatter())
        simple_file_handler.setLevel(log_level)
        logger.addHandler(simple_file_handler)
        return logger

    def _construct_simple_stream_logger(self, logger_name):
        logger = logging.getLogger(logger_name)
        log_cfg = self._get_log_config()
        log_level = log_cfg.log_level
        logger.setLevel(log_level)

        simple_file_handler = logging.StreamHandler()
        simple_file_handler.setFormatter(self.__get_formatter())
        simple_file_handler.setLevel(log_level)
        logger.addHandler(simple_file_handler)
        return logger

    def _extend_simple_stream_logger_with_file_handler(self, logger, logs_file_path):
        simple_file_handler = logging.FileHandler(logs_file_path)
        simple_file_handler.setFormatter(self.__get_formatter())
        simple_file_handler.setLevel(logger.level)
        logger.addHandler(simple_file_handler)
        return logger

    def _construct_no_format_file_logger(self, logger_name, logs_file_path):
        logger = logging.getLogger(logger_name)
        log_cfg = self._get_log_config()
        log_level = log_cfg.log_level
        logger.setLevel(log_level)

        simple_file_handler = logging.FileHandler(logs_file_path)
        simple_file_handler.setLevel(log_level)
        logger.addHandler(simple_file_handler)
        return logger

    def _construct_dummy_logger(self, logger_name):
        logger = logging.getLogger(logger_name)
        logger.addHandler(logging.NullHandler())
        return logger

    @abc.abstractmethod
    def get_logger(self, name):
        pass


TASK_EXECUTOR_LOGGER_NAME_TEMPLATE = 'mldev_task_executor_{}_{}'
TASK_EXECUTOR_LOG_FILE_NAME_TEMPLATE = 'k8s_task_executor_{}_{}.log'


class TaskExecutorLogFactory(LogFactory):

    def __init__(self):
        super().__init__()
        self.__loggers = {}

    def get_logger(self, name):
        loggers_name_prefix = str(os.getpid())
        full_name = TASK_EXECUTOR_LOGGER_NAME_TEMPLATE.format(loggers_name_prefix, name)
        if full_name in self.__loggers:
            return self.__loggers[full_name]

        lc = self._get_log_config()
        if lc.k8s_coordination_logs:
            logs_file_name = TASK_EXECUTOR_LOG_FILE_NAME_TEMPLATE.format(loggers_name_prefix, name)
            logs_file_path = os.path.join(lc.logs_dir, logs_file_name)
            logger = self._construct_simple_file_logger(full_name, logs_file_path)

        else:
            logger = self._construct_dummy_logger(full_name)
        self.__loggers[full_name] = logger
        return logger


TASK_HANDLER_LOGGER_NAME_TEMPLATE = 'mldev_task_handler_{}_{}'
TASK_HANDLER_LOG_FILE_NAME_TEMPLATE = 'k8s_task_handler_{}.log'


class TaskHandlerLogFactory(LogFactory):

    def __init__(self):
        super().__init__()
        self.__loggers = {}

    def get_logger(self, name):
        loggers_name_prefix = str(os.getpid())
        full_name = TASK_HANDLER_LOGGER_NAME_TEMPLATE.format(loggers_name_prefix, name)
        if full_name in self.__loggers:
            return self.__loggers[full_name]

        lc = self._get_log_config()
        if lc.k8s_coordination_logs:
            logs_file_name = TASK_HANDLER_LOG_FILE_NAME_TEMPLATE.format(loggers_name_prefix, name)
            logs_file_path = os.path.join(lc.logs_dir, logs_file_name)
            logger = self._construct_simple_file_logger(full_name, logs_file_path)

        else:
            logger = self._construct_dummy_logger(full_name)
        self.__loggers[full_name] = logger
        return logger


TASK_LOGGER_NAME_TEMPLATE = 'mldev_task_{}_{}'
TASK_LOG_FILE_NAME_TEMPLATE = 'k8s_task_{}_{}.log'


class TaskLogFactory(LogFactory):

    def __init__(self):
        super().__init__()
        self.__loggers = {}

    def get_logger(self, name):
        loggers_name_prefix = str(os.getpid())
        full_name = TASK_LOGGER_NAME_TEMPLATE.format(loggers_name_prefix, name)
        if full_name in self.__loggers:
            return self.__loggers[full_name]

        lc = self._get_log_config()
        if lc.k8s_tasks_logs:
            if lc.k8s_log_tasks_separately:
                logs_file_name = TASK_LOG_FILE_NAME_TEMPLATE.format(loggers_name_prefix, name)
                logs_file_path = os.path.join(lc.logs_dir, logs_file_name)
                logger = self._construct_no_format_file_logger(full_name, logs_file_path)

            else:
                logs_file_name = lc.k8s_tasks_log_file
                logs_file_path = os.path.join(lc.logs_dir, logs_file_name)
                logger = self._construct_no_format_file_logger(full_name, logs_file_path)
        else:
            logger = self._construct_dummy_logger(full_name)
        self.__loggers[full_name] = logger
        return logger


COORDINATOR_LOGGER_NAME_TEMPLATE = 'mldev_coord_{}'
COORDINATOR_LOG_FILE_NAME_TEMPLATE = 'k8s_coord_{}.log'


class CoordinatorLogFactory(LogFactory):

    def __init__(self):
        super().__init__()
        self.__loggers = {}

    def get_logger(self, name):
        if name in self.__loggers:
            return self.__loggers[name]

        lc = self._get_log_config()
        logger = self._construct_simple_stream_logger(name)
        if lc.k8s_coordination_logs:
            logs_file_name = COORDINATOR_LOG_FILE_NAME_TEMPLATE.format(os.getpid())
            logs_file_path = os.path.join(lc.logs_dir, logs_file_name)
            logger = self._extend_simple_stream_logger_with_file_handler(logger, logs_file_path)
        self.__loggers[name] = logger
        return logger
