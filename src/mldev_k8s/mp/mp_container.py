# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Multiprocessing container module
==================

Module contains linking and implementation of coordination logic via Python multiprocessing.
General design allows to change technology without (or with minimal) modification of coordination logic.

* :py:class:`SimpleCoordinatorProcess`
    Allows to run `coordination.Coordinator` in separate process.
* :py:class:`MpCoordinatorRunner`
    Runs and manages single `SimpleCoordinatorProcess` in separate process.
* :py:class:`NonBlockingJoiner`
    Allows to start and join child process without blocking parent.
* :py:class:`SimpleTaskHandlerProcess`
    Allows to run `tasks.task.TaskHandler` in separate process.
* :py:class:`MpTaskHandlerRunner`
    Allows to start `SimpleTaskHandlerProcess` in separate process.
* :py:class:`MpTaskExecutorRunner`
    Allows to start `tasks.kubernetes.KubernetesTaskExecutor` or another executor in separate process.
"""

import threading
from multiprocessing import Process
from time import sleep

import mldev_k8s.config as config
from mldev_k8s.coordination import FinishedState, Coordinator
from mldev_k8s.environments.kubernetes import KubernetesDockerImageFactory, KubernetesTaskExecutorFactory
from mldev_k8s.environments.local import LocalDockerImageFactory, LocalTaskExecutorFactory
from mldev_k8s.logs import TaskLogFactory, TaskExecutorLogFactory, TaskHandlerLogFactory, CoordinatorLogFactory
from mldev_k8s.mp.mp_channels import MpChannelsFactory
from mldev_k8s.tasks.task import TaskHandler, ExecutorFinished, ExecutorResultType, FailedState, TaskHandlerStatusChangedMessage

MAIN_CYCLE_SLEEP_TIME_SEC = 1


class SimpleCoordinatorProcess:

    def __init__(self, main_process_rcv_channel, docker_image_factory, task_handler_runner,
                 channels_factory, coordinator_log_factory):
        self.coordinator = Coordinator(docker_image_factory, task_handler_runner, channels_factory,
                                       coordinator_log_factory)
        self.main_process_rcv_channel = main_process_rcv_channel
        self.coordinator_log_factory = coordinator_log_factory

    def run(self):
        while type(self.coordinator.state) != FinishedState:
            msg_received = False
            msg_received = msg_received or self.coordinator.receive_from_channel_and_process(
                self.main_process_rcv_channel, self.coordinator.state.process_coordination_message)
            msg_received = msg_received or self.coordinator.receive_from_channel_and_process(
                self.coordinator.handlers_rcv_channel, self.coordinator.state.process_task_handler_message)
            self.coordinator.state.process_state()
            if not msg_received:
                sleep(MAIN_CYCLE_SLEEP_TIME_SEC)


class NonBlockingJoiner:

    def __init__(self, process):
        self.process = process

    def run(self):
        self.process.join()


class MpCoordinatorRunner:
    _coordinator_snd_rcv_pair = None

    def _get_docker_image_factory(self):
        if config.DEFAULT_RUN_ENV == config.RunEnvironment.LOCAL:
            return LocalDockerImageFactory()
        if config.DEFAULT_RUN_ENV == config.RunEnvironment.KUBERNETES:
            return KubernetesDockerImageFactory(CoordinatorLogFactory())
        raise Exception(f'Unsupported run environment: {config.DEFAULT_RUN_ENV}')

    def _get_task_executor_factory(self):
        if config.DEFAULT_RUN_ENV == config.RunEnvironment.LOCAL:
            return LocalTaskExecutorFactory()
        if config.DEFAULT_RUN_ENV == config.RunEnvironment.KUBERNETES:
            return KubernetesTaskExecutorFactory(TaskLogFactory(), TaskExecutorLogFactory())
        raise Exception(f'Unsupported run environment: {config.DEFAULT_RUN_ENV}')

    def _get_task_executor_runner(self):
        task_executor_factory = self._get_task_executor_factory()
        task_executor_runner = MpTaskExecutorRunner(task_executor_factory, TaskExecutorLogFactory())
        return task_executor_runner

    def _get_task_handler_runner(self):
        task_executor_runner = self._get_task_executor_runner()
        task_handler_runner = MpTaskHandlerRunner(task_executor_runner, TaskHandlerLogFactory())
        return task_handler_runner

    def _get_channels_factory(self):
        channels_factory = MpChannelsFactory()
        return channels_factory

    def run_or_get(self):
        if MpCoordinatorRunner._coordinator_snd_rcv_pair is None:
            channels_factory = self._get_channels_factory()

            coordinator_snd_channel, coordinator_rcv_channel = channels_factory.create_send_receive_pair()

            # it seems better to create and run SimpleCoordinatorProcess in one process;
            # manager queue not found errors occurs sometimes;
            # probably in some circumstances queue can be garbage collected and disposed (i.e. when cp
            # local variable is disposed, but process not started yet);
            # see also coordination.py
            def process_function():
                cp = SimpleCoordinatorProcess(coordinator_rcv_channel, docker_image_factory,
                                              task_handler_runner, channels_factory, CoordinatorLogFactory())
                cp.run()

            docker_image_factory = self._get_docker_image_factory()
            task_handler_runner = self._get_task_handler_runner()

            process = Process(target=process_function)

            non_blocking_joiner = NonBlockingJoiner(process)
            process.start()

            threading.Thread(target=non_blocking_joiner.run).start()

            MpCoordinatorRunner._coordinator_snd_rcv_pair = coordinator_snd_channel, coordinator_rcv_channel
        return MpCoordinatorRunner._coordinator_snd_rcv_pair


CoordinatorRunner = MpCoordinatorRunner


class MpTaskExecutorRunner:

    def __init__(self, task_executor_factory, task_executor_log_factory):
        self.task_executor_factory = task_executor_factory
        self.task_executor_log_factory = task_executor_log_factory

    def run_task_executor(self, stage_name, handler_snd_channel, workflow_id):

        def process_function():
            try:
                executor = self.task_executor_factory.create(stage_name, handler_snd_channel, workflow_id)
                executor.run()
            except Exception as e:
                task_executor_log_factory.get_logger(stage_name).error(e, exc_info=True)
                handler_snd_channel.send_nowait(ExecutorFinished(ExecutorResultType.ERROR))

        task_executor_log_factory = self.task_executor_log_factory
        executor_process = Process(target=process_function)

        executor_process.start()


class SimpleTaskHandlerProcess:

    def __init__(self, task_handler):
        self.task_handler = task_handler

    def run(self):
        while not self.task_handler.is_in_terminal_state():
            msg_received = False
            msg_received = msg_received or self.task_handler.receive_from_channel_and_process(
                self.task_handler.coordinator_rcv_channel, self.task_handler.state.process_control_msg)
            for channel in self.task_handler.upstream_rcv_channels:
                msg_received = msg_received or self.task_handler.receive_from_channel_and_process(
                    channel, self.task_handler.state.process_upstream_msg)
            msg_received = msg_received or self.task_handler.receive_from_channel_and_process(
                self.task_handler.executor_rcv_channel, self.task_handler.state.process_executor_msg)
            self.task_handler.state.process_state()
            if not msg_received:
                sleep(MAIN_CYCLE_SLEEP_TIME_SEC)


class MpTaskHandlerRunner:

    def __init__(self, task_executor_runner, task_handler_log_factory):
        self.task_executor_runner = task_executor_runner
        self.task_handler_log_factory = task_handler_log_factory

    def _get_task_handler(self, stage_name, workflow_id, coordinator_rcv_channel,
                          coordinator_snd_channel, from_executor_snd_channel, executor_rcv_channel,
                          upstream_stages_names, downstream_stages_names, upstream_rcv_channels,
                          downstream_snd_channels):
        return TaskHandler(self.task_handler_log_factory, stage_name, workflow_id, coordinator_rcv_channel,
                           coordinator_snd_channel, from_executor_snd_channel, executor_rcv_channel,
                           self.task_executor_runner, upstream_stages_names, downstream_stages_names,
                           upstream_rcv_channels, downstream_snd_channels)

    def run_task_handler(self, stage_name, coordinator_rcv_channel, coordinator_snd_channel, from_executor_snd_channel,
                         executor_rcv_channel,
                         upstream_stages_names, downstream_stages_names,
                         upstream_rcv_channels, downstream_snd_channels, workflow_id):

        def process_function():
            try:
                handler_process = SimpleTaskHandlerProcess(handler)
                handler_process.run()
            except Exception as e:
                new_state = FailedState(handler)
                msg = TaskHandlerStatusChangedMessage(stage_name, new_state.name)
                coordinator_snd_channel.send_nowait(msg)
                task_handler_log_factory.get_logger(stage_name).error(e, exc_info=True)

        handler = self._get_task_handler(stage_name, workflow_id, coordinator_rcv_channel,
                                         coordinator_snd_channel,
                                         from_executor_snd_channel, executor_rcv_channel, upstream_stages_names,
                                         downstream_stages_names, upstream_rcv_channels,
                                         downstream_snd_channels)

        task_handler_log_factory = self.task_handler_log_factory

        process = Process(target=process_function)
        process.start()
        return process
