from multiprocessing import Manager
from multiprocessing.queues import Queue
from queue import Empty

import mldev_k8s.channels as channels
from mldev_k8s.channels import ChannelsFactory, SendChannel, ReceiveChannel

"""
Channels multiprocessing module
==================

Implements `channels` abstractions as Python multiprocessing queues.

"""


class MpSendChannel(SendChannel):

    def __init__(self, queue: Queue):
        self.__queue = queue

    def send_nowait(self, message):
        self.__queue.put_nowait(message)

    def clone(self):
        return MpSendChannel(self.__queue)


class MpReceiveChannel(ReceiveChannel):

    def __init__(self, queue: Queue):
        self.__queue = queue

    def receive_nowait(self):
        try:
            return self.__queue.get_nowait()
        except Empty:
            raise MpWouldBlock


class MpChannelsFactory(ChannelsFactory):

    def __init__(self):
        self.__manager = Manager()

    def create_send_receive_pair(self):
        queue = self.__manager.Queue()
        return MpSendChannel(queue), MpReceiveChannel(queue)


class MpWouldBlock(channels.WouldBlock):
    pass
