ARG MLDEV_K8S_BASE_IMAGE
FROM ${MLDEV_K8S_BASE_IMAGE}

WORKDIR /
RUN mkdir experiment
WORKDIR /experiment

COPY requirements-pd.txt requirements.txt

RUN mkdir results
RUN mldev init . -r -p venv

RUN mkdir logs

COPY ./.mldev ./.mldev
COPY ./experiments ./experiments
COPY ./src ./src
COPY ./.dockerignore ./.dockerignore
COPY ./run-stage-env.yml ./run-stage-env.yml
COPY ./server_config.toml ./server_config.toml

ENV PYTHONPATH "${PYTHONPATH}:/experiment/src"