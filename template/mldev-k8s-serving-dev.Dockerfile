# for dev purposes

ARG MLDEV_K8S_BASE_IMAGE
FROM ${MLDEV_K8S_BASE_IMAGE}

WORKDIR /
RUN mkdir experiment
WORKDIR /experiment

COPY requirements-dev.txt requirements.txt

RUN mkdir results
RUN mldev init . -r -p venv

RUN mkdir logs

COPY ./.mldev ./.mldev
COPY ./experiments ./experiments
COPY ./src ./src
COPY ./.dockerignore ./.dockerignore
COPY ./run-stage-env.yml ./run-stage-env.yml
COPY ./server_config.toml ./server_config.toml

COPY --from=mldev-k8s-dev-pkg /mldev_k8s_pkg mldev_k8s_pkg

# activate venv
ENV PATH="/experiment/venv/bin:$PATH"
# install dev copy of mldev_k8s package
RUN pip install -e ./mldev_k8s_pkg

ENV PYTHONPATH "${PYTHONPATH}:/experiment/src"
