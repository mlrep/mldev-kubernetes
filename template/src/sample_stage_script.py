# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

import os
import sys
from datetime import datetime

arguments = sys.argv

RESULTS_DIR = 'results'

if len(sys.argv) == 1:
    print('Nothing is specified')

if len(sys.argv) == 2:
    print(f'Specified name is {sys.argv[1]}')

if len(sys.argv) >= 3:
    output_file = sys.argv[2]
    print(f'Results file: {output_file}')
    result = f'Name: {sys.argv[1]}; Now: {datetime.utcnow()}\n'
    if len(sys.argv) >= 4:
        result += 'Data read:\n\t'
        for input_file in sys.argv[3:]:
            with open(os.path.join(RESULTS_DIR, input_file)) as file:
                result += '\n\t'.join([line.replace('\n', '') for line in file])
            result += '\n\t'
    with open(os.path.join(RESULTS_DIR, output_file), 'w') as file:
        file.writelines(result)
