# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

# Source code from
# https://gitlab.com/mlrep/mldev-recommender-systems/-/blob/master/src/recommenders/bandit/thompson_sampling.py

import numpy as np


def init_parameters(input_data):
    """
    Initialization algorithm's parameters.

    M: Number of unique actions. 0 < M.
    returns Initialized parameters of Beta-distribution over each action.
    """
    M = input_data.M

    if M <= 0:
        raise ValueError("Negative number of actions.")

    params = np.zeros(shape=(M, 2))
    return {'params': params.tolist()}


def predict(input_data):
    """
    Predicting actions to recommend by sampling from Beta-distribution.

    params: Parameters of Beta-distribution over each action.
    l: Number of actions that the algorithm takes on each round.
    returns Vector of shape (l, ) that contains recommended actions indices.
    """
    params = np.asarray(input_data.params)
    l = input_data.l
    probs = np.random.beta(a=params[:, 0]+1, b=params[:, 1]+1)
    actions = np.argsort(-probs)[:l]
    return {'actions': actions.tolist()}


def update(input_data):
    """
    Updating algorithm parameters using bandit's response.

    params: Parameters of Beta-distribution over each action.
    actions: Vector of recommended actions by the algorithm.
    reward: Vector of reward per each action.
    returns Updated algorithm parameters.
    """
    params = np.asarray(input_data.params)
    actions = np.asarray(input_data.actions)
    reward = np.asarray(input_data.reward)

    # First round without the bandit response
    if (reward is None) or (len(reward) == 0):
        return params

    if any(i < 0 for i in reward):
        raise ValueError("Negative reward")

    params[actions] += np.vstack([reward[actions], 1 - reward[actions]]).T
    return {'params': params.tolist()}
