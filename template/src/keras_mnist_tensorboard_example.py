# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

# Examples used:
# https://www.tensorflow.org/tensorboard/get_started
# https://keras.io/examples/vision/mnist_convnet/

import shutil
import os
import sys
import numpy as np

import tensorflow as tf


def cleanup_and_prepare_dir(dir):
    if os.path.exists(dir):
        shutil.rmtree(dir)
    os.makedirs(dir)


def create_dense_model():
    return tf.keras.models.Sequential([
        tf.keras.layers.Flatten(input_shape=(28, 28)),
        tf.keras.layers.Dense(512, activation='relu'),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.Dense(10, activation='softmax')
    ])


def create_2_layer_conv_model():
    return tf.keras.models.Sequential([
        tf.keras.Input(shape=(28, 28, 1)),
        tf.keras.layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
        tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),
        tf.keras.layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
        tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dropout(0.5),
        tf.keras.layers.Dense(10, activation="softmax"),
    ])


def create_1_layer_conv_model():
    return tf.keras.models.Sequential([
        tf.keras.Input(shape=(28, 28, 1)),
        tf.keras.layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
        tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dropout(0.5),
        tf.keras.layers.Dense(10, activation="softmax"),
    ])


def run(model_name, tensorboard_logs_dir):
    mnist = tf.keras.datasets.mnist

    (x_train, y_train),(x_test, y_test) = mnist.load_data()
    x_train, x_test = x_train / 255.0, x_test / 255.0

    model = None

    if model_name == 'conv_1l':
        model = create_1_layer_conv_model()
        x_train = np.expand_dims(x_train, -1)
        x_test = np.expand_dims(x_test, -1)

    if model_name == 'conv_2l':
        model = create_2_layer_conv_model()
        x_train = np.expand_dims(x_train, -1)
        x_test = np.expand_dims(x_test, -1)

    if model_name == 'dense':
        model = create_dense_model()

    model.compile(optimizer='adam',
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])

    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=tensorboard_logs_dir, histogram_freq=1)

    model.fit(x=x_train,
              y=y_train,
              epochs=50,
              validation_data=(x_test, y_test),
              callbacks=[tensorboard_callback])


if __name__ == "__main__":
    arguments = sys.argv

    if len(sys.argv) != 3:
        raise Exception('Specify subdirectory and model name: conv_1l, conv_2l or dense')

    subdirectory = arguments[1]
    model_name = arguments[2]

    if (model_name != 'conv_2l') and (model_name != 'conv_1l') and (model_name != 'dense'):
        raise Exception(f'Model name can be conv_1l, conv_2l or dense; given {model_name}')

    tensorboard_logs_dir = os.path.join(subdirectory, model_name)

    cleanup_and_prepare_dir(tensorboard_logs_dir)
    run(model_name, tensorboard_logs_dir)
