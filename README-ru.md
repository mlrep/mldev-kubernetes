# MLDev Kubernetes

MLDev Kubernetes дает возможность исследователю выполнять эксперименты в Kubernetes. 
Kubernetes позволяет задействовать кластерные ресурсы, недоступные на рабочей станции исследователя, 
что разрешает эксперименты с трудоемкими вычислениями.

Кроме этого поддерживается параллельное выполнение этапов эксперимента, 
что позволяет масштабировать вычисления как на кластере, так и локально, на рабочей станции исследователя.

Для сохранения входных данных и результатов экспериментов можно использовать различные системы хранения, 
с которыми можно работать через абстракции и интерфейсы файловой системы.

Отличительной чертой MLDev Kubernetes является простота и доступность для исследователя.
Чтобы запустить эксперимент с MLDev Kubernetes исследователю не нужно погружаться в вопросы инженерии, 
и не требуется сколь-нибудь существенной модификации существующих пайплайнов экспериментов.  

# Запуск эксперимента в Kubernetes

MLDev Kubernetes требует предварительной настройки окружения и пользовательской среды, что описано в разделе [Настройка](https://gitlab.com/mlrep/mldev-kubernetes/-/wikis/%D0%A0%D1%83%D0%BA%D0%BE%D0%B2%D0%BE%D0%B4%D1%81%D1%82%D0%B2%D0%BE-%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D1%82%D0%B5%D0%BB%D1%8F#%D0%B7%D0%B0%D0%BF%D1%83%D1%81%D0%BA-%D1%8D%D0%BA%D1%81%D0%BF%D0%B5%D1%80%D0%B8%D0%BC%D0%B5%D0%BD%D1%82%D0%B0) в руководстве пользователя.

Порядок запуска простого эксперимента в Kubernetes описан в [инструкции](https://gitlab.com/mlrep/mldev-kubernetes/-/wikis/%D0%A0%D1%83%D0%BA%D0%BE%D0%B2%D0%BE%D0%B4%D1%81%D1%82%D0%B2%D0%BE-%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D1%82%D0%B5%D0%BB%D1%8F#%D0%B7%D0%B0%D0%BF%D1%83%D1%81%D0%BA-%D1%8D%D0%BA%D1%81%D0%BF%D0%B5%D1%80%D0%B8%D0%BC%D0%B5%D0%BD%D1%82%D0%B0).  

# Документация

Документация пользователя и разработчика доступна в [wiki](https://gitlab.com/mlrep/mldev-kubernetes/-/wikis/Главная) проекта.

## Руководство пользователя

[Руководство пользователя](https://gitlab.com/mlrep/mldev-kubernetes/-/wikis/%D0%A0%D1%83%D0%BA%D0%BE%D0%B2%D0%BE%D0%B4%D1%81%D1%82%D0%B2%D0%BE-%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D1%82%D0%B5%D0%BB%D1%8F) помогает:
- настроить окружение и пользовательскую среду
- запустить первый эксперимент в Kubernetes 
- изучить функции MLDev Kubernetes

## Учебник

Учебник помогает решить типовые задачи и лучше освоить функциональность MLDev Kubernetes.

Cценарии использования MLDev Kubernetes приведены в [учебнике](https://gitlab.com/mlrep/mldev-kubernetes/-/wikis/%D0%A3%D1%87%D0%B5%D0%B1%D0%BD%D0%B8%D0%BA) в виде пошаговых руководств.

## Документация разработчика

Создание окружения для разработки, архитектура и рекомендации по разработке описаны в [документации разработчика](https://gitlab.com/mlrep/mldev-kubernetes/-/wikis/%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D1%86%D0%B8%D1%8F-%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA%D0%B0).

## Руководство DevOps

Инструкции по настройке среды выполнения Kubernetes и систем хранения данных с учетом требований MLDev Kubernetes приводятся в [руководстве DevOps](https://gitlab.com/mlrep/mldev-kubernetes/-/wikis/%D0%A0%D1%83%D0%BA%D0%BE%D0%B2%D0%BE%D0%B4%D1%81%D1%82%D0%B2%D0%BE-DevOps).

# Партнеры и поддержка

### Фонд Содействия Инновациям

<p>
<a href="https://fasie.ru/"><img src="https://fasie.ru/local/templates/.default/markup/img/logo_new.svg" alt="Фонд Содействия Инновациям" height="80px"/></a>
</p>

### Gitlab open source

<p>
<a href="https://about.gitlab.com/solutions/open-source/"><img src="https://gitlab.com/mlrep/mldev/-/wikis/images/gitlab-logo-gray-stacked-rgb.png" alt="GitLab Open Source program" height="80px"></a>
</p> 


# Поддержка и контакты

Предоставить обратную связь, предложить функцию или сообщить о баге:
- Группа пользователей в [Телеграм](https://t.me/mldev_betatest)
- Канал [#mlrep](https://opendatascience.slack.com) в Slack OpenDataScience
- Трэкер задач [Gitlab](https://gitlab.com/mlrep/mldev-kubernetes/-/issues)

# Участие

Чтобы участвовать в проекте, задать вопрос или предложить функцию, пожалуйста, ознакомьтесь с руководством [CONTRIBUTING.md](https://gitlab.com/mlrep/mldev/-/blob/develop/CONTRIBUTING.md). 

# Лицензия

Это программное обеспечение распространятся с лицензией [Apache 2.0 license](LICENSE).
