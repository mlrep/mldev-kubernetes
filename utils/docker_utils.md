### build mldev-dev Docker image
docker build . -t mldev-dev -f mldev-dev.Dockerfile
docker build . -t mldev-dev -f mldev-dev-ssh-no-pwd.Dockerfile

### run experiment in Docker container (interactive)
docker run -v C:\Code\MLRep\mldev-kubernetes:/experiment --name mldev-dev -t -i mldev-dev
cd experiment
mldev init . -r -p venv
#### start again
docker start -i mldev-dev

### run experiment via ssh in Docker container (remote debugger)
docker run -v C:\Code\MLRep\mldev-kubernetes:/experiment --name mldev-dev -t -i -p 2222:22 mldev-dev
cd experiment
mldev init . -r -p venv
#### start again
docker start -i mldev-dev
service ssh start

### build and run experiment using Docker (docker inside docker container)
docker build . -t docker-ubuntu -f docker-ubuntu.Dockerfile
docker build . -t mldev-docker-dev -f mldev-docker-dev.Dockerfile
docker run -v C:\Code\MLRep\mldev-kubernetes:/experiment --name mldev-docker-dev -t -i -p 2223:22 --add-host=host.docker.internal:host-gateway --privileged mldev-docker-dev
#### init if necessary
service ssh start
service docker start
#### start again
docker start -i mldev-docker-dev
service ssh start
service docker start
