Problem:
- to develop and test k8s package without deployment to remote package registry
- test or sample experiments require k8s package inside experiment container, 
also they are placed in template folder (package code - in src, build files - in root);
hence, installation of package in editable mode is complicated  

Application:
- local or remote execution (remote Linux or Docker container)

Steps to run sample experiments:
- make changes in package source code (/src)
- go to root (/)
- build Docker image containing actual package code (will be used to get package code 
  in experiment container):
  ``docker build . -t mldev-k8s-dev-pkg -f ./utils/mldev-k8s-dev-pkg.Dockerfile``this image will be used to 
- in experiment spec set dockerfile to dev (it will copy package source code from mldev-k8s-dev-pkg container, 
  and install it to experiment env in editable mode):
  ``PARALLEL_TASK_DOCKER_FILE: 'mldev-k8s-dev.Dockerfile'``
- init mldev venv inside template using experiment requirements:
  ``mldev init . -r``
- activate venv
  ``source ./template/venv/bin/activate``
- install package inside activated venv in editable mode:
  ``pip install -e .``

On source code changes only mldev-k8s-dev-pkg rebuild required.

Such a complicated process with special image build is required for current project structure. 
It clearly separates package source code and sample experiments.

Steps to run Python program invoking mldev-k8s package code:
- to run just Python code (not mldev) only installation of package in editable mode is enough