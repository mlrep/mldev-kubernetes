FROM ubuntu:18.04

USER root

RUN apt-get update && apt-get install curl openssh-server sudo -y

RUN useradd -rm -s /bin/bash -g root -G sudo -u 1000 user
# simplifies ssh connection
RUN passwd --delete user
RUN sed -i 's/^#PermitEmptyPasswords no/PermitEmptyPasswords yes/' /etc/ssh/sshd_config
# (!) line needed to allow connections without password
RUN echo "ssh" >> /etc/securetty

EXPOSE 22

RUN mkdir /install_mldev
WORKDIR /install_mldev
RUN curl https://gitlab.com/mlrep/mldev/-/raw/develop/install_mldev.sh -o install_mldev.sh
RUN chmod +x ./install_mldev.sh
RUN ./install_mldev.sh base
WORKDIR /

ENTRYPOINT service ssh start && bash