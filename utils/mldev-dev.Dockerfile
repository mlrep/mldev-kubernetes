FROM ubuntu:18.04

USER root

RUN apt-get update && apt-get install curl openssh-server sudo -y

RUN useradd -rm -s /bin/bash -g root -G sudo -u 1000 user
RUN  echo 'user:user' | chpasswd

EXPOSE 22

RUN mkdir /install_mldev
WORKDIR /install_mldev
RUN curl https://gitlab.com/mlrep/mldev/-/raw/develop/install_mldev.sh -o install_mldev.sh
RUN chmod +x ./install_mldev.sh
RUN ./install_mldev.sh base
WORKDIR /

ENTRYPOINT service ssh start && bash