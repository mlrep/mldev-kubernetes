FROM docker-ubuntu:latest

USER root

# todo: install kubectl https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/

RUN apt-get update && apt-get install curl openssh-server sudo python3-pip -y
RUN apt-get install python3-pip -y

# todo: add upgrade pip, virtualenv to mldev-dev.Dockerfile, mldev-dev-ssh-no-pwd.Dockerfile
RUN pip3 install --upgrade pip
RUN pip3 install --upgrade virtualenv

RUN mkdir /install_mldev
WORKDIR /install_mldev
RUN curl https://gitlab.com/mlrep/mldev/-/raw/develop/install_mldev.sh -o install_mldev.sh
RUN chmod +x ./install_mldev.sh
RUN ./install_mldev.sh base

# simplifies ssh connection
RUN passwd --delete user
RUN sed -i 's/^#PermitEmptyPasswords no/PermitEmptyPasswords yes/' /etc/ssh/sshd_config
# (!) line needed to allow connections without password
RUN echo "ssh" >> /etc/securetty

EXPOSE 22

WORKDIR /
