FROM ubuntu:18.04

WORKDIR /
RUN mkdir mldev_k8s_pkg
WORKDIR /mldev_k8s_pkg

COPY ./src src
COPY pyproject.toml pyproject.toml
COPY setup.cfg setup.cfg
