FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y ca-certificates curl gnupg lsb-release sudo

RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

RUN echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

RUN apt-get update
RUN apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin iptables

RUN useradd -rm -s /bin/bash -g root -u 1000 user && usermod -aG sudo user
# commands below are usefull for starting/using docker in scripts without sudo, which is not allowed
# can be omitted in other scenarios
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
RUN usermod -aG docker user

USER user

