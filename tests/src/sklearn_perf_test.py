import argparse
import importlib
import json
import os
from typing import List

import numpy as np
import pandas as pd
import sklearn.ensemble
from sklearn.datasets import make_classification
from sklearn.model_selection import RepeatedStratifiedKFold, GridSearchCV

RESULTS_DIR = 'results'

np.random.seed(0)


def create_estimator(estimator_name=None):
    if not estimator_name:
        return sklearn.ensemble.GradientBoostingClassifier()
    index = estimator_name.rfind('.')
    module_name = estimator_name[:index]
    class_name = estimator_name[index + 1:]
    module = importlib.import_module(module_name)
    class_ = getattr(module, class_name)
    estimator = class_()
    return estimator


def run(estimator_name, grid_params=None, output=None, verbose_param=None):
    X, y = make_classification(n_samples=10000, n_features=10, n_informative=5, n_redundant=5)

    param_grid = [
        {'n_estimators': [1, 10, 100]}
    ]

    if grid_params:
        param_grid[0].update(grid_params)

    model = create_estimator(estimator_name)

    verbose = 4
    if verbose_param:
        verbose = verbose_param

    # model = sklearn.ensemble.GradientBoostingClassifier()
    cv = RepeatedStratifiedKFold(n_splits=10, n_repeats=3)
    grid_search = GridSearchCV(estimator=model,
                               param_grid=param_grid, scoring='accuracy', cv=cv, n_jobs=1,
                               error_score='raise', verbose=verbose)
    grid_search.fit(X, y)

    df_cv_results = pd.concat([pd.DataFrame(grid_search.cv_results_["params"]),
                               pd.DataFrame(grid_search.cv_results_["mean_test_score"], columns=["accuracy"])],
                              axis=1)

    if output:
        df_cv_results.to_csv(os.path.join(RESULTS_DIR, output), index=False)


def parse_key_value_var(s):
    items = s.split('=')
    key = items[0].strip()
    right_side = items[1].strip()
    val = json.loads(right_side)
    if not isinstance(val, List):
        val = [val]

    return key, val


def parse_grid_params(items):
    if not items:
        return None
    result = {}
    for item in items:
        key, value = parse_key_value_var(item)
        result[key] = value
    return result


parser = argparse.ArgumentParser(description='Runs gradient boosting grid search')
parser.add_argument('-e', '--estimator', type=str)
parser.add_argument('-g', '--grid', nargs='+', type=str)
parser.add_argument('-o', '--output', type=str)
parser.add_argument('-v', '--verbose', type=int)

args = parser.parse_args()
print(args)
grid_params = parse_grid_params(args.grid)
run(args.estimator, grid_params, args.output, args.verbose)
