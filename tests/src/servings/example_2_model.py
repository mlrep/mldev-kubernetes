# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

def invoke(input_data):
    print(f'Echo 2 input: {input_data}')
    return 2


def load(model_loading_params):
    print(f'Model 2 load called, params: {model_loading_params}')
