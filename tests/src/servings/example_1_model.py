# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

def invoke(input_data):
    print(f'Echo 1 input: {input_data}')
    return {'output': (input_data.min + input_data.max)/2}


def forecast(input_data):
    print(f'Echo 1 input: {input_data}')
    if isinstance(input_data, int):
        return 10/input_data
    return 1


def load(model_loading_params):
    print(f'Model 1 load called, params: {model_loading_params}')
