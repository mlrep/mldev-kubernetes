# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

import requests
import trio
from hypercorn.config import Config
from hypercorn.trio import serve
from requests import ReadTimeout

from mldev import logger

# change to 'DEBUG' or 'ERROR' to show server logs
logger.setLevel('CRITICAL')

# возможно, переписать на pytest с pytest-trio
# https://trio.readthedocs.io/en/stable/awesome-trio-libraries.html#testing

HOST = '127.0.0.1:8881'
BASE_URL = 'http://127.0.0.1:8881'


async def run_server():
    # modify environment for testing purposes
    # https://www.starlette.io/config/#reading-or-modifying-the-environment
    from starlette.config import environ
    environ['SERVING_MODELS_METADATA'] = "./integration/servings/model_1_metadata.json," \
                                         "./integration/servings/model_2_metadata.json"

    from mldev_k8s.servings.service.main import app
    config = Config()
    config.bind = [HOST]

    await serve(app, config, shutdown_trigger=shutdown_event.wait)


async def prepare_and_run():

    async with trio.open_nursery() as n:
        n.start_soon(run_server)
        n.start_soon(check_server_is_running)
        n.start_soon(run_test_suite)


async def check_server_is_running():

    def make_requests():
        while True:
            try:
                request_url = BASE_URL
                requests.get(request_url, timeout=0.1)
            except ConnectionError:
                continue
            except ReadTimeout:
                continue
            return

    await trio.to_thread.run_sync(make_requests)
    server_is_running_event.set()


async def run_test_suite():
    await server_is_running_event.wait()
    await trio.to_thread.run_sync(test_call_model_1_with_valid_data_responses_with_valid_result)
    await trio.to_thread.run_sync(test_call_model_1_with_skipped_json_header_responses_with_code_400)
    await trio.to_thread.run_sync(test_call_model_1_with_invalid_data_responses_with_code_400)
    await trio.to_thread.run_sync(test_forecast_model_1_with_valid_data_responses_with_valid_result)
    await trio.to_thread.run_sync(test_public_invoke_model_2_with_valid_data_responses_with_valid_result)
    await trio.to_thread.run_sync(test_forecast_model_1_with_invalid_data_responses_with_code_500)
    await trio.to_thread.run_sync(test_request_invalid_model_name_responses_with_code_404)
    shutdown_event.set()


def test_call_model_1_with_valid_data_responses_with_valid_result():
    print('test_call_model_1_with_valid_data_responses_with_valid_result')
    request_url = f'{BASE_URL}/mldev/models/some_model_1/versions/0.1/call'
    request_data = {'min': 1, 'max': 2}

    response = requests.post(request_url, json=request_data)
    assert response.status_code == 200, (response.status_code, response.content)

    result = response.json()
    assert 'output' in result
    assert result['output'] == 1.5
    print('passed')


def test_call_model_1_with_skipped_json_header_responses_with_code_400():
    print('test_call_model_1_with_skipped_json_header_responses_with_code_400')
    request_url = f'{BASE_URL}/mldev/models/some_model_1/versions/0.1/call'
    request_data = {'min': 1, 'max': 2}

    response = requests.post(request_url, data=request_data)
    assert response.status_code == 400
    print(response.content)
    print('passed')


def test_forecast_model_1_with_valid_data_responses_with_valid_result():
    print('test_forecast_model_1_with_valid_data_responses_with_valid_result')
    request_url = f'{BASE_URL}/mldev/models/some_model_1/versions/0.1/forecast'
    request_data = {'any_field': 'any_data'}

    response = requests.post(request_url, json=request_data)
    assert response.status_code == 200, (response.status_code, response.content)

    result = response.json()
    assert result == 1
    print(response.content)
    print('passed')


def test_public_invoke_model_2_with_valid_data_responses_with_valid_result():
    print('test_public_invoke_model_2_with_valid_data_responses_with_valid_result')
    request_url = f'{BASE_URL}/mldev/models/some_model_2/versions/0.1/public_invoke'
    request_data = {'any_field': 'any_data'}

    response = requests.post(request_url, json=request_data)
    assert response.status_code == 200, (response.status_code, response.content)

    result = response.json()
    assert result == 2
    print(response.content)
    print('passed')


def test_call_model_1_with_invalid_data_responses_with_code_400():
    print('test_call_model_1_with_invalid_data_responses_with_code_400')
    request_url = f'{BASE_URL}/mldev/models/some_model_1/versions/0.1/call'
    request_data = {'min': 'abcd'}

    response = requests.post(request_url, json=request_data)
    assert response.status_code == 400

    print(response.content)
    print('passed')


def test_forecast_model_1_with_invalid_data_responses_with_code_500():
    print('test_forecast_model_1_with_invalid_data_responses_with_code_500')
    request_url = f'{BASE_URL}/mldev/models/some_model_1/versions/0.1/forecast'
    request_data = 0

    response = requests.post(request_url, json=request_data)
    assert response.status_code == 500, (response.status_code, response.content)

    print(response.content)
    print('passed')


def test_request_invalid_model_name_responses_with_code_404():
    print('test_request_invalid_model_name_responses_with_code_404')
    request_url = f'{BASE_URL}/mldev/models/wrong_model_name/versions/0.1/forecast'
    request_data = 0

    response = requests.post(request_url, json=request_data)
    assert response.status_code == 404, (response.status_code, response.content)

    print(response.content)
    print('passed')


shutdown_event = trio.Event()
server_is_running_event = trio.Event()
trio.run(prepare_and_run)
