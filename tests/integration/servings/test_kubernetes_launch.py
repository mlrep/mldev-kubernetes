# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

from mldev import logger

from mldev_k8s.servings import config
from mldev_k8s.servings.environments.kubernetes import KubernetesDockerImageFactory, KubernetesServiceLauncherFactory
from mldev_k8s.servings.fire import Fire

logger.setLevel('INFO')

# dev package Docker image build required (see utils\mldev_k8s_pkg_development.md)

# setup required env vars:
config.SERVICE_DOCKER_FILE = 'mldev-k8s-serving-dev.Dockerfile'
config.SERVICE_K8S_IMAGE_PULL_SECRET = 'iimage-registry-credentials'
config.SERVICE_DOCKER_IMAGE_REGISTRY = 'localhost:30300/home'


def launch_service_in_kubernetes():
    name = 'sample-serving'
    # deployment_id = secrets.token_hex(3)
    models_metadata_files = ["./integration/servings/model_1_metadata.json",
                             "./integration/servings/model_2_metadata.json"]
    # ksl = KubernetesServiceLauncher(name, deployment_id, models_metadata_files)

    dif = KubernetesDockerImageFactory()
    slf = KubernetesServiceLauncherFactory()

    fire = Fire(dif, slf)
    fire.fire_up(name, models_metadata_files)


launch_service_in_kubernetes()
