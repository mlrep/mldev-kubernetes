#!/bin/bash

# Example, how to run service via command line
export SERVING_MODELS_PARAMS="./integration/servings/model_1_metadata.json,
                              ./integration/servings/model_2_metadata.json"
hypercorn mldev_k8s.serving.server:app