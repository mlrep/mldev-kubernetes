# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

import trio
from hypercorn.config import Config
from hypercorn.trio import serve
from starlette.config import environ

from mldev_k8s.servings.service.main import app

# Example, how to run service programmatically

# modify environment for testing purposes
# https://www.starlette.io/config/#reading-or-modifying-the-environment
environ['SERVING_MODELS_METADATA'] = "./integration/servings/model_1_metadata.json," \
                                     "./integration/servings/model_2_metadata.json"

config = Config()
config.bind = ["localhost:8881"]

trio.run(serve, app, config)

# example curl command
# kubectl port-forward <pod_name> 8881 -n mldev
# curl -X POST http://127.0.0.1:8881/mldev/models/some_model_1/versions/0.1/forecast
# -d '{}' -H "Content-Type: application/json"
# curl -X POST http://10.110.141.133:8881/mldev/models/some_model_1/versions/0.1/forecast
# -d '{}' -H "Content-Type: application/json"
# curl -X POST http://127.0.0.1:8881/mldev/models/some_model_1/versions/0.1/call
# -d '{"min":1, "max":2}' -H "Content-Type: application/json"
# curl -X POST http://127.0.0.1:8881/mldev/models/ts_model_1/versions/0.1/init
# -d '{"M":4}' -H "Content-Type: application/json"
# curl -X POST http://127.0.0.1:8881/mldev/models/ts_model_1/versions/0.1/predict
# -d '{"l":2, "params":[[5.0,3.0],[1.0,1.0],[12.0,3.0],[24.0,2.0]]}' -H "Content-Type: application/json"
