# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

import matplotlib.pyplot as plt
import numpy as np
import requests

HOST = '127.0.0.1:8881'
BASE_URL = 'http://127.0.0.1:8881'

BASE_REQUEST_URL = f'{BASE_URL}/mldev/models/ts_model_1/versions/0.1/'

ROUNDS_COUNT = 60


class ThompsonSamplingState:
    M = 7
    l = 2
    params = None


class UserState:
    preferred_actions = [2, 3]
    preferred_actions_probs = {2: 0.3, 3: 0.6}
    last_actions_received = None
    reward_after_last_actions = None
    actions_received_history = np.zeros((ThompsonSamplingState.M, ROUNDS_COUNT))


def init_ts_algorithm():
    request_url = f'{BASE_REQUEST_URL}init'
    request_data = {'M': ThompsonSamplingState.M}

    response = requests.post(request_url, json=request_data)

    if response.status_code != 200:
        raise Exception('Status code != 200')

    result = response.json()
    ThompsonSamplingState.params = result['params']
    print(f'Init ok, result: {result}')


def _compute_reward(actions, round_num):
    UserState.last_actions_received = actions
    reward = np.zeros(ThompsonSamplingState.M)
    interests = UserState.preferred_actions
    for action in actions:
        UserState.actions_received_history[action, round_num] = 1
        if action not in interests:
            continue
        reward[action] = np.random.binomial(1, UserState.preferred_actions_probs[action])
    UserState.reward_after_last_actions = reward.tolist()


def ts_predict(round_num):
    request_url = f'{BASE_REQUEST_URL}predict'
    request_data = {'l': ThompsonSamplingState.l,
                    'params': ThompsonSamplingState.params}

    response = requests.post(request_url, json=request_data)

    if response.status_code != 200:
        raise Exception('Status code != 200')

    result = response.json()
    print(f'Predict ok, result: {result}')
    _compute_reward(result['actions'], round_num)


def ts_update():
    request_url = f'{BASE_REQUEST_URL}update'
    request_data = {'actions': UserState.last_actions_received,
                    'params': ThompsonSamplingState.params,
                    'reward': UserState.reward_after_last_actions}

    response = requests.post(request_url, json=request_data)

    if response.status_code != 200:
        raise Exception('Status code != 200')

    result = response.json()
    ThompsonSamplingState.params = result['params']
    print(f'Update ok, result: {result}')


def visualize_results():
    plt.imshow(UserState.actions_received_history)
    plt.show()


init_ts_algorithm()
for rnd in range(ROUNDS_COUNT):
    ts_predict(rnd)
    ts_update()
visualize_results()
