# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

import requests
import trio
from hypercorn.config import Config
from hypercorn.trio import serve
from requests import ReadTimeout
import numpy as np

from mldev import logger

# change to 'DEBUG' or 'ERROR' to show server logs
logger.setLevel('CRITICAL')

# возможно, переписать на pytest с pytest-trio
# https://trio.readthedocs.io/en/stable/awesome-trio-libraries.html#testing

HOST = '127.0.0.1:8881'
BASE_URL = 'http://127.0.0.1:8881'

BASE_REQUEST_URL = f'{BASE_URL}/mldev/models/ts_model_1/versions/0.1/'

ROUNDS_COUNT = 100


class ThompsonSamplingState:
    M = 5
    l = 2
    params = None


class UserState:
    preferred_actions = [2, 3]
    preferred_actions_probs = {2: 0.3, 3: 0.6}
    last_actions_received = None
    reward_after_last_actions = None


async def run_server():
    # modify environment for testing purposes
    # https://www.starlette.io/config/#reading-or-modifying-the-environment
    from starlette.config import environ
    environ['SERVING_MODELS_METADATA'] = "./integration/servings/ts_model_metadata.json"

    from mldev_k8s.servings.service.main import app
    config = Config()
    config.bind = [HOST]

    await serve(app, config, shutdown_trigger=shutdown_event.wait)


async def prepare_and_run():

    async with trio.open_nursery() as n:
        n.start_soon(run_server)
        n.start_soon(check_server_is_running)
        n.start_soon(run_test_suite)


async def check_server_is_running():

    def make_requests():
        while True:
            try:
                request_url = BASE_URL
                requests.get(request_url, timeout=0.1)
            except ConnectionError:
                continue
            except ReadTimeout:
                continue
            return

    await trio.to_thread.run_sync(make_requests)
    server_is_running_event.set()


async def run_test_suite():
    await server_is_running_event.wait()
    await trio.to_thread.run_sync(init_ts_algorithm)
    for index in range(ROUNDS_COUNT):
        await trio.to_thread.run_sync(ts_predict)
        await trio.to_thread.run_sync(ts_update)
    shutdown_event.set()


def init_ts_algorithm():
    request_url = f'{BASE_REQUEST_URL}init'
    request_data = {'M': ThompsonSamplingState.M}

    response = requests.post(request_url, json=request_data)

    assert response.status_code == 200, (response.status_code, response.content)

    result = response.json()
    assert 'params' in result
    assert len(result['params']) == ThompsonSamplingState.M
    ThompsonSamplingState.params = result['params']
    print(f'Init ok, result: {result}')


def _compute_reward(actions):
    UserState.last_actions_received = actions
    reward = np.zeros(ThompsonSamplingState.M)
    interests = UserState.preferred_actions
    for action in actions:
        if action not in interests:
            continue
        reward[action] = np.random.binomial(1, UserState.preferred_actions_probs[action])
    UserState.reward_after_last_actions = reward.tolist()


def ts_predict():
    request_url = f'{BASE_REQUEST_URL}predict'
    request_data = {'l': ThompsonSamplingState.l,
                    'params': ThompsonSamplingState.params}

    response = requests.post(request_url, json=request_data)

    assert response.status_code == 200, (response.status_code, response.content)

    result = response.json()
    assert 'actions' in result
    assert len(result['actions']) == ThompsonSamplingState.l
    print(f'Predict ok, result: {result}')
    _compute_reward(result['actions'])


def ts_update():
    request_url = f'{BASE_REQUEST_URL}update'
    request_data = {'actions': UserState.last_actions_received,
                    'params': ThompsonSamplingState.params,
                    'reward': UserState.reward_after_last_actions}

    response = requests.post(request_url, json=request_data)

    assert response.status_code == 200, (response.status_code, response.content)

    result = response.json()
    assert 'params' in result
    assert len(result['params']) == ThompsonSamplingState.M
    ThompsonSamplingState.params = result['params']
    print(f'Update ok, result: {result}')


shutdown_event = trio.Event()
server_is_running_event = trio.Event()
trio.run(prepare_and_run)
