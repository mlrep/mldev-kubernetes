from typing import Type, List

import pytest
from pydantic import create_model, BaseModel
from pydantic_core import PydanticUndefined

from mldev_k8s.servings.models.model_meta import ModelMethodSignatureMetadata
from mldev_k8s.servings.models.signatures.json_pydantic import JsonPydanticSignature, create_json_pydantic_signature


# def test_1():
# input_definition = {"input_field_1": 0}
# m1 = create_model("M1", f1=(int, ...))
# x: list[m1]
# m2 = create_model("M2", f2=(List[m1], ...))
# m3 = create_model("M3", __cls_kwargs__={'__root__': int})
# m3.model_validate(3)
# m2
# #create_model(root_model)


def test_create_json_pydantic_signature_with_complex_list_input_returns_signature():
    input_definition = '{"input_field_1": [{"nested_field_1": "abc"}]}'
    signature_definition_type = 'json_pydantic'
    mmsm = ModelMethodSignatureMetadata(input_definition=input_definition,
                                        output_definition=None,
                                        type=signature_definition_type,
                                        type_params=None)

    jsp = create_json_pydantic_signature(mmsm)

    input_pydantic_model: Type[BaseModel] = jsp._JsonPydanticSignature__input_pydantic_model

    assert 'input_field_1' in input_pydantic_model.model_fields
    field = input_pydantic_model.model_fields['input_field_1']
    assert field.default is PydanticUndefined

    # из-за особенностей реализации pydantic, созданная модель проверяется через ее использование
    input_pydantic_model.model_validate({'input_field_1': [
                                            {"nested_field_1": 'abc'},
                                            {"nested_field_1": 'zxc'}
                                        ]})


def test_create_json_pydantic_signature_with_list_input_returns_signature():
    input_definition = '{"input_field_1": [[1]]}'
    signature_definition_type = 'json_pydantic'
    mmsm = ModelMethodSignatureMetadata(input_definition=input_definition,
                                        output_definition=None,
                                        type=signature_definition_type,
                                        type_params=None)

    jsp = create_json_pydantic_signature(mmsm)

    input_pydantic_model: Type[BaseModel] = jsp._JsonPydanticSignature__input_pydantic_model

    assert 'input_field_1' in input_pydantic_model.model_fields
    field = input_pydantic_model.model_fields['input_field_1']
    assert field.default is PydanticUndefined

    # из-за особенностей реализации pydantic, созданная модель проверяется через ее использование
    input_pydantic_model.model_validate({'input_field_1': [[1, 2, 3], [1, 2, 3]]})

def test_create_json_pydantic_signature_with_simple_correct_input_returns_signature():
    input_definition = '{"input_field_1": 0}'
    signature_definition_type = 'json_pydantic'
    mmsm = ModelMethodSignatureMetadata(input_definition=input_definition,
                                        output_definition=None,
                                        type=signature_definition_type,
                                        type_params=None)

    jsp = create_json_pydantic_signature(mmsm)

    input_pydantic_model: Type[BaseModel] = jsp._JsonPydanticSignature__input_pydantic_model

    assert 'input_field_1' in input_pydantic_model.model_fields
    field = input_pydantic_model.model_fields['input_field_1']
    assert field.annotation == int
    assert field.default == PydanticUndefined


def test_create_json_pydantic_signature_with_complex_correct_input_returns_signature():
    input_definition = '{"input_field_1": {"nested_field_1": "abc", "nested_field_2": 0.1}}'
    output_definition = '{"output_field_1": 0}'
    signature_definition_type = 'json_pydantic'
    type_params = {'parse_input_dict': True}
    mmsm = ModelMethodSignatureMetadata(input_definition=input_definition,
                                        output_definition=output_definition,
                                        type=signature_definition_type,
                                        type_params=type_params)

    jsp = create_json_pydantic_signature(mmsm)

    input_pydantic_model: Type[BaseModel] = jsp._JsonPydanticSignature__input_pydantic_model
    output_pydantic_model: Type[BaseModel] = jsp._JsonPydanticSignature__output_pydantic_model

    assert 'input_field_1' in input_pydantic_model.model_fields
    field = input_pydantic_model.model_fields['input_field_1']
    assert field.default is None

    assert len(field.annotation.model_fields) == 2
    nested_fields = field.annotation.model_fields

    assert 'nested_field_1' in nested_fields
    nested_field_1 = nested_fields['nested_field_1']
    assert nested_field_1.annotation == str
    assert nested_field_1.default is PydanticUndefined

    assert 'nested_field_2' in nested_fields
    nested_field_2 = nested_fields['nested_field_2']
    assert nested_field_2.annotation == float
    assert nested_field_2.default is PydanticUndefined

    assert 'output_field_1' in output_pydantic_model.model_fields
    field = output_pydantic_model.model_fields['output_field_1']
    assert field.annotation == int
    assert field.default == PydanticUndefined


@pytest.mark.parametrize('input_data,not_fail',
                         [({'f1': 123, 'f2': {'nf1': 'abc'}}, True),
                          ({'f1': 123}, False),
                          ({'f1': 0.1, 'f2': {'nf1': '12'}}, False)])
def test_validate_input_data_given_correct_data_raises_no_error(input_data, not_fail):
    input_pydantic_model = create_model('InputModel',
                                        f1=(int, ...),
                                        f2=(
                                            create_model('NestedInputModel',
                                                         nf1=(str, ...))
                                            , ...)
                                        )
    output_pydantic_model = None
    jps = JsonPydanticSignature(input_pydantic_model, output_pydantic_model,
                                parse_input_dict=True)

    fail_state = False
    try:
        jps.validate_and_transform_input_data(input_data)
    except Exception:
        fail_state = True

    assert fail_state is not not_fail


def test_validate_input_data_given_parse_input_params_returns_model_instance():
    input_pydantic_model = create_model('InputModel', f1=(int, ...))
    output_pydantic_model = None
    jps = JsonPydanticSignature(input_pydantic_model, output_pydantic_model,
                                parse_input_dict=True)
    input_data = {'f1': 123}

    result = jps.validate_and_transform_input_data(input_data)

    assert result
    assert isinstance(result, input_pydantic_model)


def test_validate_input_data_given_parse_input_params_returns_dict_instance():
    input_pydantic_model = create_model('InputModel', f1=(int, ...))
    output_pydantic_model = None
    jps = JsonPydanticSignature(input_pydantic_model, output_pydantic_model,
                                parse_input_dict=False)
    input_data = {'f1': 123}

    result = jps.validate_and_transform_input_data(input_data)

    assert result
    assert isinstance(result, dict)


def test_validate_output_data_without_model_raises_no_error():
    input_pydantic_model = create_model('InputModel', f1=(int, ...))
    output_pydantic_model = None
    jps = JsonPydanticSignature(input_pydantic_model, output_pydantic_model,
                                parse_input_dict=False)
    output_data = {'f2': 'abc'}

    result = jps.validate_and_transform_output_data(output_data)

    assert result
