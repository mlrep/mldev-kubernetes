Semi-automatic test to verify resilience to errors in stage.

### 1) Start
2.1. Set logger.k8s_coordination_logs: True and logger.level: DEBUG in config.yml  
2.2. Run venv/bin/mldev run -f experiments/resilience/stage-error/experiment.yml

### 2) Assert results
2.1. Assert stage1 task handler changes state to FAILED (console output)
2.2. Assert stage2 task handler changes state to WILL_NOT_START (console output) 
2.3. Assert experiment finishes
2.4. Assert task executor logs contain error record
2.5. Assert task logs contain error details

### 3) Cleanup
3.1. Delete kubernetes job manually (optional)
