Semi-automatic test to verify resilience to hanging or death of task handler process.

### 1) Start
2.1. Set logger.k8s_coordination_logs: True and logger.level: DEBUG in config.yml  
2.2. Run venv/bin/mldev run -f experiments/resilience/task-handler-failure/task-handler-killed/experiment.yml 
  
Test experiment will run ~ 2 min; 

### 2) Kill task handler process
2.1. Check logs to find task handler process id
2.2. kill -SIGKILL <handler process id>

### 3) Assert results
3.1. Assert coordinator stops receive heartbeats from stage1 task handler (console output)
3.2. Stage2 task handler can change its state to WILL_NOT_START (console output)
3.3. Assert coordinator logs timeout from stage1 task handler
3.4. Wait ~1 min while stage1 task executor finishes work or use Ctrl+C to stop immediately

### 4) Cleanup
4.1. Delete kubernetes job manually (optional)
