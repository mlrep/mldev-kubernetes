Semi-automatic test to verify resilience to unhandled exception in task handler.

### 1) Start
2.1. Set logger.k8s_coordination_logs: True and logger.level: DEBUG in config.yml  
2.2. Run venv/bin/mldev run -f experiments/resilience/task-handler-failure/task-handler-exception/experiment.yml 
  
Test experiment will run ~ 1 min; 

### 2) Assert results
2.1. Assert stage1 task handler changes state to FAILED (console output)
2.1. Assert coordinator stops receive heartbeats from stage1 task handler (console output)
2.3. Assert stage2 task handler changes state to WILL_NOT_START (console output) 
2.4. Assert experiment finishes
2.5. Assert stage1 task handler logs contain exception message
2.6. Assert stage2 task handler logs contain upstream stage heartbeat timout record

### 3) Cleanup
3.1. Delete kubernetes job manually (optional)
