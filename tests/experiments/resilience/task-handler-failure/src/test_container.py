from mldev_k8s.logs import TaskHandlerLogFactory
from mldev_k8s.mp.mp_container import MpCoordinatorRunner, MpTaskHandlerRunner
from mldev_k8s.tasks.task import TaskHandler


class FailureTestKubernetesTaskHandler:

    def __init__(self, handler_snd_channel):
        self.handler_snd_channel = handler_snd_channel

    def run(self):
        raise Exception('Test failure')


class TaskHandlerFailureTestTaskHandler(TaskHandler):

    def send_heartbeats_to_downstream_task_handlers(self):
        if self.stage_name == 'stage1':
            raise Exception('Test failure')
        else:
            super().send_heartbeats_to_downstream_task_handlers()


class TaskHandlerFailureTestTaskHandlerRunner(MpTaskHandlerRunner):

    def __init__(self, task_executor_runner, task_handler_log_factory):
        super().__init__(task_executor_runner, task_handler_log_factory)

    def _get_task_handler(self, stage_name, workflow_id, coordinator_rcv_channel,
                          coordinator_snd_channel, from_executor_snd_channel, executor_rcv_channel,
                          upstream_stages_names, downstream_stages_names, upstream_rcv_channels,
                          downstream_snd_channels):
        return TaskHandlerFailureTestTaskHandler(self.task_handler_log_factory, stage_name, workflow_id,
                                                 coordinator_rcv_channel,
                                                 coordinator_snd_channel, from_executor_snd_channel,
                                                 executor_rcv_channel,
                                                 self.task_executor_runner, upstream_stages_names,
                                                 downstream_stages_names,
                                                 upstream_rcv_channels, downstream_snd_channels)


class TaskHandlerFailureTestCoordinatorRunner(MpCoordinatorRunner):

    def _get_task_handler_runner(self):
        task_executor_runner = self._get_task_executor_runner()
        task_handler_runner = TaskHandlerFailureTestTaskHandlerRunner(task_executor_runner, TaskHandlerLogFactory())
        return task_handler_runner


CoordinatorRunner = TaskHandlerFailureTestCoordinatorRunner
