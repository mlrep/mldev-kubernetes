Semi-automatic test to verify resilience to unhandled exception in task executor.

### 1) Start
2.1. Set logger.k8s_coordination_logs: True and logger.level: DEBUG in config.yml  
2.2. Run venv/bin/mldev run -f experiments/resilience/task-executor-failure/task-executor-exception/experiment.yml 
  
Test experiment will run ~ 1 min; 

### 2) Assert results
2.1. Assert stage1 task handler changes state to FAILED (console output)
2.2. Assert stage2 task handler changes state to WILL_NOT_START (console output) 
2.3. Assert experiment finishes
2.4. Assert task executor logs contain exception message

### 3) Cleanup
3.1. Delete kubernetes job manually (optional)
