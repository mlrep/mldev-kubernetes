from mldev_k8s.environments.abc import TaskExecutorFactory
from mldev_k8s.logs import TaskExecutorLogFactory
from mldev_k8s.mp.mp_container import MpCoordinatorRunner
from mldev_k8s.tasks.task import ExecutorFinished, ExecutorResultType


class FailureTestKubernetesTaskExecutor:

    def __init__(self, handler_snd_channel):
        self.handler_snd_channel = handler_snd_channel

    def run(self):
        raise Exception('Test failure')


class FailureTestKubernetesTaskExecutorFactory(TaskExecutorFactory):

    def create(self, name, from_executor_snd_channel, workflow_id):
        return FailureTestKubernetesTaskExecutor(from_executor_snd_channel)


class TaskExecutorFailureTestCoordinatorRunner(MpCoordinatorRunner):

    def _get_task_executor_factory(self):
        return FailureTestKubernetesTaskExecutorFactory()


CoordinatorRunner = TaskExecutorFailureTestCoordinatorRunner

