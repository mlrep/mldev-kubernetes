Semi-automatic test to verify resilience to hanging or death of task executor process.

### 1) Start
2.1. Set logger.k8s_coordination_logs: True and logger.level: DEBUG in config.yml  
2.2. Run venv/bin/mldev run -f experiments/resilience/task-executor-failure/task-executor-killed/experiment.yml 
  
Test experiment will run ~ 2 min; 

### 2) Kill task executor process
2.1. Check logs to find task executor process id
2.2. kill -SIGKILL <executor process id> 
2.3. Wait ~1 min

### 3) Assert results
3.1. Assert stage1 task handler changes state to FAILED (console output)
3.2. Assert stage2 task handler changes state to WILL_NOT_START (console output)
3.3. Assert experiment finishes (console output)
3.4. Assert task handlers logs contain heartbeat timout record

### 4) Cleanup
4.1. Delete kubernetes job manually (optional)
