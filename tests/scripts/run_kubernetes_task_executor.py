# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

from logs import TaskExecutorLogFactory, TaskLogFactory
from mp.mp_channels import MpChannelsFactory
from tasks.kubernetes import KubernetesTaskExecutor

mp_channels_factory = MpChannelsFactory()
snd_channel, rcv_channel = mp_channels_factory.create_send_receive_pair()

kte = KubernetesTaskExecutor('stage1', snd_channel, 'w1', TaskLogFactory(), TaskExecutorLogFactory())

kte.run()
